--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.8
-- Dumped by pg_dump version 10.1

-- Started on 2019-03-15 10:17:24

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12387)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2276 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- TOC entry 199 (class 1259 OID 17142)
-- Name: elenco_clienti_id_elenco_clienti_seq; Type: SEQUENCE; Schema: public; Owner: valpenguin
--

CREATE SEQUENCE elenco_clienti_id_elenco_clienti_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE elenco_clienti_id_elenco_clienti_seq OWNER TO valpenguin;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 200 (class 1259 OID 17144)
-- Name: elenco_clienti; Type: TABLE; Schema: public; Owner: valpenguin
--

CREATE TABLE elenco_clienti (
    id_elenco_clienti integer DEFAULT nextval('elenco_clienti_id_elenco_clienti_seq'::regclass) NOT NULL,
    elenco_clienti character varying DEFAULT '----------'::character varying NOT NULL
);


ALTER TABLE elenco_clienti OWNER TO valpenguin;

--
-- TOC entry 185 (class 1259 OID 16999)
-- Name: gruppi; Type: TABLE; Schema: public; Owner: valpenguin
--

CREATE TABLE gruppi (
    id_gruppi integer NOT NULL,
    nome_gruppo character varying(510) DEFAULT NULL::character varying,
    email_gruppo character varying(510) DEFAULT NULL::character varying
);


ALTER TABLE gruppi OWNER TO valpenguin;

--
-- TOC entry 186 (class 1259 OID 17007)
-- Name: gruppi_id_gruppi_seq; Type: SEQUENCE; Schema: public; Owner: valpenguin
--

CREATE SEQUENCE gruppi_id_gruppi_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE gruppi_id_gruppi_seq OWNER TO valpenguin;

--
-- TOC entry 2277 (class 0 OID 0)
-- Dependencies: 186
-- Name: gruppi_id_gruppi_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: valpenguin
--

ALTER SEQUENCE gruppi_id_gruppi_seq OWNED BY gruppi.id_gruppi;


--
-- TOC entry 187 (class 1259 OID 17009)
-- Name: incident; Type: TABLE; Schema: public; Owner: valpenguin
--

CREATE TABLE incident (
    id_inc integer NOT NULL,
    inc_data_ora_iniziale timestamp(6) without time zone,
    inc_data_ora_chiusura timestamp(6) without time zone,
    "cliente impattato" character varying(150) DEFAULT NULL::character varying,
    descrizione_incident character varying(510) DEFAULT NULL::character varying,
    perimetro_27001 boolean NOT NULL,
    oggetto_incident character varying(510) DEFAULT NULL::character varying,
    impatto_normalizzato character varying(510) DEFAULT NULL::character varying,
    gruppo_risolutore character varying(150) DEFAULT NULL::character varying,
    risoluzione character varying(510) DEFAULT NULL::character varying,
    inc_gravita character varying(150) DEFAULT NULL::character varying,
    tipologia_causa character varying(150),
    identificativo_remedy character varying(150),
    note_incident character varying(510),
    segnalato_da character varying(150)
);


ALTER TABLE incident OWNER TO valpenguin;

--
-- TOC entry 188 (class 1259 OID 17022)
-- Name: incident_id_inc_seq; Type: SEQUENCE; Schema: public; Owner: valpenguin
--

CREATE SEQUENCE incident_id_inc_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE incident_id_inc_seq OWNER TO valpenguin;

--
-- TOC entry 2278 (class 0 OID 0)
-- Dependencies: 188
-- Name: incident_id_inc_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: valpenguin
--

ALTER SEQUENCE incident_id_inc_seq OWNED BY incident.id_inc;


--
-- TOC entry 189 (class 1259 OID 17024)
-- Name: interventi; Type: TABLE; Schema: public; Owner: valpenguin
--

CREATE TABLE interventi (
    id_int integer NOT NULL,
    int_creato_il timestamp(6) without time zone,
    int_chiuso_il timestamp(6) without time zone,
    stato character varying(110) DEFAULT NULL::character varying,
    incident character varying(510) DEFAULT NULL::character varying,
    note character varying(510) DEFAULT NULL::character varying,
    nome_gruppo character varying(510) DEFAULT NULL::character varying,
    attivita character varying(510) DEFAULT NULL::character varying,
    esito character varying(510) DEFAULT NULL::character varying,
    file_allegato character varying(510) DEFAULT NULL::character varying,
    int_link_kb character varying(512)
);


ALTER TABLE interventi OWNER TO valpenguin;

--
-- TOC entry 190 (class 1259 OID 17037)
-- Name: interventi_id_int_seq; Type: SEQUENCE; Schema: public; Owner: valpenguin
--

CREATE SEQUENCE interventi_id_int_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE interventi_id_int_seq OWNER TO valpenguin;

--
-- TOC entry 2279 (class 0 OID 0)
-- Dependencies: 190
-- Name: interventi_id_int_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: valpenguin
--

ALTER SEQUENCE interventi_id_int_seq OWNED BY interventi.id_int;


--
-- TOC entry 204 (class 1259 OID 17203)
-- Name: pg-incman_uggroups; Type: TABLE; Schema: public; Owner: valpenguin
--

CREATE TABLE "pg-incman_uggroups" (
    "GroupID" integer NOT NULL,
    "Label" character varying
);


ALTER TABLE "pg-incman_uggroups" OWNER TO valpenguin;

--
-- TOC entry 203 (class 1259 OID 17201)
-- Name: pg-incman_uggroups_GroupID_seq; Type: SEQUENCE; Schema: public; Owner: valpenguin
--

CREATE SEQUENCE "pg-incman_uggroups_GroupID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "pg-incman_uggroups_GroupID_seq" OWNER TO valpenguin;

--
-- TOC entry 2280 (class 0 OID 0)
-- Dependencies: 203
-- Name: pg-incman_uggroups_GroupID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: valpenguin
--

ALTER SEQUENCE "pg-incman_uggroups_GroupID_seq" OWNED BY "pg-incman_uggroups"."GroupID";


--
-- TOC entry 205 (class 1259 OID 17212)
-- Name: pg-incman_ugmembers; Type: TABLE; Schema: public; Owner: valpenguin
--

CREATE TABLE "pg-incman_ugmembers" (
    "UserName" character varying NOT NULL,
    "GroupID" integer NOT NULL
);


ALTER TABLE "pg-incman_ugmembers" OWNER TO valpenguin;

--
-- TOC entry 206 (class 1259 OID 17220)
-- Name: pg-incman_ugrights; Type: TABLE; Schema: public; Owner: valpenguin
--

CREATE TABLE "pg-incman_ugrights" (
    "TableName" character varying NOT NULL,
    "GroupID" integer NOT NULL,
    "AccessMask" character varying
);


ALTER TABLE "pg-incman_ugrights" OWNER TO valpenguin;

--
-- TOC entry 202 (class 1259 OID 17192)
-- Name: pg-incman_utenti; Type: TABLE; Schema: public; Owner: valpenguin
--

CREATE TABLE "pg-incman_utenti" (
    "ID" integer NOT NULL,
    username character varying,
    password character varying,
    email character varying,
    fullname character varying,
    groupid character varying,
    active integer
);


ALTER TABLE "pg-incman_utenti" OWNER TO valpenguin;

--
-- TOC entry 201 (class 1259 OID 17190)
-- Name: pg-incman_utenti_ID_seq; Type: SEQUENCE; Schema: public; Owner: valpenguin
--

CREATE SEQUENCE "pg-incman_utenti_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "pg-incman_utenti_ID_seq" OWNER TO valpenguin;

--
-- TOC entry 2281 (class 0 OID 0)
-- Dependencies: 201
-- Name: pg-incman_utenti_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: valpenguin
--

ALTER SEQUENCE "pg-incman_utenti_ID_seq" OWNED BY "pg-incman_utenti"."ID";


--
-- TOC entry 195 (class 1259 OID 17118)
-- Name: stato_gravita_id_stato_gravita_seq; Type: SEQUENCE; Schema: public; Owner: valpenguin
--

CREATE SEQUENCE stato_gravita_id_stato_gravita_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE stato_gravita_id_stato_gravita_seq OWNER TO valpenguin;

--
-- TOC entry 196 (class 1259 OID 17120)
-- Name: stato_gravita; Type: TABLE; Schema: public; Owner: valpenguin
--

CREATE TABLE stato_gravita (
    id_stato_gravita integer DEFAULT nextval('stato_gravita_id_stato_gravita_seq'::regclass) NOT NULL,
    livello_gravita character varying DEFAULT '----------'::character varying NOT NULL
);


ALTER TABLE stato_gravita OWNER TO valpenguin;

--
-- TOC entry 191 (class 1259 OID 17039)
-- Name: stato_incident; Type: TABLE; Schema: public; Owner: valpenguin
--

CREATE TABLE stato_incident (
    id_stato_inc integer NOT NULL,
    stato_incident character varying DEFAULT '----------'::character varying NOT NULL
);


ALTER TABLE stato_incident OWNER TO valpenguin;

--
-- TOC entry 192 (class 1259 OID 17046)
-- Name: stato_incident_id_stato_inc_seq; Type: SEQUENCE; Schema: public; Owner: valpenguin
--

CREATE SEQUENCE stato_incident_id_stato_inc_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE stato_incident_id_stato_inc_seq OWNER TO valpenguin;

--
-- TOC entry 2282 (class 0 OID 0)
-- Dependencies: 192
-- Name: stato_incident_id_stato_inc_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: valpenguin
--

ALTER SEQUENCE stato_incident_id_stato_inc_seq OWNED BY stato_incident.id_stato_inc;


--
-- TOC entry 193 (class 1259 OID 17048)
-- Name: stato_interventi; Type: TABLE; Schema: public; Owner: valpenguin
--

CREATE TABLE stato_interventi (
    id_stato_int integer NOT NULL,
    stato_intervento character varying DEFAULT '----------'::character varying NOT NULL
);


ALTER TABLE stato_interventi OWNER TO valpenguin;

--
-- TOC entry 194 (class 1259 OID 17055)
-- Name: stato_interventi_id_stato_int_seq; Type: SEQUENCE; Schema: public; Owner: valpenguin
--

CREATE SEQUENCE stato_interventi_id_stato_int_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE stato_interventi_id_stato_int_seq OWNER TO valpenguin;

--
-- TOC entry 2283 (class 0 OID 0)
-- Dependencies: 194
-- Name: stato_interventi_id_stato_int_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: valpenguin
--

ALTER SEQUENCE stato_interventi_id_stato_int_seq OWNED BY stato_interventi.id_stato_int;


--
-- TOC entry 197 (class 1259 OID 17130)
-- Name: tipo_causa_id_tipo_causa_seq; Type: SEQUENCE; Schema: public; Owner: valpenguin
--

CREATE SEQUENCE tipo_causa_id_tipo_causa_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tipo_causa_id_tipo_causa_seq OWNER TO valpenguin;

--
-- TOC entry 198 (class 1259 OID 17132)
-- Name: tipo_causa; Type: TABLE; Schema: public; Owner: valpenguin
--

CREATE TABLE tipo_causa (
    id_tipo_causa integer DEFAULT nextval('tipo_causa_id_tipo_causa_seq'::regclass) NOT NULL,
    tipo_causa character varying DEFAULT '----------'::character varying NOT NULL
);


ALTER TABLE tipo_causa OWNER TO valpenguin;

--
-- TOC entry 2077 (class 2604 OID 17057)
-- Name: gruppi id_gruppi; Type: DEFAULT; Schema: public; Owner: valpenguin
--

ALTER TABLE ONLY gruppi ALTER COLUMN id_gruppi SET DEFAULT nextval('gruppi_id_gruppi_seq'::regclass);


--
-- TOC entry 2082 (class 2604 OID 17058)
-- Name: incident id_inc; Type: DEFAULT; Schema: public; Owner: valpenguin
--

ALTER TABLE ONLY incident ALTER COLUMN id_inc SET DEFAULT nextval('incident_id_inc_seq'::regclass);


--
-- TOC entry 2093 (class 2604 OID 17059)
-- Name: interventi id_int; Type: DEFAULT; Schema: public; Owner: valpenguin
--

ALTER TABLE ONLY interventi ALTER COLUMN id_int SET DEFAULT nextval('interventi_id_int_seq'::regclass);


--
-- TOC entry 2105 (class 2604 OID 17206)
-- Name: pg-incman_uggroups GroupID; Type: DEFAULT; Schema: public; Owner: valpenguin
--

ALTER TABLE ONLY "pg-incman_uggroups" ALTER COLUMN "GroupID" SET DEFAULT nextval('"pg-incman_uggroups_GroupID_seq"'::regclass);


--
-- TOC entry 2104 (class 2604 OID 17195)
-- Name: pg-incman_utenti ID; Type: DEFAULT; Schema: public; Owner: valpenguin
--

ALTER TABLE ONLY "pg-incman_utenti" ALTER COLUMN "ID" SET DEFAULT nextval('"pg-incman_utenti_ID_seq"'::regclass);


--
-- TOC entry 2095 (class 2604 OID 17060)
-- Name: stato_incident id_stato_inc; Type: DEFAULT; Schema: public; Owner: valpenguin
--

ALTER TABLE ONLY stato_incident ALTER COLUMN id_stato_inc SET DEFAULT nextval('stato_incident_id_stato_inc_seq'::regclass);


--
-- TOC entry 2097 (class 2604 OID 17061)
-- Name: stato_interventi id_stato_int; Type: DEFAULT; Schema: public; Owner: valpenguin
--

ALTER TABLE ONLY stato_interventi ALTER COLUMN id_stato_int SET DEFAULT nextval('stato_interventi_id_stato_int_seq'::regclass);


--
-- TOC entry 2262 (class 0 OID 17144)
-- Dependencies: 200
-- Data for Name: elenco_clienti; Type: TABLE DATA; Schema: public; Owner: valpenguin
--

INSERT INTO elenco_clienti (id_elenco_clienti, elenco_clienti) VALUES (1, 'Regione Piemonte');
INSERT INTO elenco_clienti (id_elenco_clienti, elenco_clienti) VALUES (2, 'Comune di Torino');
INSERT INTO elenco_clienti (id_elenco_clienti, elenco_clienti) VALUES (4, 'Consiglio Regionale');
INSERT INTO elenco_clienti (id_elenco_clienti, elenco_clienti) VALUES (5, 'Centro Trapianti');
INSERT INTO elenco_clienti (id_elenco_clienti, elenco_clienti) VALUES (6, 'UPO');
INSERT INTO elenco_clienti (id_elenco_clienti, elenco_clienti) VALUES (7, 'ARPA');
INSERT INTO elenco_clienti (id_elenco_clienti, elenco_clienti) VALUES (8, 'AIPO');
INSERT INTO elenco_clienti (id_elenco_clienti, elenco_clienti) VALUES (9, 'ARPEA');
INSERT INTO elenco_clienti (id_elenco_clienti, elenco_clienti) VALUES (10, 'Comune di Nichelino');
INSERT INTO elenco_clienti (id_elenco_clienti, elenco_clienti) VALUES (11, 'Comune di Collegno');
INSERT INTO elenco_clienti (id_elenco_clienti, elenco_clienti) VALUES (12, 'Comune di Biella');
INSERT INTO elenco_clienti (id_elenco_clienti, elenco_clienti) VALUES (13, 'Comune di Fossano');
INSERT INTO elenco_clienti (id_elenco_clienti, elenco_clienti) VALUES (14, 'Comune di Pinerolo');
INSERT INTO elenco_clienti (id_elenco_clienti, elenco_clienti) VALUES (15, 'Comune di Vercelli');
INSERT INTO elenco_clienti (id_elenco_clienti, elenco_clienti) VALUES (16, 'Comune di Alessandria');
INSERT INTO elenco_clienti (id_elenco_clienti, elenco_clienti) VALUES (17, 'Comune di Cuneo');
INSERT INTO elenco_clienti (id_elenco_clienti, elenco_clienti) VALUES (18, 'Ospedale Mauriziano');
INSERT INTO elenco_clienti (id_elenco_clienti, elenco_clienti) VALUES (19, 'Provincia');
INSERT INTO elenco_clienti (id_elenco_clienti, elenco_clienti) VALUES (20, 'VCO');
INSERT INTO elenco_clienti (id_elenco_clienti, elenco_clienti) VALUES (21, 'Servizio 118');
INSERT INTO elenco_clienti (id_elenco_clienti, elenco_clienti) VALUES (23, 'Enti Locali');
INSERT INTO elenco_clienti (id_elenco_clienti, elenco_clienti) VALUES (24, 'TOPIX');
INSERT INTO elenco_clienti (id_elenco_clienti, elenco_clienti) VALUES (25, 'Comune Cuneo CSI');
INSERT INTO elenco_clienti (id_elenco_clienti, elenco_clienti) VALUES (3, 'Citta''  Metropolitana');
INSERT INTO elenco_clienti (id_elenco_clienti, elenco_clienti) VALUES (22, 'Sanita''');


--
-- TOC entry 2247 (class 0 OID 16999)
-- Dependencies: 185
-- Data for Name: gruppi; Type: TABLE DATA; Schema: public; Owner: valpenguin
--

INSERT INTO gruppi (id_gruppi, nome_gruppo, email_gruppo) VALUES (1, 'Sistemisti-Unix', 'sistemisti-unix@csi.it');
INSERT INTO gruppi (id_gruppi, nome_gruppo, email_gruppo) VALUES (2, 'Sistemisti-MDweb', 'sistemisti-mdweb@csi.it');
INSERT INTO gruppi (id_gruppi, nome_gruppo, email_gruppo) VALUES (3, 'Sistemisti-Posta', 'sistemisti-posta@csi.it');
INSERT INTO gruppi (id_gruppi, nome_gruppo, email_gruppo) VALUES (4, 'Tlc-Sicurezza Gruppo', 'tlc-sicurezza@csi.it');
INSERT INTO gruppi (id_gruppi, nome_gruppo, email_gruppo) VALUES (5, 'Gestione-database', 'gestione.database@csi.it');
INSERT INTO gruppi (id_gruppi, nome_gruppo, email_gruppo) VALUES (6, 'Assistenza-Oracle', 'assistenza.oracle@csi.it');
INSERT INTO gruppi (id_gruppi, nome_gruppo, email_gruppo) VALUES (7, 'Assistenza-Operativa', 'assistenza.operativa@csi.it');


--
-- TOC entry 2249 (class 0 OID 17009)
-- Dependencies: 187
-- Data for Name: incident; Type: TABLE DATA; Schema: public; Owner: valpenguin
--

INSERT INTO incident (id_inc, inc_data_ora_iniziale, inc_data_ora_chiusura, "cliente impattato", descrizione_incident, perimetro_27001, oggetto_incident, impatto_normalizzato, gruppo_risolutore, risoluzione, inc_gravita, tipologia_causa, identificativo_remedy, note_incident, segnalato_da) VALUES (3, '2019-02-25 11:30:00', '2019-02-25 11:54:00', 'Regione Piemonte,ARPA', 'Applicativo SICEE bloccato', false, 'SICEE', 'Applicativo SICEE bloccato', 'Sistemisti-MDweb', 'Riavvio server bea 1,2,3 ', 'Major', 'Applicativa', 'INC00003054005', 'Servizio ripristinato', 'ZABBIX');
INSERT INTO incident (id_inc, inc_data_ora_iniziale, inc_data_ora_chiusura, "cliente impattato", descrizione_incident, perimetro_27001, oggetto_incident, impatto_normalizzato, gruppo_risolutore, risoluzione, inc_gravita, tipologia_causa, identificativo_remedy, note_incident, segnalato_da) VALUES (2, '2019-02-22 03:08:00', '2019-02-22 13:50:00', 'Città  Metropolitana', 'descrizione incident', true, 'qui impatto', 'normalizzo app.', 'Sistemisti-Unix', 'risolvo', 'Hot', 'Probl. Elettrico', 'INC00000300000', 'note', 'chi te pare');
INSERT INTO incident (id_inc, inc_data_ora_iniziale, inc_data_ora_chiusura, "cliente impattato", descrizione_incident, perimetro_27001, oggetto_incident, impatto_normalizzato, gruppo_risolutore, risoluzione, inc_gravita, tipologia_causa, identificativo_remedy, note_incident, segnalato_da) VALUES (4, '2019-03-06 00:00:00', '2019-03-06 00:00:00', 'Comune di Torino', 'Descrizione', false, 'Impatto oggetto', 'Impatto normalizzato', 'Sistemisti-Unix', 'Riavvio', 'Event', 'Software Applicativo', 'INC48959854', 'Nota incident', 'Munin');


--
-- TOC entry 2251 (class 0 OID 17024)
-- Dependencies: 189
-- Data for Name: interventi; Type: TABLE DATA; Schema: public; Owner: valpenguin
--

INSERT INTO interventi (id_int, int_creato_il, int_chiuso_il, stato, incident, note, nome_gruppo, attivita, esito, file_allegato, int_link_kb) VALUES (2, '2019-02-22 09:05:00', '2019-02-23 11:00:00', 'Terminato', '2', 'Vedi allegato per soluzione operativa.', 'Sistemisti-Unix', 'Attività svolta', 'Risolto', '[{"name":"files\/cmd_san_f2k8yi0k.txt","usrName":"cmd_san.txt","size":12063,"type":"text\/plain","searchStr":"cmd_san.txt,!:sStrEnd"}]', '');
INSERT INTO interventi (id_int, int_creato_il, int_chiuso_il, stato, incident, note, nome_gruppo, attivita, esito, file_allegato, int_link_kb) VALUES (3, '2019-02-25 11:40:00', '2019-02-25 11:40:00', 'Terminato', '3', 'Servizio ripristinato', 'Sistemisti-MDweb', 'Eseguito  riavvio server bea 1,2,3 ', 'Servizio ripristinato', '[{"name":"files\/riavvio server bea_soa3rq5v.pdf","usrName":"riavvio server bea.pdf","size":42578,"type":"application\/pdf","searchStr":"riavvio server bea.pdf,!:sStrEnd"}]', 'https://docs.oracle.com/cd/E28280_01/web.1111/e13708/overview.htm#START112');
INSERT INTO interventi (id_int, int_creato_il, int_chiuso_il, stato, incident, note, nome_gruppo, attivita, esito, file_allegato, int_link_kb) VALUES (7, '2019-03-11 00:00:00', '2019-03-11 00:00:00', 'Sospeso su richiesta del cliente', '4', 'test note', 'Sistemisti-Unix', 'Attesa feedback cliente', 'esito', '', '');


--
-- TOC entry 2266 (class 0 OID 17203)
-- Dependencies: 204
-- Data for Name: pg-incman_uggroups; Type: TABLE DATA; Schema: public; Owner: valpenguin
--

INSERT INTO "pg-incman_uggroups" ("GroupID", "Label") VALUES (2, 'read-only');
INSERT INTO "pg-incman_uggroups" ("GroupID", "Label") VALUES (1, 'gruppi-operativi');


--
-- TOC entry 2267 (class 0 OID 17212)
-- Dependencies: 205
-- Data for Name: pg-incman_ugmembers; Type: TABLE DATA; Schema: public; Owner: valpenguin
--

INSERT INTO "pg-incman_ugmembers" ("UserName", "GroupID") VALUES ('admin', -1);
INSERT INTO "pg-incman_ugmembers" ("UserName", "GroupID") VALUES ('sistemisti-unix', 1);
INSERT INTO "pg-incman_ugmembers" ("UserName", "GroupID") VALUES ('guest', 2);


--
-- TOC entry 2268 (class 0 OID 17220)
-- Dependencies: 206
-- Data for Name: pg-incman_ugrights; Type: TABLE DATA; Schema: public; Owner: valpenguin
--

INSERT INTO "pg-incman_ugrights" ("TableName", "GroupID", "AccessMask") VALUES ('public.gruppi', -1, 'ADESPIM');
INSERT INTO "pg-incman_ugrights" ("TableName", "GroupID", "AccessMask") VALUES ('public.interventi', -1, 'ADESPIM');
INSERT INTO "pg-incman_ugrights" ("TableName", "GroupID", "AccessMask") VALUES ('public.stato_incident', -1, 'ADESPIM');
INSERT INTO "pg-incman_ugrights" ("TableName", "GroupID", "AccessMask") VALUES ('public.stato_interventi', -1, 'ADESPIM');
INSERT INTO "pg-incman_ugrights" ("TableName", "GroupID", "AccessMask") VALUES ('public.incident', -1, 'ADESPIM');
INSERT INTO "pg-incman_ugrights" ("TableName", "GroupID", "AccessMask") VALUES ('public.tipo_causa', -1, 'ADESPIM');
INSERT INTO "pg-incman_ugrights" ("TableName", "GroupID", "AccessMask") VALUES ('public.elenco_clienti', -1, 'ADESPIM');
INSERT INTO "pg-incman_ugrights" ("TableName", "GroupID", "AccessMask") VALUES ('public.stato_gravita', -1, 'ADESPIM');
INSERT INTO "pg-incman_ugrights" ("TableName", "GroupID", "AccessMask") VALUES ('public.incident', 1, 'SP');
INSERT INTO "pg-incman_ugrights" ("TableName", "GroupID", "AccessMask") VALUES ('public.interventi', 1, 'AESP');
INSERT INTO "pg-incman_ugrights" ("TableName", "GroupID", "AccessMask") VALUES ('public.incident', 2, 'SP');
INSERT INTO "pg-incman_ugrights" ("TableName", "GroupID", "AccessMask") VALUES ('public.stato_incident', 2, 'S');
INSERT INTO "pg-incman_ugrights" ("TableName", "GroupID", "AccessMask") VALUES ('public.stato_gravita', 2, 'S');
INSERT INTO "pg-incman_ugrights" ("TableName", "GroupID", "AccessMask") VALUES ('public.tipo_causa', 2, 'S');
INSERT INTO "pg-incman_ugrights" ("TableName", "GroupID", "AccessMask") VALUES ('public.interventi', 2, 'SP');
INSERT INTO "pg-incman_ugrights" ("TableName", "GroupID", "AccessMask") VALUES ('public.stato_interventi', 2, 'S');
INSERT INTO "pg-incman_ugrights" ("TableName", "GroupID", "AccessMask") VALUES ('public.gruppi', 2, 'S');
INSERT INTO "pg-incman_ugrights" ("TableName", "GroupID", "AccessMask") VALUES ('public.elenco_clienti', 2, 'S');


--
-- TOC entry 2264 (class 0 OID 17192)
-- Dependencies: 202
-- Data for Name: pg-incman_utenti; Type: TABLE DATA; Schema: public; Owner: valpenguin
--

INSERT INTO "pg-incman_utenti" ("ID", username, password, email, fullname, groupid, active) VALUES (1, 'admin', 'admin123', 'change_management.datacenter@csi.it', 'Change&Incident MD', NULL, 1);
INSERT INTO "pg-incman_utenti" ("ID", username, password, email, fullname, groupid, active) VALUES (3, 'guest', 'guest123', 'guest@csi.it', 'Ospite', '', 1);
INSERT INTO "pg-incman_utenti" ("ID", username, password, email, fullname, groupid, active) VALUES (2, 'sistemisti-unix', 'Solaris1', 'sistemisti-unix@csi.it', 'Gruppo Sistemisti-Unix', '', 1);


--
-- TOC entry 2258 (class 0 OID 17120)
-- Dependencies: 196
-- Data for Name: stato_gravita; Type: TABLE DATA; Schema: public; Owner: valpenguin
--

INSERT INTO stato_gravita (id_stato_gravita, livello_gravita) VALUES (4, 'Hot');
INSERT INTO stato_gravita (id_stato_gravita, livello_gravita) VALUES (3, 'Major');
INSERT INTO stato_gravita (id_stato_gravita, livello_gravita) VALUES (2, 'Minor');
INSERT INTO stato_gravita (id_stato_gravita, livello_gravita) VALUES (1, 'Event');


--
-- TOC entry 2253 (class 0 OID 17039)
-- Dependencies: 191
-- Data for Name: stato_incident; Type: TABLE DATA; Schema: public; Owner: valpenguin
--

INSERT INTO stato_incident (id_stato_inc, stato_incident) VALUES (2, 'Aperto');
INSERT INTO stato_incident (id_stato_inc, stato_incident) VALUES (7, 'Chiuso');
INSERT INTO stato_incident (id_stato_inc, stato_incident) VALUES (8, 'Sospeso');
INSERT INTO stato_incident (id_stato_inc, stato_incident) VALUES (1, 'Sospeso su richiesta del cliente');
INSERT INTO stato_incident (id_stato_inc, stato_incident) VALUES (10, 'Sospeso per intervento fornitore');


--
-- TOC entry 2255 (class 0 OID 17048)
-- Dependencies: 193
-- Data for Name: stato_interventi; Type: TABLE DATA; Schema: public; Owner: valpenguin
--

INSERT INTO stato_interventi (id_stato_int, stato_intervento) VALUES (2, 'In corso');
INSERT INTO stato_interventi (id_stato_int, stato_intervento) VALUES (3, 'Sospeso');
INSERT INTO stato_interventi (id_stato_int, stato_intervento) VALUES (4, 'Terminato');
INSERT INTO stato_interventi (id_stato_int, stato_intervento) VALUES (1, 'Sospeso su richiesta del cliente');
INSERT INTO stato_interventi (id_stato_int, stato_intervento) VALUES (5, 'Sospeso per intervento fornitore');


--
-- TOC entry 2260 (class 0 OID 17132)
-- Dependencies: 198
-- Data for Name: tipo_causa; Type: TABLE DATA; Schema: public; Owner: valpenguin
--

INSERT INTO tipo_causa (id_tipo_causa, tipo_causa) VALUES (1, 'Applicativa');
INSERT INTO tipo_causa (id_tipo_causa, tipo_causa) VALUES (2, 'Esterna');
INSERT INTO tipo_causa (id_tipo_causa, tipo_causa) VALUES (3, 'Attacco informatico');
INSERT INTO tipo_causa (id_tipo_causa, tipo_causa) VALUES (4, 'Probl. Storage');
INSERT INTO tipo_causa (id_tipo_causa, tipo_causa) VALUES (5, 'Probl. FileSystem');
INSERT INTO tipo_causa (id_tipo_causa, tipo_causa) VALUES (6, 'Probl. Database');
INSERT INTO tipo_causa (id_tipo_causa, tipo_causa) VALUES (8, 'Probl. Software');
INSERT INTO tipo_causa (id_tipo_causa, tipo_causa) VALUES (9, 'Backup');
INSERT INTO tipo_causa (id_tipo_causa, tipo_causa) VALUES (10, 'Guasto elettrico');
INSERT INTO tipo_causa (id_tipo_causa, tipo_causa) VALUES (11, 'Errore umano');
INSERT INTO tipo_causa (id_tipo_causa, tipo_causa) VALUES (12, 'Software di base');
INSERT INTO tipo_causa (id_tipo_causa, tipo_causa) VALUES (13, 'Software Applicativo');
INSERT INTO tipo_causa (id_tipo_causa, tipo_causa) VALUES (7, 'Probl. Elettrico');
INSERT INTO tipo_causa (id_tipo_causa, tipo_causa) VALUES (14, 'Probl. Rete');


--
-- TOC entry 2284 (class 0 OID 0)
-- Dependencies: 199
-- Name: elenco_clienti_id_elenco_clienti_seq; Type: SEQUENCE SET; Schema: public; Owner: valpenguin
--

SELECT pg_catalog.setval('elenco_clienti_id_elenco_clienti_seq', 25, true);


--
-- TOC entry 2285 (class 0 OID 0)
-- Dependencies: 186
-- Name: gruppi_id_gruppi_seq; Type: SEQUENCE SET; Schema: public; Owner: valpenguin
--

SELECT pg_catalog.setval('gruppi_id_gruppi_seq', 7, true);


--
-- TOC entry 2286 (class 0 OID 0)
-- Dependencies: 188
-- Name: incident_id_inc_seq; Type: SEQUENCE SET; Schema: public; Owner: valpenguin
--

SELECT pg_catalog.setval('incident_id_inc_seq', 12, true);


--
-- TOC entry 2287 (class 0 OID 0)
-- Dependencies: 190
-- Name: interventi_id_int_seq; Type: SEQUENCE SET; Schema: public; Owner: valpenguin
--

SELECT pg_catalog.setval('interventi_id_int_seq', 7, true);


--
-- TOC entry 2288 (class 0 OID 0)
-- Dependencies: 203
-- Name: pg-incman_uggroups_GroupID_seq; Type: SEQUENCE SET; Schema: public; Owner: valpenguin
--

SELECT pg_catalog.setval('"pg-incman_uggroups_GroupID_seq"', 2, true);


--
-- TOC entry 2289 (class 0 OID 0)
-- Dependencies: 201
-- Name: pg-incman_utenti_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: valpenguin
--

SELECT pg_catalog.setval('"pg-incman_utenti_ID_seq"', 3, true);


--
-- TOC entry 2290 (class 0 OID 0)
-- Dependencies: 195
-- Name: stato_gravita_id_stato_gravita_seq; Type: SEQUENCE SET; Schema: public; Owner: valpenguin
--

SELECT pg_catalog.setval('stato_gravita_id_stato_gravita_seq', 4, true);


--
-- TOC entry 2291 (class 0 OID 0)
-- Dependencies: 192
-- Name: stato_incident_id_stato_inc_seq; Type: SEQUENCE SET; Schema: public; Owner: valpenguin
--

SELECT pg_catalog.setval('stato_incident_id_stato_inc_seq', 10, true);


--
-- TOC entry 2292 (class 0 OID 0)
-- Dependencies: 194
-- Name: stato_interventi_id_stato_int_seq; Type: SEQUENCE SET; Schema: public; Owner: valpenguin
--

SELECT pg_catalog.setval('stato_interventi_id_stato_int_seq', 5, true);


--
-- TOC entry 2293 (class 0 OID 0)
-- Dependencies: 197
-- Name: tipo_causa_id_tipo_causa_seq; Type: SEQUENCE SET; Schema: public; Owner: valpenguin
--

SELECT pg_catalog.setval('tipo_causa_id_tipo_causa_seq', 14, true);


--
-- TOC entry 2121 (class 2606 OID 17153)
-- Name: elenco_clienti elenco_clienti_pkey; Type: CONSTRAINT; Schema: public; Owner: valpenguin
--

ALTER TABLE ONLY elenco_clienti
    ADD CONSTRAINT elenco_clienti_pkey PRIMARY KEY (id_elenco_clienti);


--
-- TOC entry 2107 (class 2606 OID 17063)
-- Name: gruppi gruppi_pkey; Type: CONSTRAINT; Schema: public; Owner: valpenguin
--

ALTER TABLE ONLY gruppi
    ADD CONSTRAINT gruppi_pkey PRIMARY KEY (id_gruppi);


--
-- TOC entry 2109 (class 2606 OID 17065)
-- Name: incident incident_pkey; Type: CONSTRAINT; Schema: public; Owner: valpenguin
--

ALTER TABLE ONLY incident
    ADD CONSTRAINT incident_pkey PRIMARY KEY (id_inc);


--
-- TOC entry 2111 (class 2606 OID 17067)
-- Name: interventi interventi_pkey; Type: CONSTRAINT; Schema: public; Owner: valpenguin
--

ALTER TABLE ONLY interventi
    ADD CONSTRAINT interventi_pkey PRIMARY KEY (id_int);


--
-- TOC entry 2125 (class 2606 OID 17211)
-- Name: pg-incman_uggroups pg-incman_uggroups_pkey; Type: CONSTRAINT; Schema: public; Owner: valpenguin
--

ALTER TABLE ONLY "pg-incman_uggroups"
    ADD CONSTRAINT "pg-incman_uggroups_pkey" PRIMARY KEY ("GroupID");


--
-- TOC entry 2127 (class 2606 OID 17219)
-- Name: pg-incman_ugmembers pg-incman_ugmembers_pkey; Type: CONSTRAINT; Schema: public; Owner: valpenguin
--

ALTER TABLE ONLY "pg-incman_ugmembers"
    ADD CONSTRAINT "pg-incman_ugmembers_pkey" PRIMARY KEY ("UserName", "GroupID");


--
-- TOC entry 2129 (class 2606 OID 17227)
-- Name: pg-incman_ugrights pg-incman_ugrights_pkey; Type: CONSTRAINT; Schema: public; Owner: valpenguin
--

ALTER TABLE ONLY "pg-incman_ugrights"
    ADD CONSTRAINT "pg-incman_ugrights_pkey" PRIMARY KEY ("TableName", "GroupID");


--
-- TOC entry 2123 (class 2606 OID 17200)
-- Name: pg-incman_utenti pg-incman_utenti_pkey; Type: CONSTRAINT; Schema: public; Owner: valpenguin
--

ALTER TABLE ONLY "pg-incman_utenti"
    ADD CONSTRAINT "pg-incman_utenti_pkey" PRIMARY KEY ("ID");


--
-- TOC entry 2117 (class 2606 OID 17129)
-- Name: stato_gravita stato_gravita_pkey; Type: CONSTRAINT; Schema: public; Owner: valpenguin
--

ALTER TABLE ONLY stato_gravita
    ADD CONSTRAINT stato_gravita_pkey PRIMARY KEY (id_stato_gravita);


--
-- TOC entry 2113 (class 2606 OID 17069)
-- Name: stato_incident stato_incident_pkey; Type: CONSTRAINT; Schema: public; Owner: valpenguin
--

ALTER TABLE ONLY stato_incident
    ADD CONSTRAINT stato_incident_pkey PRIMARY KEY (id_stato_inc);


--
-- TOC entry 2115 (class 2606 OID 17071)
-- Name: stato_interventi stato_interventi_pkey; Type: CONSTRAINT; Schema: public; Owner: valpenguin
--

ALTER TABLE ONLY stato_interventi
    ADD CONSTRAINT stato_interventi_pkey PRIMARY KEY (id_stato_int);


--
-- TOC entry 2119 (class 2606 OID 17141)
-- Name: tipo_causa tipo_causa_pkey; Type: CONSTRAINT; Schema: public; Owner: valpenguin
--

ALTER TABLE ONLY tipo_causa
    ADD CONSTRAINT tipo_causa_pkey PRIMARY KEY (id_tipo_causa);


--
-- TOC entry 2275 (class 0 OID 0)
-- Dependencies: 3
-- Name: public; Type: ACL; Schema: -; Owner: valpenguin
--

REVOKE ALL ON SCHEMA public FROM postgres;
REVOKE ALL ON SCHEMA public FROM PUBLIC;
GRANT ALL ON SCHEMA public TO valpenguin;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2019-03-15 10:17:26

--
-- PostgreSQL database dump complete
--

