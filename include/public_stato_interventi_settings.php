<?php
require_once(getabspath("classes/cipherer.php"));




$tdatapublic_stato_interventi = array();
	$tdatapublic_stato_interventi[".truncateText"] = true;
	$tdatapublic_stato_interventi[".NumberOfChars"] = 80;
	$tdatapublic_stato_interventi[".ShortName"] = "public_stato_interventi";
	$tdatapublic_stato_interventi[".OwnerID"] = "";
	$tdatapublic_stato_interventi[".OriginalTable"] = "public.stato_interventi";

//	field labels
$fieldLabelspublic_stato_interventi = array();
$fieldToolTipspublic_stato_interventi = array();
$pageTitlespublic_stato_interventi = array();

if(mlang_getcurrentlang()=="Italian")
{
	$fieldLabelspublic_stato_interventi["Italian"] = array();
	$fieldToolTipspublic_stato_interventi["Italian"] = array();
	$pageTitlespublic_stato_interventi["Italian"] = array();
	$fieldLabelspublic_stato_interventi["Italian"]["Stato_intervento"] = "Stato intervento";
	$fieldToolTipspublic_stato_interventi["Italian"]["Stato_intervento"] = "";
	$fieldLabelspublic_stato_interventi["Italian"]["id_stato_int"] = "Id Stato Int";
	$fieldToolTipspublic_stato_interventi["Italian"]["id_stato_int"] = "";
	if (count($fieldToolTipspublic_stato_interventi["Italian"]))
		$tdatapublic_stato_interventi[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelspublic_stato_interventi[""] = array();
	$fieldToolTipspublic_stato_interventi[""] = array();
	$pageTitlespublic_stato_interventi[""] = array();
	$fieldLabelspublic_stato_interventi[""]["Stato_intervento"] = "Stato Intervento";
	$fieldToolTipspublic_stato_interventi[""]["Stato_intervento"] = "";
	$fieldLabelspublic_stato_interventi[""]["id_stato_int"] = "Id Stato Int";
	$fieldToolTipspublic_stato_interventi[""]["id_stato_int"] = "";
	if (count($fieldToolTipspublic_stato_interventi[""]))
		$tdatapublic_stato_interventi[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="English")
{
	$fieldLabelspublic_stato_interventi["English"] = array();
	$fieldToolTipspublic_stato_interventi["English"] = array();
	$pageTitlespublic_stato_interventi["English"] = array();
	$fieldLabelspublic_stato_interventi["English"]["Stato_intervento"] = "Stato Intervento";
	$fieldToolTipspublic_stato_interventi["English"]["Stato_intervento"] = "";
	$fieldLabelspublic_stato_interventi["English"]["id_stato_int"] = "Id Stato Int";
	$fieldToolTipspublic_stato_interventi["English"]["id_stato_int"] = "";
	if (count($fieldToolTipspublic_stato_interventi["English"]))
		$tdatapublic_stato_interventi[".isUseToolTips"] = true;
}


	$tdatapublic_stato_interventi[".NCSearch"] = true;



$tdatapublic_stato_interventi[".shortTableName"] = "public_stato_interventi";
$tdatapublic_stato_interventi[".nSecOptions"] = 0;
$tdatapublic_stato_interventi[".recsPerRowPrint"] = 1;
$tdatapublic_stato_interventi[".mainTableOwnerID"] = "";
$tdatapublic_stato_interventi[".moveNext"] = 1;
$tdatapublic_stato_interventi[".entityType"] = 0;

$tdatapublic_stato_interventi[".strOriginalTableName"] = "public.stato_interventi";

	



$tdatapublic_stato_interventi[".showAddInPopup"] = false;

$tdatapublic_stato_interventi[".showEditInPopup"] = false;

$tdatapublic_stato_interventi[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatapublic_stato_interventi[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatapublic_stato_interventi[".fieldsForRegister"] = array();

$tdatapublic_stato_interventi[".listAjax"] = false;

	$tdatapublic_stato_interventi[".audit"] = false;

	$tdatapublic_stato_interventi[".locking"] = false;

$tdatapublic_stato_interventi[".edit"] = true;
$tdatapublic_stato_interventi[".afterEditAction"] = 1;
$tdatapublic_stato_interventi[".closePopupAfterEdit"] = 1;
$tdatapublic_stato_interventi[".afterEditActionDetTable"] = "";

$tdatapublic_stato_interventi[".add"] = true;
$tdatapublic_stato_interventi[".afterAddAction"] = 1;
$tdatapublic_stato_interventi[".closePopupAfterAdd"] = 1;
$tdatapublic_stato_interventi[".afterAddActionDetTable"] = "";

$tdatapublic_stato_interventi[".list"] = true;





$tdatapublic_stato_interventi[".delete"] = true;

$tdatapublic_stato_interventi[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdatapublic_stato_interventi[".searchSaving"] = false;
//

$tdatapublic_stato_interventi[".showSearchPanel"] = true;
		$tdatapublic_stato_interventi[".flexibleSearch"] = true;

$tdatapublic_stato_interventi[".isUseAjaxSuggest"] = true;

$tdatapublic_stato_interventi[".rowHighlite"] = true;



$tdatapublic_stato_interventi[".addPageEvents"] = false;

// use timepicker for search panel
$tdatapublic_stato_interventi[".isUseTimeForSearch"] = false;



$tdatapublic_stato_interventi[".badgeColor"] = "4682B4";


$tdatapublic_stato_interventi[".allSearchFields"] = array();
$tdatapublic_stato_interventi[".filterFields"] = array();
$tdatapublic_stato_interventi[".requiredSearchFields"] = array();

$tdatapublic_stato_interventi[".allSearchFields"][] = "Stato intervento";
	

$tdatapublic_stato_interventi[".googleLikeFields"] = array();
$tdatapublic_stato_interventi[".googleLikeFields"][] = "Stato intervento";
$tdatapublic_stato_interventi[".googleLikeFields"][] = "id_stato_int";


$tdatapublic_stato_interventi[".advSearchFields"] = array();
$tdatapublic_stato_interventi[".advSearchFields"][] = "Stato intervento";
$tdatapublic_stato_interventi[".advSearchFields"][] = "id_stato_int";

$tdatapublic_stato_interventi[".tableType"] = "list";

$tdatapublic_stato_interventi[".printerPageOrientation"] = 0;
$tdatapublic_stato_interventi[".nPrinterPageScale"] = 100;

$tdatapublic_stato_interventi[".nPrinterSplitRecords"] = 40;

$tdatapublic_stato_interventi[".nPrinterPDFSplitRecords"] = 40;



$tdatapublic_stato_interventi[".geocodingEnabled"] = false;





$tdatapublic_stato_interventi[".listGridLayout"] = 3;





// view page pdf
$tdatapublic_stato_interventi[".isViewPagePDF"] = true;
$tdatapublic_stato_interventi[".isLandscapeViewPDFOrientation"] = 1;
$tdatapublic_stato_interventi[".isViewPagePDFFitToPage"] = 0;
$tdatapublic_stato_interventi[".nViewPagePDFScale"] = 100;

// print page pdf
$tdatapublic_stato_interventi[".isPrinterPagePDF"] = true;
$tdatapublic_stato_interventi[".isLandscapePrinterPagePDFOrientation"] = 1;
$tdatapublic_stato_interventi[".isPrinterPagePDFFitToPage"] = 0;
$tdatapublic_stato_interventi[".nPrinterPagePDFScale"] = 100;


$tdatapublic_stato_interventi[".pageSize"] = 20;

$tdatapublic_stato_interventi[".warnLeavingPages"] = true;



$tstrOrderBy = "ORDER BY id_stato_int DESC";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatapublic_stato_interventi[".strOrderBy"] = $tstrOrderBy;

$tdatapublic_stato_interventi[".orderindexes"] = array();
$tdatapublic_stato_interventi[".orderindexes"][] = array(2, (0 ? "ASC" : "DESC"), "id_stato_int");

$tdatapublic_stato_interventi[".sqlHead"] = "SELECT stato_intervento AS \"Stato intervento\",  id_stato_int";
$tdatapublic_stato_interventi[".sqlFrom"] = "FROM \"public\".stato_interventi";
$tdatapublic_stato_interventi[".sqlWhereExpr"] = "";
$tdatapublic_stato_interventi[".sqlTail"] = "";











//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatapublic_stato_interventi[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatapublic_stato_interventi[".arrGroupsPerPage"] = $arrGPP;

$tdatapublic_stato_interventi[".highlightSearchResults"] = true;

$tableKeyspublic_stato_interventi = array();
$tableKeyspublic_stato_interventi[] = "id_stato_int";
$tdatapublic_stato_interventi[".Keys"] = $tableKeyspublic_stato_interventi;

$tdatapublic_stato_interventi[".listFields"] = array();
$tdatapublic_stato_interventi[".listFields"][] = "Stato intervento";

$tdatapublic_stato_interventi[".hideMobileList"] = array();


$tdatapublic_stato_interventi[".viewFields"] = array();

$tdatapublic_stato_interventi[".addFields"] = array();
$tdatapublic_stato_interventi[".addFields"][] = "Stato intervento";

$tdatapublic_stato_interventi[".masterListFields"] = array();
$tdatapublic_stato_interventi[".masterListFields"][] = "Stato intervento";
$tdatapublic_stato_interventi[".masterListFields"][] = "id_stato_int";

$tdatapublic_stato_interventi[".inlineAddFields"] = array();

$tdatapublic_stato_interventi[".editFields"] = array();
$tdatapublic_stato_interventi[".editFields"][] = "Stato intervento";

$tdatapublic_stato_interventi[".inlineEditFields"] = array();

$tdatapublic_stato_interventi[".exportFields"] = array();

$tdatapublic_stato_interventi[".importFields"] = array();

$tdatapublic_stato_interventi[".printFields"] = array();

//	Stato intervento
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "Stato intervento";
	$fdata["GoodName"] = "Stato_intervento";
	$fdata["ownerTable"] = "public.stato_interventi";
	$fdata["Label"] = GetFieldLabel("public_stato_interventi","Stato_intervento");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
	
		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "stato_intervento";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "stato_intervento";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
			$edata["validateAs"]["basicValidate"][] = "DenyDuplicated";
	$edata["validateAs"]["customMessages"]["DenyDuplicated"] = array("message" => "Stato inserito %value% già presente!", "messageType" => "Text");

	
	//	End validation

	
			
	
		$edata["denyDuplicates"] = true;

	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_stato_interventi["Stato intervento"] = $fdata;
//	id_stato_int
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "id_stato_int";
	$fdata["GoodName"] = "id_stato_int";
	$fdata["ownerTable"] = "public.stato_interventi";
	$fdata["Label"] = GetFieldLabel("public_stato_interventi","id_stato_int");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "id_stato_int";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "id_stato_int";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "number";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_stato_interventi["id_stato_int"] = $fdata;


$tables_data["public.stato_interventi"]=&$tdatapublic_stato_interventi;
$field_labels["public_stato_interventi"] = &$fieldLabelspublic_stato_interventi;
$fieldToolTips["public_stato_interventi"] = &$fieldToolTipspublic_stato_interventi;
$page_titles["public_stato_interventi"] = &$pageTitlespublic_stato_interventi;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["public.stato_interventi"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["public.stato_interventi"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_public_stato_interventi()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "stato_intervento AS \"Stato intervento\",  id_stato_int";
$proto0["m_strFrom"] = "FROM \"public\".stato_interventi";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "ORDER BY id_stato_int DESC";
$proto0["m_strTail"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "stato_intervento",
	"m_strTable" => "public.stato_interventi",
	"m_srcTableName" => "public.stato_interventi"
));

$proto6["m_sql"] = "stato_intervento";
$proto6["m_srcTableName"] = "public.stato_interventi";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "Stato intervento";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "id_stato_int",
	"m_strTable" => "public.stato_interventi",
	"m_srcTableName" => "public.stato_interventi"
));

$proto8["m_sql"] = "id_stato_int";
$proto8["m_srcTableName"] = "public.stato_interventi";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto10=array();
$proto10["m_link"] = "SQLL_MAIN";
			$proto11=array();
$proto11["m_strName"] = "public.stato_interventi";
$proto11["m_srcTableName"] = "public.stato_interventi";
$proto11["m_columns"] = array();
$proto11["m_columns"][] = "id_stato_int";
$proto11["m_columns"][] = "stato_intervento";
$obj = new SQLTable($proto11);

$proto10["m_table"] = $obj;
$proto10["m_sql"] = "\"public\".stato_interventi";
$proto10["m_alias"] = "";
$proto10["m_srcTableName"] = "public.stato_interventi";
$proto12=array();
$proto12["m_sql"] = "";
$proto12["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto12["m_column"]=$obj;
$proto12["m_contained"] = array();
$proto12["m_strCase"] = "";
$proto12["m_havingmode"] = false;
$proto12["m_inBrackets"] = false;
$proto12["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto12);

$proto10["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto10);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
												$proto14=array();
						$obj = new SQLField(array(
	"m_strName" => "id_stato_int",
	"m_strTable" => "public.stato_interventi",
	"m_srcTableName" => "public.stato_interventi"
));

$proto14["m_column"]=$obj;
$proto14["m_bAsc"] = 0;
$proto14["m_nColumn"] = 0;
$obj = new SQLOrderByItem($proto14);

$proto0["m_orderby"][]=$obj;					
$proto0["m_srcTableName"]="public.stato_interventi";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_public_stato_interventi = createSqlQuery_public_stato_interventi();


	
		;

		

$tdatapublic_stato_interventi[".sqlquery"] = $queryData_public_stato_interventi;

$tableEvents["public.stato_interventi"] = new eventsBase;
$tdatapublic_stato_interventi[".hasEvents"] = false;

?>