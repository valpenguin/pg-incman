<?php
require_once(getabspath("classes/cipherer.php"));




$tdatapublic_gruppi = array();
	$tdatapublic_gruppi[".truncateText"] = true;
	$tdatapublic_gruppi[".NumberOfChars"] = 80;
	$tdatapublic_gruppi[".ShortName"] = "public_gruppi";
	$tdatapublic_gruppi[".OwnerID"] = "";
	$tdatapublic_gruppi[".OriginalTable"] = "public.gruppi";

//	field labels
$fieldLabelspublic_gruppi = array();
$fieldToolTipspublic_gruppi = array();
$pageTitlespublic_gruppi = array();

if(mlang_getcurrentlang()=="Italian")
{
	$fieldLabelspublic_gruppi["Italian"] = array();
	$fieldToolTipspublic_gruppi["Italian"] = array();
	$pageTitlespublic_gruppi["Italian"] = array();
	$fieldLabelspublic_gruppi["Italian"]["id_gruppi"] = "Id Gruppo";
	$fieldToolTipspublic_gruppi["Italian"]["id_gruppi"] = "";
	$fieldLabelspublic_gruppi["Italian"]["Nome_gruppo"] = "Nome del gruppo";
	$fieldToolTipspublic_gruppi["Italian"]["Nome_gruppo"] = "";
	$fieldLabelspublic_gruppi["Italian"]["E_mail_gruppo"] = "E-mail del gruppo";
	$fieldToolTipspublic_gruppi["Italian"]["E_mail_gruppo"] = "";
	if (count($fieldToolTipspublic_gruppi["Italian"]))
		$tdatapublic_gruppi[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelspublic_gruppi[""] = array();
	$fieldToolTipspublic_gruppi[""] = array();
	$pageTitlespublic_gruppi[""] = array();
	$fieldLabelspublic_gruppi[""]["E_mail_gruppo"] = "E-mail Gruppo";
	$fieldToolTipspublic_gruppi[""]["E_mail_gruppo"] = "";
	if (count($fieldToolTipspublic_gruppi[""]))
		$tdatapublic_gruppi[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="English")
{
	$fieldLabelspublic_gruppi["English"] = array();
	$fieldToolTipspublic_gruppi["English"] = array();
	$pageTitlespublic_gruppi["English"] = array();
	if (count($fieldToolTipspublic_gruppi["English"]))
		$tdatapublic_gruppi[".isUseToolTips"] = true;
}


	$tdatapublic_gruppi[".NCSearch"] = true;



$tdatapublic_gruppi[".shortTableName"] = "public_gruppi";
$tdatapublic_gruppi[".nSecOptions"] = 0;
$tdatapublic_gruppi[".recsPerRowPrint"] = 1;
$tdatapublic_gruppi[".mainTableOwnerID"] = "";
$tdatapublic_gruppi[".moveNext"] = 1;
$tdatapublic_gruppi[".entityType"] = 0;

$tdatapublic_gruppi[".strOriginalTableName"] = "public.gruppi";

	



$tdatapublic_gruppi[".showAddInPopup"] = false;

$tdatapublic_gruppi[".showEditInPopup"] = false;

$tdatapublic_gruppi[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatapublic_gruppi[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatapublic_gruppi[".fieldsForRegister"] = array();

$tdatapublic_gruppi[".listAjax"] = false;

	$tdatapublic_gruppi[".audit"] = false;

	$tdatapublic_gruppi[".locking"] = false;

$tdatapublic_gruppi[".edit"] = true;
$tdatapublic_gruppi[".afterEditAction"] = 1;
$tdatapublic_gruppi[".closePopupAfterEdit"] = 1;
$tdatapublic_gruppi[".afterEditActionDetTable"] = "";

$tdatapublic_gruppi[".add"] = true;
$tdatapublic_gruppi[".afterAddAction"] = 1;
$tdatapublic_gruppi[".closePopupAfterAdd"] = 1;
$tdatapublic_gruppi[".afterAddActionDetTable"] = "";

$tdatapublic_gruppi[".list"] = true;





$tdatapublic_gruppi[".delete"] = true;

$tdatapublic_gruppi[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdatapublic_gruppi[".searchSaving"] = false;
//

$tdatapublic_gruppi[".showSearchPanel"] = true;
		$tdatapublic_gruppi[".flexibleSearch"] = true;

$tdatapublic_gruppi[".isUseAjaxSuggest"] = true;

$tdatapublic_gruppi[".rowHighlite"] = true;



$tdatapublic_gruppi[".addPageEvents"] = false;

// use timepicker for search panel
$tdatapublic_gruppi[".isUseTimeForSearch"] = false;



$tdatapublic_gruppi[".badgeColor"] = "4682B4";


$tdatapublic_gruppi[".allSearchFields"] = array();
$tdatapublic_gruppi[".filterFields"] = array();
$tdatapublic_gruppi[".requiredSearchFields"] = array();

$tdatapublic_gruppi[".allSearchFields"][] = "Nome gruppo";
	$tdatapublic_gruppi[".allSearchFields"][] = "E-mail gruppo";
	

$tdatapublic_gruppi[".googleLikeFields"] = array();
$tdatapublic_gruppi[".googleLikeFields"][] = "id_gruppi";
$tdatapublic_gruppi[".googleLikeFields"][] = "Nome gruppo";
$tdatapublic_gruppi[".googleLikeFields"][] = "E-mail gruppo";


$tdatapublic_gruppi[".advSearchFields"] = array();
$tdatapublic_gruppi[".advSearchFields"][] = "id_gruppi";
$tdatapublic_gruppi[".advSearchFields"][] = "Nome gruppo";
$tdatapublic_gruppi[".advSearchFields"][] = "E-mail gruppo";

$tdatapublic_gruppi[".tableType"] = "list";

$tdatapublic_gruppi[".printerPageOrientation"] = 0;
$tdatapublic_gruppi[".nPrinterPageScale"] = 100;

$tdatapublic_gruppi[".nPrinterSplitRecords"] = 40;

$tdatapublic_gruppi[".nPrinterPDFSplitRecords"] = 40;



$tdatapublic_gruppi[".geocodingEnabled"] = false;





$tdatapublic_gruppi[".listGridLayout"] = 3;





// view page pdf
$tdatapublic_gruppi[".isViewPagePDF"] = true;
$tdatapublic_gruppi[".isLandscapeViewPDFOrientation"] = 1;
$tdatapublic_gruppi[".isViewPagePDFFitToPage"] = 0;
$tdatapublic_gruppi[".nViewPagePDFScale"] = 100;

// print page pdf
$tdatapublic_gruppi[".isPrinterPagePDF"] = true;
$tdatapublic_gruppi[".isLandscapePrinterPagePDFOrientation"] = 1;
$tdatapublic_gruppi[".isPrinterPagePDFFitToPage"] = 0;
$tdatapublic_gruppi[".nPrinterPagePDFScale"] = 100;


$tdatapublic_gruppi[".pageSize"] = 20;

$tdatapublic_gruppi[".warnLeavingPages"] = true;



$tstrOrderBy = "ORDER BY id_gruppi DESC";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatapublic_gruppi[".strOrderBy"] = $tstrOrderBy;

$tdatapublic_gruppi[".orderindexes"] = array();
$tdatapublic_gruppi[".orderindexes"][] = array(1, (0 ? "ASC" : "DESC"), "id_gruppi");

$tdatapublic_gruppi[".sqlHead"] = "SELECT id_gruppi,  nome_gruppo AS \"Nome gruppo\",  email_gruppo AS \"E-mail gruppo\"";
$tdatapublic_gruppi[".sqlFrom"] = "FROM \"public\".gruppi";
$tdatapublic_gruppi[".sqlWhereExpr"] = "";
$tdatapublic_gruppi[".sqlTail"] = "";











//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatapublic_gruppi[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatapublic_gruppi[".arrGroupsPerPage"] = $arrGPP;

$tdatapublic_gruppi[".highlightSearchResults"] = true;

$tableKeyspublic_gruppi = array();
$tableKeyspublic_gruppi[] = "id_gruppi";
$tdatapublic_gruppi[".Keys"] = $tableKeyspublic_gruppi;

$tdatapublic_gruppi[".listFields"] = array();
$tdatapublic_gruppi[".listFields"][] = "Nome gruppo";
$tdatapublic_gruppi[".listFields"][] = "E-mail gruppo";

$tdatapublic_gruppi[".hideMobileList"] = array();


$tdatapublic_gruppi[".viewFields"] = array();

$tdatapublic_gruppi[".addFields"] = array();
$tdatapublic_gruppi[".addFields"][] = "Nome gruppo";
$tdatapublic_gruppi[".addFields"][] = "E-mail gruppo";

$tdatapublic_gruppi[".masterListFields"] = array();
$tdatapublic_gruppi[".masterListFields"][] = "id_gruppi";
$tdatapublic_gruppi[".masterListFields"][] = "Nome gruppo";
$tdatapublic_gruppi[".masterListFields"][] = "E-mail gruppo";

$tdatapublic_gruppi[".inlineAddFields"] = array();

$tdatapublic_gruppi[".editFields"] = array();
$tdatapublic_gruppi[".editFields"][] = "Nome gruppo";
$tdatapublic_gruppi[".editFields"][] = "E-mail gruppo";

$tdatapublic_gruppi[".inlineEditFields"] = array();

$tdatapublic_gruppi[".exportFields"] = array();

$tdatapublic_gruppi[".importFields"] = array();

$tdatapublic_gruppi[".printFields"] = array();

//	id_gruppi
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "id_gruppi";
	$fdata["GoodName"] = "id_gruppi";
	$fdata["ownerTable"] = "public.gruppi";
	$fdata["Label"] = GetFieldLabel("public_gruppi","id_gruppi");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "id_gruppi";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "id_gruppi";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "number";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_gruppi["id_gruppi"] = $fdata;
//	Nome gruppo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "Nome gruppo";
	$fdata["GoodName"] = "Nome_gruppo";
	$fdata["ownerTable"] = "public.gruppi";
	$fdata["Label"] = GetFieldLabel("public_gruppi","Nome_gruppo");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
	
		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "nome_gruppo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "nome_gruppo";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
			$edata["validateAs"]["basicValidate"][] = "DenyDuplicated";
	$edata["validateAs"]["customMessages"]["DenyDuplicated"] = array("message" => "Il gruppo %value% esiste già!", "messageType" => "Text");

	
	//	End validation

	
			
	
		$edata["denyDuplicates"] = true;

	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_gruppi["Nome gruppo"] = $fdata;
//	E-mail gruppo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "E-mail gruppo";
	$fdata["GoodName"] = "E_mail_gruppo";
	$fdata["ownerTable"] = "public.gruppi";
	$fdata["Label"] = GetFieldLabel("public_gruppi","E_mail_gruppo");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
	
		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "email_gruppo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "email_gruppo";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Email Hyperlink");

	
	
	
	
	
	
	
	
	
	
	
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "email";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Email");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
			$edata["validateAs"]["basicValidate"][] = "DenyDuplicated";
	$edata["validateAs"]["customMessages"]["DenyDuplicated"] = array("message" => "L'indirizzo %value% esiste già!", "messageType" => "Text");

	
	//	End validation

	
			
	
		$edata["denyDuplicates"] = true;

	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_gruppi["E-mail gruppo"] = $fdata;


$tables_data["public.gruppi"]=&$tdatapublic_gruppi;
$field_labels["public_gruppi"] = &$fieldLabelspublic_gruppi;
$fieldToolTips["public_gruppi"] = &$fieldToolTipspublic_gruppi;
$page_titles["public_gruppi"] = &$pageTitlespublic_gruppi;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["public.gruppi"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["public.gruppi"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_public_gruppi()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "id_gruppi,  nome_gruppo AS \"Nome gruppo\",  email_gruppo AS \"E-mail gruppo\"";
$proto0["m_strFrom"] = "FROM \"public\".gruppi";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "ORDER BY id_gruppi DESC";
$proto0["m_strTail"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "id_gruppi",
	"m_strTable" => "public.gruppi",
	"m_srcTableName" => "public.gruppi"
));

$proto6["m_sql"] = "id_gruppi";
$proto6["m_srcTableName"] = "public.gruppi";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "nome_gruppo",
	"m_strTable" => "public.gruppi",
	"m_srcTableName" => "public.gruppi"
));

$proto8["m_sql"] = "nome_gruppo";
$proto8["m_srcTableName"] = "public.gruppi";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "Nome gruppo";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "email_gruppo",
	"m_strTable" => "public.gruppi",
	"m_srcTableName" => "public.gruppi"
));

$proto10["m_sql"] = "email_gruppo";
$proto10["m_srcTableName"] = "public.gruppi";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "E-mail gruppo";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto12=array();
$proto12["m_link"] = "SQLL_MAIN";
			$proto13=array();
$proto13["m_strName"] = "public.gruppi";
$proto13["m_srcTableName"] = "public.gruppi";
$proto13["m_columns"] = array();
$proto13["m_columns"][] = "id_gruppi";
$proto13["m_columns"][] = "nome_gruppo";
$proto13["m_columns"][] = "email_gruppo";
$obj = new SQLTable($proto13);

$proto12["m_table"] = $obj;
$proto12["m_sql"] = "\"public\".gruppi";
$proto12["m_alias"] = "";
$proto12["m_srcTableName"] = "public.gruppi";
$proto14=array();
$proto14["m_sql"] = "";
$proto14["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto14["m_column"]=$obj;
$proto14["m_contained"] = array();
$proto14["m_strCase"] = "";
$proto14["m_havingmode"] = false;
$proto14["m_inBrackets"] = false;
$proto14["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto14);

$proto12["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto12);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
												$proto16=array();
						$obj = new SQLField(array(
	"m_strName" => "id_gruppi",
	"m_strTable" => "public.gruppi",
	"m_srcTableName" => "public.gruppi"
));

$proto16["m_column"]=$obj;
$proto16["m_bAsc"] = 0;
$proto16["m_nColumn"] = 0;
$obj = new SQLOrderByItem($proto16);

$proto0["m_orderby"][]=$obj;					
$proto0["m_srcTableName"]="public.gruppi";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_public_gruppi = createSqlQuery_public_gruppi();


	
		;

			

$tdatapublic_gruppi[".sqlquery"] = $queryData_public_gruppi;

$tableEvents["public.gruppi"] = new eventsBase;
$tdatapublic_gruppi[".hasEvents"] = false;

?>