<?php
require_once(getabspath("classes/cipherer.php"));




$tdatapublic_stato_gravita = array();
	$tdatapublic_stato_gravita[".truncateText"] = true;
	$tdatapublic_stato_gravita[".NumberOfChars"] = 80;
	$tdatapublic_stato_gravita[".ShortName"] = "public_stato_gravita";
	$tdatapublic_stato_gravita[".OwnerID"] = "";
	$tdatapublic_stato_gravita[".OriginalTable"] = "public.stato_gravita";

//	field labels
$fieldLabelspublic_stato_gravita = array();
$fieldToolTipspublic_stato_gravita = array();
$pageTitlespublic_stato_gravita = array();

if(mlang_getcurrentlang()=="Italian")
{
	$fieldLabelspublic_stato_gravita["Italian"] = array();
	$fieldToolTipspublic_stato_gravita["Italian"] = array();
	$pageTitlespublic_stato_gravita["Italian"] = array();
	$fieldLabelspublic_stato_gravita["Italian"]["id_stato_gravita"] = "Id Stato Gravità";
	$fieldToolTipspublic_stato_gravita["Italian"]["id_stato_gravita"] = "";
	$fieldLabelspublic_stato_gravita["Italian"]["Gravit_"] = "Gravità";
	$fieldToolTipspublic_stato_gravita["Italian"]["Gravit_"] = "";
	if (count($fieldToolTipspublic_stato_gravita["Italian"]))
		$tdatapublic_stato_gravita[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelspublic_stato_gravita[""] = array();
	$fieldToolTipspublic_stato_gravita[""] = array();
	$pageTitlespublic_stato_gravita[""] = array();
	$fieldLabelspublic_stato_gravita[""]["id_stato_gravita"] = "Id Stato Gravita";
	$fieldToolTipspublic_stato_gravita[""]["id_stato_gravita"] = "";
	$fieldLabelspublic_stato_gravita[""]["Gravit_"] = "Gravità";
	$fieldToolTipspublic_stato_gravita[""]["Gravit_"] = "";
	if (count($fieldToolTipspublic_stato_gravita[""]))
		$tdatapublic_stato_gravita[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="English")
{
	$fieldLabelspublic_stato_gravita["English"] = array();
	$fieldToolTipspublic_stato_gravita["English"] = array();
	$pageTitlespublic_stato_gravita["English"] = array();
	$fieldLabelspublic_stato_gravita["English"]["id_stato_gravita"] = "Id Stato Gravita";
	$fieldToolTipspublic_stato_gravita["English"]["id_stato_gravita"] = "";
	$fieldLabelspublic_stato_gravita["English"]["Gravit_"] = "Gravità";
	$fieldToolTipspublic_stato_gravita["English"]["Gravit_"] = "";
	if (count($fieldToolTipspublic_stato_gravita["English"]))
		$tdatapublic_stato_gravita[".isUseToolTips"] = true;
}


	$tdatapublic_stato_gravita[".NCSearch"] = true;



$tdatapublic_stato_gravita[".shortTableName"] = "public_stato_gravita";
$tdatapublic_stato_gravita[".nSecOptions"] = 0;
$tdatapublic_stato_gravita[".recsPerRowPrint"] = 1;
$tdatapublic_stato_gravita[".mainTableOwnerID"] = "";
$tdatapublic_stato_gravita[".moveNext"] = 1;
$tdatapublic_stato_gravita[".entityType"] = 0;

$tdatapublic_stato_gravita[".strOriginalTableName"] = "public.stato_gravita";

	



$tdatapublic_stato_gravita[".showAddInPopup"] = false;

$tdatapublic_stato_gravita[".showEditInPopup"] = false;

$tdatapublic_stato_gravita[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatapublic_stato_gravita[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatapublic_stato_gravita[".fieldsForRegister"] = array();

$tdatapublic_stato_gravita[".listAjax"] = false;

	$tdatapublic_stato_gravita[".audit"] = false;

	$tdatapublic_stato_gravita[".locking"] = false;

$tdatapublic_stato_gravita[".edit"] = true;
$tdatapublic_stato_gravita[".afterEditAction"] = 1;
$tdatapublic_stato_gravita[".closePopupAfterEdit"] = 1;
$tdatapublic_stato_gravita[".afterEditActionDetTable"] = "";

$tdatapublic_stato_gravita[".add"] = true;
$tdatapublic_stato_gravita[".afterAddAction"] = 1;
$tdatapublic_stato_gravita[".closePopupAfterAdd"] = 1;
$tdatapublic_stato_gravita[".afterAddActionDetTable"] = "";

$tdatapublic_stato_gravita[".list"] = true;





$tdatapublic_stato_gravita[".delete"] = true;

$tdatapublic_stato_gravita[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdatapublic_stato_gravita[".searchSaving"] = false;
//

$tdatapublic_stato_gravita[".showSearchPanel"] = true;
		$tdatapublic_stato_gravita[".flexibleSearch"] = true;

$tdatapublic_stato_gravita[".isUseAjaxSuggest"] = true;

$tdatapublic_stato_gravita[".rowHighlite"] = true;



$tdatapublic_stato_gravita[".addPageEvents"] = false;

// use timepicker for search panel
$tdatapublic_stato_gravita[".isUseTimeForSearch"] = false;





$tdatapublic_stato_gravita[".allSearchFields"] = array();
$tdatapublic_stato_gravita[".filterFields"] = array();
$tdatapublic_stato_gravita[".requiredSearchFields"] = array();

$tdatapublic_stato_gravita[".allSearchFields"][] = "Gravità";
	

$tdatapublic_stato_gravita[".googleLikeFields"] = array();
$tdatapublic_stato_gravita[".googleLikeFields"][] = "id_stato_gravita";
$tdatapublic_stato_gravita[".googleLikeFields"][] = "Gravità";


$tdatapublic_stato_gravita[".advSearchFields"] = array();
$tdatapublic_stato_gravita[".advSearchFields"][] = "Gravità";
$tdatapublic_stato_gravita[".advSearchFields"][] = "id_stato_gravita";

$tdatapublic_stato_gravita[".tableType"] = "list";

$tdatapublic_stato_gravita[".printerPageOrientation"] = 0;
$tdatapublic_stato_gravita[".nPrinterPageScale"] = 100;

$tdatapublic_stato_gravita[".nPrinterSplitRecords"] = 40;

$tdatapublic_stato_gravita[".nPrinterPDFSplitRecords"] = 40;



$tdatapublic_stato_gravita[".geocodingEnabled"] = false;





$tdatapublic_stato_gravita[".listGridLayout"] = 3;





// view page pdf
$tdatapublic_stato_gravita[".isViewPagePDF"] = true;
$tdatapublic_stato_gravita[".isLandscapeViewPDFOrientation"] = 1;
$tdatapublic_stato_gravita[".isViewPagePDFFitToPage"] = 0;
$tdatapublic_stato_gravita[".nViewPagePDFScale"] = 100;

// print page pdf
$tdatapublic_stato_gravita[".isPrinterPagePDF"] = true;
$tdatapublic_stato_gravita[".isLandscapePrinterPagePDFOrientation"] = 1;
$tdatapublic_stato_gravita[".isPrinterPagePDFFitToPage"] = 0;
$tdatapublic_stato_gravita[".nPrinterPagePDFScale"] = 100;


$tdatapublic_stato_gravita[".pageSize"] = 20;

$tdatapublic_stato_gravita[".warnLeavingPages"] = true;



$tstrOrderBy = "ORDER BY id_stato_gravita DESC";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatapublic_stato_gravita[".strOrderBy"] = $tstrOrderBy;

$tdatapublic_stato_gravita[".orderindexes"] = array();
$tdatapublic_stato_gravita[".orderindexes"][] = array(1, (0 ? "ASC" : "DESC"), "id_stato_gravita");

$tdatapublic_stato_gravita[".sqlHead"] = "SELECT id_stato_gravita,  livello_gravita AS \"Gravità\"";
$tdatapublic_stato_gravita[".sqlFrom"] = "FROM \"public\".stato_gravita";
$tdatapublic_stato_gravita[".sqlWhereExpr"] = "";
$tdatapublic_stato_gravita[".sqlTail"] = "";











//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatapublic_stato_gravita[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatapublic_stato_gravita[".arrGroupsPerPage"] = $arrGPP;

$tdatapublic_stato_gravita[".highlightSearchResults"] = true;

$tableKeyspublic_stato_gravita = array();
$tableKeyspublic_stato_gravita[] = "id_stato_gravita";
$tdatapublic_stato_gravita[".Keys"] = $tableKeyspublic_stato_gravita;

$tdatapublic_stato_gravita[".listFields"] = array();
$tdatapublic_stato_gravita[".listFields"][] = "Gravità";

$tdatapublic_stato_gravita[".hideMobileList"] = array();


$tdatapublic_stato_gravita[".viewFields"] = array();

$tdatapublic_stato_gravita[".addFields"] = array();
$tdatapublic_stato_gravita[".addFields"][] = "Gravità";

$tdatapublic_stato_gravita[".masterListFields"] = array();
$tdatapublic_stato_gravita[".masterListFields"][] = "id_stato_gravita";
$tdatapublic_stato_gravita[".masterListFields"][] = "Gravità";

$tdatapublic_stato_gravita[".inlineAddFields"] = array();

$tdatapublic_stato_gravita[".editFields"] = array();
$tdatapublic_stato_gravita[".editFields"][] = "Gravità";

$tdatapublic_stato_gravita[".inlineEditFields"] = array();

$tdatapublic_stato_gravita[".exportFields"] = array();

$tdatapublic_stato_gravita[".importFields"] = array();

$tdatapublic_stato_gravita[".printFields"] = array();

//	id_stato_gravita
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "id_stato_gravita";
	$fdata["GoodName"] = "id_stato_gravita";
	$fdata["ownerTable"] = "public.stato_gravita";
	$fdata["Label"] = GetFieldLabel("public_stato_gravita","id_stato_gravita");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "id_stato_gravita";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "id_stato_gravita";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "number";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_stato_gravita["id_stato_gravita"] = $fdata;
//	Gravità
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "Gravità";
	$fdata["GoodName"] = "Gravit_";
	$fdata["ownerTable"] = "public.stato_gravita";
	$fdata["Label"] = GetFieldLabel("public_stato_gravita","Gravit_");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
	
		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "livello_gravita";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "livello_gravita";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
			$edata["validateAs"]["basicValidate"][] = "DenyDuplicated";
	$edata["validateAs"]["customMessages"]["DenyDuplicated"] = array("message" => "Stato %value% già presente!", "messageType" => "Text");

	
	//	End validation

	
			
	
		$edata["denyDuplicates"] = true;

	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_stato_gravita["Gravità"] = $fdata;


$tables_data["public.stato_gravita"]=&$tdatapublic_stato_gravita;
$field_labels["public_stato_gravita"] = &$fieldLabelspublic_stato_gravita;
$fieldToolTips["public_stato_gravita"] = &$fieldToolTipspublic_stato_gravita;
$page_titles["public_stato_gravita"] = &$pageTitlespublic_stato_gravita;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["public.stato_gravita"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["public.stato_gravita"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_public_stato_gravita()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "id_stato_gravita,  livello_gravita AS \"Gravità\"";
$proto0["m_strFrom"] = "FROM \"public\".stato_gravita";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "ORDER BY id_stato_gravita DESC";
$proto0["m_strTail"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "id_stato_gravita",
	"m_strTable" => "public.stato_gravita",
	"m_srcTableName" => "public.stato_gravita"
));

$proto6["m_sql"] = "id_stato_gravita";
$proto6["m_srcTableName"] = "public.stato_gravita";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "livello_gravita",
	"m_strTable" => "public.stato_gravita",
	"m_srcTableName" => "public.stato_gravita"
));

$proto8["m_sql"] = "livello_gravita";
$proto8["m_srcTableName"] = "public.stato_gravita";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "Gravità";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto10=array();
$proto10["m_link"] = "SQLL_MAIN";
			$proto11=array();
$proto11["m_strName"] = "public.stato_gravita";
$proto11["m_srcTableName"] = "public.stato_gravita";
$proto11["m_columns"] = array();
$proto11["m_columns"][] = "id_stato_gravita";
$proto11["m_columns"][] = "livello_gravita";
$obj = new SQLTable($proto11);

$proto10["m_table"] = $obj;
$proto10["m_sql"] = "\"public\".stato_gravita";
$proto10["m_alias"] = "";
$proto10["m_srcTableName"] = "public.stato_gravita";
$proto12=array();
$proto12["m_sql"] = "";
$proto12["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto12["m_column"]=$obj;
$proto12["m_contained"] = array();
$proto12["m_strCase"] = "";
$proto12["m_havingmode"] = false;
$proto12["m_inBrackets"] = false;
$proto12["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto12);

$proto10["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto10);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
												$proto14=array();
						$obj = new SQLField(array(
	"m_strName" => "id_stato_gravita",
	"m_strTable" => "public.stato_gravita",
	"m_srcTableName" => "public.stato_gravita"
));

$proto14["m_column"]=$obj;
$proto14["m_bAsc"] = 0;
$proto14["m_nColumn"] = 0;
$obj = new SQLOrderByItem($proto14);

$proto0["m_orderby"][]=$obj;					
$proto0["m_srcTableName"]="public.stato_gravita";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_public_stato_gravita = createSqlQuery_public_stato_gravita();


	
		;

		

$tdatapublic_stato_gravita[".sqlquery"] = $queryData_public_stato_gravita;

$tableEvents["public.stato_gravita"] = new eventsBase;
$tdatapublic_stato_gravita[".hasEvents"] = false;

?>