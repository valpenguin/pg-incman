<?php
require_once(getabspath("classes/cipherer.php"));




$tdatapublic_stato_incident = array();
	$tdatapublic_stato_incident[".truncateText"] = true;
	$tdatapublic_stato_incident[".NumberOfChars"] = 80;
	$tdatapublic_stato_incident[".ShortName"] = "public_stato_incident";
	$tdatapublic_stato_incident[".OwnerID"] = "";
	$tdatapublic_stato_incident[".OriginalTable"] = "public.stato_incident";

//	field labels
$fieldLabelspublic_stato_incident = array();
$fieldToolTipspublic_stato_incident = array();
$pageTitlespublic_stato_incident = array();

if(mlang_getcurrentlang()=="Italian")
{
	$fieldLabelspublic_stato_incident["Italian"] = array();
	$fieldToolTipspublic_stato_incident["Italian"] = array();
	$pageTitlespublic_stato_incident["Italian"] = array();
	$fieldLabelspublic_stato_incident["Italian"]["Stato_incident"] = "Stato incident";
	$fieldToolTipspublic_stato_incident["Italian"]["Stato_incident"] = "";
	$fieldLabelspublic_stato_incident["Italian"]["id_stato_inc"] = "Id Stato Inc";
	$fieldToolTipspublic_stato_incident["Italian"]["id_stato_inc"] = "";
	if (count($fieldToolTipspublic_stato_incident["Italian"]))
		$tdatapublic_stato_incident[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelspublic_stato_incident[""] = array();
	$fieldToolTipspublic_stato_incident[""] = array();
	$pageTitlespublic_stato_incident[""] = array();
	$fieldLabelspublic_stato_incident[""]["Stato_incident"] = "Stato Incident";
	$fieldToolTipspublic_stato_incident[""]["Stato_incident"] = "";
	$fieldLabelspublic_stato_incident[""]["id_stato_inc"] = "Id Stato Inc";
	$fieldToolTipspublic_stato_incident[""]["id_stato_inc"] = "";
	if (count($fieldToolTipspublic_stato_incident[""]))
		$tdatapublic_stato_incident[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="English")
{
	$fieldLabelspublic_stato_incident["English"] = array();
	$fieldToolTipspublic_stato_incident["English"] = array();
	$pageTitlespublic_stato_incident["English"] = array();
	$fieldLabelspublic_stato_incident["English"]["Stato_incident"] = "Stato Incident";
	$fieldToolTipspublic_stato_incident["English"]["Stato_incident"] = "";
	$fieldLabelspublic_stato_incident["English"]["id_stato_inc"] = "Id Stato Inc";
	$fieldToolTipspublic_stato_incident["English"]["id_stato_inc"] = "";
	if (count($fieldToolTipspublic_stato_incident["English"]))
		$tdatapublic_stato_incident[".isUseToolTips"] = true;
}


	$tdatapublic_stato_incident[".NCSearch"] = true;



$tdatapublic_stato_incident[".shortTableName"] = "public_stato_incident";
$tdatapublic_stato_incident[".nSecOptions"] = 0;
$tdatapublic_stato_incident[".recsPerRowPrint"] = 1;
$tdatapublic_stato_incident[".mainTableOwnerID"] = "";
$tdatapublic_stato_incident[".moveNext"] = 1;
$tdatapublic_stato_incident[".entityType"] = 0;

$tdatapublic_stato_incident[".strOriginalTableName"] = "public.stato_incident";

	



$tdatapublic_stato_incident[".showAddInPopup"] = false;

$tdatapublic_stato_incident[".showEditInPopup"] = false;

$tdatapublic_stato_incident[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatapublic_stato_incident[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatapublic_stato_incident[".fieldsForRegister"] = array();

$tdatapublic_stato_incident[".listAjax"] = false;

	$tdatapublic_stato_incident[".audit"] = false;

	$tdatapublic_stato_incident[".locking"] = false;

$tdatapublic_stato_incident[".edit"] = true;
$tdatapublic_stato_incident[".afterEditAction"] = 1;
$tdatapublic_stato_incident[".closePopupAfterEdit"] = 1;
$tdatapublic_stato_incident[".afterEditActionDetTable"] = "";

$tdatapublic_stato_incident[".add"] = true;
$tdatapublic_stato_incident[".afterAddAction"] = 1;
$tdatapublic_stato_incident[".closePopupAfterAdd"] = 1;
$tdatapublic_stato_incident[".afterAddActionDetTable"] = "";

$tdatapublic_stato_incident[".list"] = true;





$tdatapublic_stato_incident[".delete"] = true;

$tdatapublic_stato_incident[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdatapublic_stato_incident[".searchSaving"] = false;
//

$tdatapublic_stato_incident[".showSearchPanel"] = true;
		$tdatapublic_stato_incident[".flexibleSearch"] = true;

$tdatapublic_stato_incident[".isUseAjaxSuggest"] = true;

$tdatapublic_stato_incident[".rowHighlite"] = true;



$tdatapublic_stato_incident[".addPageEvents"] = false;

// use timepicker for search panel
$tdatapublic_stato_incident[".isUseTimeForSearch"] = false;



$tdatapublic_stato_incident[".badgeColor"] = "4682B4";


$tdatapublic_stato_incident[".allSearchFields"] = array();
$tdatapublic_stato_incident[".filterFields"] = array();
$tdatapublic_stato_incident[".requiredSearchFields"] = array();

$tdatapublic_stato_incident[".allSearchFields"][] = "Stato incident";
	

$tdatapublic_stato_incident[".googleLikeFields"] = array();
$tdatapublic_stato_incident[".googleLikeFields"][] = "Stato incident";
$tdatapublic_stato_incident[".googleLikeFields"][] = "id_stato_inc";


$tdatapublic_stato_incident[".advSearchFields"] = array();
$tdatapublic_stato_incident[".advSearchFields"][] = "id_stato_inc";
$tdatapublic_stato_incident[".advSearchFields"][] = "Stato incident";

$tdatapublic_stato_incident[".tableType"] = "list";

$tdatapublic_stato_incident[".printerPageOrientation"] = 0;
$tdatapublic_stato_incident[".nPrinterPageScale"] = 100;

$tdatapublic_stato_incident[".nPrinterSplitRecords"] = 40;

$tdatapublic_stato_incident[".nPrinterPDFSplitRecords"] = 40;



$tdatapublic_stato_incident[".geocodingEnabled"] = false;





$tdatapublic_stato_incident[".listGridLayout"] = 3;





// view page pdf
$tdatapublic_stato_incident[".isViewPagePDF"] = true;
$tdatapublic_stato_incident[".isLandscapeViewPDFOrientation"] = 1;
$tdatapublic_stato_incident[".isViewPagePDFFitToPage"] = 0;
$tdatapublic_stato_incident[".nViewPagePDFScale"] = 100;

// print page pdf
$tdatapublic_stato_incident[".isPrinterPagePDF"] = true;
$tdatapublic_stato_incident[".isLandscapePrinterPagePDFOrientation"] = 1;
$tdatapublic_stato_incident[".isPrinterPagePDFFitToPage"] = 0;
$tdatapublic_stato_incident[".nPrinterPagePDFScale"] = 100;


$tdatapublic_stato_incident[".pageSize"] = 20;

$tdatapublic_stato_incident[".warnLeavingPages"] = true;



$tstrOrderBy = "ORDER BY id_stato_inc DESC";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatapublic_stato_incident[".strOrderBy"] = $tstrOrderBy;

$tdatapublic_stato_incident[".orderindexes"] = array();
$tdatapublic_stato_incident[".orderindexes"][] = array(2, (0 ? "ASC" : "DESC"), "id_stato_inc");

$tdatapublic_stato_incident[".sqlHead"] = "SELECT stato_incident AS \"Stato incident\",  id_stato_inc";
$tdatapublic_stato_incident[".sqlFrom"] = "FROM \"public\".stato_incident";
$tdatapublic_stato_incident[".sqlWhereExpr"] = "";
$tdatapublic_stato_incident[".sqlTail"] = "";











//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatapublic_stato_incident[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatapublic_stato_incident[".arrGroupsPerPage"] = $arrGPP;

$tdatapublic_stato_incident[".highlightSearchResults"] = true;

$tableKeyspublic_stato_incident = array();
$tableKeyspublic_stato_incident[] = "id_stato_inc";
$tdatapublic_stato_incident[".Keys"] = $tableKeyspublic_stato_incident;

$tdatapublic_stato_incident[".listFields"] = array();
$tdatapublic_stato_incident[".listFields"][] = "Stato incident";

$tdatapublic_stato_incident[".hideMobileList"] = array();


$tdatapublic_stato_incident[".viewFields"] = array();

$tdatapublic_stato_incident[".addFields"] = array();
$tdatapublic_stato_incident[".addFields"][] = "Stato incident";

$tdatapublic_stato_incident[".masterListFields"] = array();
$tdatapublic_stato_incident[".masterListFields"][] = "Stato incident";
$tdatapublic_stato_incident[".masterListFields"][] = "id_stato_inc";

$tdatapublic_stato_incident[".inlineAddFields"] = array();

$tdatapublic_stato_incident[".editFields"] = array();
$tdatapublic_stato_incident[".editFields"][] = "Stato incident";

$tdatapublic_stato_incident[".inlineEditFields"] = array();

$tdatapublic_stato_incident[".exportFields"] = array();

$tdatapublic_stato_incident[".importFields"] = array();

$tdatapublic_stato_incident[".printFields"] = array();

//	Stato incident
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "Stato incident";
	$fdata["GoodName"] = "Stato_incident";
	$fdata["ownerTable"] = "public.stato_incident";
	$fdata["Label"] = GetFieldLabel("public_stato_incident","Stato_incident");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
	
		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "stato_incident";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "stato_incident";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
			$edata["validateAs"]["basicValidate"][] = "DenyDuplicated";
	$edata["validateAs"]["customMessages"]["DenyDuplicated"] = array("message" => "Stato incident %value% già presente!", "messageType" => "Text");

	
	//	End validation

	
			
	
		$edata["denyDuplicates"] = true;

	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_stato_incident["Stato incident"] = $fdata;
//	id_stato_inc
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "id_stato_inc";
	$fdata["GoodName"] = "id_stato_inc";
	$fdata["ownerTable"] = "public.stato_incident";
	$fdata["Label"] = GetFieldLabel("public_stato_incident","id_stato_inc");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "id_stato_inc";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "id_stato_inc";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "number";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_stato_incident["id_stato_inc"] = $fdata;


$tables_data["public.stato_incident"]=&$tdatapublic_stato_incident;
$field_labels["public_stato_incident"] = &$fieldLabelspublic_stato_incident;
$fieldToolTips["public_stato_incident"] = &$fieldToolTipspublic_stato_incident;
$page_titles["public_stato_incident"] = &$pageTitlespublic_stato_incident;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["public.stato_incident"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["public.stato_incident"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_public_stato_incident()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "stato_incident AS \"Stato incident\",  id_stato_inc";
$proto0["m_strFrom"] = "FROM \"public\".stato_incident";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "ORDER BY id_stato_inc DESC";
$proto0["m_strTail"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "stato_incident",
	"m_strTable" => "public.stato_incident",
	"m_srcTableName" => "public.stato_incident"
));

$proto6["m_sql"] = "stato_incident";
$proto6["m_srcTableName"] = "public.stato_incident";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "Stato incident";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "id_stato_inc",
	"m_strTable" => "public.stato_incident",
	"m_srcTableName" => "public.stato_incident"
));

$proto8["m_sql"] = "id_stato_inc";
$proto8["m_srcTableName"] = "public.stato_incident";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto10=array();
$proto10["m_link"] = "SQLL_MAIN";
			$proto11=array();
$proto11["m_strName"] = "public.stato_incident";
$proto11["m_srcTableName"] = "public.stato_incident";
$proto11["m_columns"] = array();
$proto11["m_columns"][] = "id_stato_inc";
$proto11["m_columns"][] = "stato_incident";
$obj = new SQLTable($proto11);

$proto10["m_table"] = $obj;
$proto10["m_sql"] = "\"public\".stato_incident";
$proto10["m_alias"] = "";
$proto10["m_srcTableName"] = "public.stato_incident";
$proto12=array();
$proto12["m_sql"] = "";
$proto12["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto12["m_column"]=$obj;
$proto12["m_contained"] = array();
$proto12["m_strCase"] = "";
$proto12["m_havingmode"] = false;
$proto12["m_inBrackets"] = false;
$proto12["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto12);

$proto10["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto10);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
												$proto14=array();
						$obj = new SQLField(array(
	"m_strName" => "id_stato_inc",
	"m_strTable" => "public.stato_incident",
	"m_srcTableName" => "public.stato_incident"
));

$proto14["m_column"]=$obj;
$proto14["m_bAsc"] = 0;
$proto14["m_nColumn"] = 0;
$obj = new SQLOrderByItem($proto14);

$proto0["m_orderby"][]=$obj;					
$proto0["m_srcTableName"]="public.stato_incident";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_public_stato_incident = createSqlQuery_public_stato_incident();


	
		;

		

$tdatapublic_stato_incident[".sqlquery"] = $queryData_public_stato_incident;

$tableEvents["public.stato_incident"] = new eventsBase;
$tdatapublic_stato_incident[".hasEvents"] = false;

?>