<?php
require_once(getabspath("classes/cipherer.php"));




$tdatapublic_tipo_causa = array();
	$tdatapublic_tipo_causa[".truncateText"] = true;
	$tdatapublic_tipo_causa[".NumberOfChars"] = 80;
	$tdatapublic_tipo_causa[".ShortName"] = "public_tipo_causa";
	$tdatapublic_tipo_causa[".OwnerID"] = "";
	$tdatapublic_tipo_causa[".OriginalTable"] = "public.tipo_causa";

//	field labels
$fieldLabelspublic_tipo_causa = array();
$fieldToolTipspublic_tipo_causa = array();
$pageTitlespublic_tipo_causa = array();

if(mlang_getcurrentlang()=="Italian")
{
	$fieldLabelspublic_tipo_causa["Italian"] = array();
	$fieldToolTipspublic_tipo_causa["Italian"] = array();
	$pageTitlespublic_tipo_causa["Italian"] = array();
	$fieldLabelspublic_tipo_causa["Italian"]["id_tipo_causa"] = "Id Tipo Causa";
	$fieldToolTipspublic_tipo_causa["Italian"]["id_tipo_causa"] = "";
	$fieldLabelspublic_tipo_causa["Italian"]["Tipologia_causa"] = "Tipologia causa";
	$fieldToolTipspublic_tipo_causa["Italian"]["Tipologia_causa"] = "";
	if (count($fieldToolTipspublic_tipo_causa["Italian"]))
		$tdatapublic_tipo_causa[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelspublic_tipo_causa[""] = array();
	$fieldToolTipspublic_tipo_causa[""] = array();
	$pageTitlespublic_tipo_causa[""] = array();
	$fieldLabelspublic_tipo_causa[""]["id_tipo_causa"] = "Id Tipo Causa";
	$fieldToolTipspublic_tipo_causa[""]["id_tipo_causa"] = "";
	$fieldLabelspublic_tipo_causa[""]["Tipologia_causa"] = "Tipologia Causa";
	$fieldToolTipspublic_tipo_causa[""]["Tipologia_causa"] = "";
	if (count($fieldToolTipspublic_tipo_causa[""]))
		$tdatapublic_tipo_causa[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="English")
{
	$fieldLabelspublic_tipo_causa["English"] = array();
	$fieldToolTipspublic_tipo_causa["English"] = array();
	$pageTitlespublic_tipo_causa["English"] = array();
	$fieldLabelspublic_tipo_causa["English"]["id_tipo_causa"] = "Id Tipo Causa";
	$fieldToolTipspublic_tipo_causa["English"]["id_tipo_causa"] = "";
	$fieldLabelspublic_tipo_causa["English"]["Tipologia_causa"] = "Tipologia Causa";
	$fieldToolTipspublic_tipo_causa["English"]["Tipologia_causa"] = "";
	if (count($fieldToolTipspublic_tipo_causa["English"]))
		$tdatapublic_tipo_causa[".isUseToolTips"] = true;
}


	$tdatapublic_tipo_causa[".NCSearch"] = true;



$tdatapublic_tipo_causa[".shortTableName"] = "public_tipo_causa";
$tdatapublic_tipo_causa[".nSecOptions"] = 0;
$tdatapublic_tipo_causa[".recsPerRowPrint"] = 1;
$tdatapublic_tipo_causa[".mainTableOwnerID"] = "";
$tdatapublic_tipo_causa[".moveNext"] = 1;
$tdatapublic_tipo_causa[".entityType"] = 0;

$tdatapublic_tipo_causa[".strOriginalTableName"] = "public.tipo_causa";

	



$tdatapublic_tipo_causa[".showAddInPopup"] = false;

$tdatapublic_tipo_causa[".showEditInPopup"] = false;

$tdatapublic_tipo_causa[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatapublic_tipo_causa[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatapublic_tipo_causa[".fieldsForRegister"] = array();

$tdatapublic_tipo_causa[".listAjax"] = false;

	$tdatapublic_tipo_causa[".audit"] = false;

	$tdatapublic_tipo_causa[".locking"] = false;

$tdatapublic_tipo_causa[".edit"] = true;
$tdatapublic_tipo_causa[".afterEditAction"] = 1;
$tdatapublic_tipo_causa[".closePopupAfterEdit"] = 1;
$tdatapublic_tipo_causa[".afterEditActionDetTable"] = "";

$tdatapublic_tipo_causa[".add"] = true;
$tdatapublic_tipo_causa[".afterAddAction"] = 1;
$tdatapublic_tipo_causa[".closePopupAfterAdd"] = 1;
$tdatapublic_tipo_causa[".afterAddActionDetTable"] = "";

$tdatapublic_tipo_causa[".list"] = true;





$tdatapublic_tipo_causa[".delete"] = true;

$tdatapublic_tipo_causa[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdatapublic_tipo_causa[".searchSaving"] = false;
//

$tdatapublic_tipo_causa[".showSearchPanel"] = true;
		$tdatapublic_tipo_causa[".flexibleSearch"] = true;

$tdatapublic_tipo_causa[".isUseAjaxSuggest"] = true;

$tdatapublic_tipo_causa[".rowHighlite"] = true;



$tdatapublic_tipo_causa[".addPageEvents"] = false;

// use timepicker for search panel
$tdatapublic_tipo_causa[".isUseTimeForSearch"] = false;





$tdatapublic_tipo_causa[".allSearchFields"] = array();
$tdatapublic_tipo_causa[".filterFields"] = array();
$tdatapublic_tipo_causa[".requiredSearchFields"] = array();

$tdatapublic_tipo_causa[".allSearchFields"][] = "Tipologia causa";
	

$tdatapublic_tipo_causa[".googleLikeFields"] = array();
$tdatapublic_tipo_causa[".googleLikeFields"][] = "id_tipo_causa";
$tdatapublic_tipo_causa[".googleLikeFields"][] = "Tipologia causa";


$tdatapublic_tipo_causa[".advSearchFields"] = array();
$tdatapublic_tipo_causa[".advSearchFields"][] = "id_tipo_causa";
$tdatapublic_tipo_causa[".advSearchFields"][] = "Tipologia causa";

$tdatapublic_tipo_causa[".tableType"] = "list";

$tdatapublic_tipo_causa[".printerPageOrientation"] = 0;
$tdatapublic_tipo_causa[".nPrinterPageScale"] = 100;

$tdatapublic_tipo_causa[".nPrinterSplitRecords"] = 40;

$tdatapublic_tipo_causa[".nPrinterPDFSplitRecords"] = 40;



$tdatapublic_tipo_causa[".geocodingEnabled"] = false;





$tdatapublic_tipo_causa[".listGridLayout"] = 3;





// view page pdf
$tdatapublic_tipo_causa[".isViewPagePDF"] = true;
$tdatapublic_tipo_causa[".isLandscapeViewPDFOrientation"] = 1;
$tdatapublic_tipo_causa[".isViewPagePDFFitToPage"] = 0;
$tdatapublic_tipo_causa[".nViewPagePDFScale"] = 100;

// print page pdf
$tdatapublic_tipo_causa[".isPrinterPagePDF"] = true;
$tdatapublic_tipo_causa[".isLandscapePrinterPagePDFOrientation"] = 1;
$tdatapublic_tipo_causa[".isPrinterPagePDFFitToPage"] = 0;
$tdatapublic_tipo_causa[".nPrinterPagePDFScale"] = 100;


$tdatapublic_tipo_causa[".pageSize"] = 20;

$tdatapublic_tipo_causa[".warnLeavingPages"] = true;



$tstrOrderBy = "ORDER BY id_tipo_causa DESC";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatapublic_tipo_causa[".strOrderBy"] = $tstrOrderBy;

$tdatapublic_tipo_causa[".orderindexes"] = array();
$tdatapublic_tipo_causa[".orderindexes"][] = array(1, (0 ? "ASC" : "DESC"), "id_tipo_causa");

$tdatapublic_tipo_causa[".sqlHead"] = "SELECT id_tipo_causa,  tipo_causa AS \"Tipologia causa\"";
$tdatapublic_tipo_causa[".sqlFrom"] = "FROM \"public\".tipo_causa";
$tdatapublic_tipo_causa[".sqlWhereExpr"] = "";
$tdatapublic_tipo_causa[".sqlTail"] = "";











//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatapublic_tipo_causa[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatapublic_tipo_causa[".arrGroupsPerPage"] = $arrGPP;

$tdatapublic_tipo_causa[".highlightSearchResults"] = true;

$tableKeyspublic_tipo_causa = array();
$tableKeyspublic_tipo_causa[] = "id_tipo_causa";
$tdatapublic_tipo_causa[".Keys"] = $tableKeyspublic_tipo_causa;

$tdatapublic_tipo_causa[".listFields"] = array();
$tdatapublic_tipo_causa[".listFields"][] = "Tipologia causa";

$tdatapublic_tipo_causa[".hideMobileList"] = array();


$tdatapublic_tipo_causa[".viewFields"] = array();

$tdatapublic_tipo_causa[".addFields"] = array();
$tdatapublic_tipo_causa[".addFields"][] = "Tipologia causa";

$tdatapublic_tipo_causa[".masterListFields"] = array();
$tdatapublic_tipo_causa[".masterListFields"][] = "id_tipo_causa";
$tdatapublic_tipo_causa[".masterListFields"][] = "Tipologia causa";

$tdatapublic_tipo_causa[".inlineAddFields"] = array();

$tdatapublic_tipo_causa[".editFields"] = array();
$tdatapublic_tipo_causa[".editFields"][] = "Tipologia causa";

$tdatapublic_tipo_causa[".inlineEditFields"] = array();

$tdatapublic_tipo_causa[".exportFields"] = array();

$tdatapublic_tipo_causa[".importFields"] = array();

$tdatapublic_tipo_causa[".printFields"] = array();

//	id_tipo_causa
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "id_tipo_causa";
	$fdata["GoodName"] = "id_tipo_causa";
	$fdata["ownerTable"] = "public.tipo_causa";
	$fdata["Label"] = GetFieldLabel("public_tipo_causa","id_tipo_causa");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "id_tipo_causa";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "id_tipo_causa";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "number";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_tipo_causa["id_tipo_causa"] = $fdata;
//	Tipologia causa
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "Tipologia causa";
	$fdata["GoodName"] = "Tipologia_causa";
	$fdata["ownerTable"] = "public.tipo_causa";
	$fdata["Label"] = GetFieldLabel("public_tipo_causa","Tipologia_causa");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
	
		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "tipo_causa";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "tipo_causa";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
			$edata["validateAs"]["basicValidate"][] = "DenyDuplicated";
	$edata["validateAs"]["customMessages"]["DenyDuplicated"] = array("message" => "Tipologia %value% già presente!", "messageType" => "Text");

	
	//	End validation

	
			
	
		$edata["denyDuplicates"] = true;

	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_tipo_causa["Tipologia causa"] = $fdata;


$tables_data["public.tipo_causa"]=&$tdatapublic_tipo_causa;
$field_labels["public_tipo_causa"] = &$fieldLabelspublic_tipo_causa;
$fieldToolTips["public_tipo_causa"] = &$fieldToolTipspublic_tipo_causa;
$page_titles["public_tipo_causa"] = &$pageTitlespublic_tipo_causa;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["public.tipo_causa"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["public.tipo_causa"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_public_tipo_causa()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "id_tipo_causa,  tipo_causa AS \"Tipologia causa\"";
$proto0["m_strFrom"] = "FROM \"public\".tipo_causa";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "ORDER BY id_tipo_causa DESC";
$proto0["m_strTail"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "id_tipo_causa",
	"m_strTable" => "public.tipo_causa",
	"m_srcTableName" => "public.tipo_causa"
));

$proto6["m_sql"] = "id_tipo_causa";
$proto6["m_srcTableName"] = "public.tipo_causa";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "tipo_causa",
	"m_strTable" => "public.tipo_causa",
	"m_srcTableName" => "public.tipo_causa"
));

$proto8["m_sql"] = "tipo_causa";
$proto8["m_srcTableName"] = "public.tipo_causa";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "Tipologia causa";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto10=array();
$proto10["m_link"] = "SQLL_MAIN";
			$proto11=array();
$proto11["m_strName"] = "public.tipo_causa";
$proto11["m_srcTableName"] = "public.tipo_causa";
$proto11["m_columns"] = array();
$proto11["m_columns"][] = "id_tipo_causa";
$proto11["m_columns"][] = "tipo_causa";
$obj = new SQLTable($proto11);

$proto10["m_table"] = $obj;
$proto10["m_sql"] = "\"public\".tipo_causa";
$proto10["m_alias"] = "";
$proto10["m_srcTableName"] = "public.tipo_causa";
$proto12=array();
$proto12["m_sql"] = "";
$proto12["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto12["m_column"]=$obj;
$proto12["m_contained"] = array();
$proto12["m_strCase"] = "";
$proto12["m_havingmode"] = false;
$proto12["m_inBrackets"] = false;
$proto12["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto12);

$proto10["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto10);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
												$proto14=array();
						$obj = new SQLField(array(
	"m_strName" => "id_tipo_causa",
	"m_strTable" => "public.tipo_causa",
	"m_srcTableName" => "public.tipo_causa"
));

$proto14["m_column"]=$obj;
$proto14["m_bAsc"] = 0;
$proto14["m_nColumn"] = 0;
$obj = new SQLOrderByItem($proto14);

$proto0["m_orderby"][]=$obj;					
$proto0["m_srcTableName"]="public.tipo_causa";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_public_tipo_causa = createSqlQuery_public_tipo_causa();


	
		;

		

$tdatapublic_tipo_causa[".sqlquery"] = $queryData_public_tipo_causa;

$tableEvents["public.tipo_causa"] = new eventsBase;
$tdatapublic_tipo_causa[".hasEvents"] = false;

?>