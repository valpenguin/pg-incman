<?php

/**
* getLookupMainTableSettings - tests whether the lookup link exists between the tables
*
*  returns array with ProjectSettings class for main table if the link exists in project settings.
*  returns NULL otherwise
*/
function getLookupMainTableSettings($lookupTable, $mainTableShortName, $mainField, $desiredPage = "")
{
	global $lookupTableLinks;
	if(!isset($lookupTableLinks[$lookupTable]))
		return null;
	if(!isset($lookupTableLinks[$lookupTable][$mainTableShortName.".".$mainField]))
		return null;
	$arr = &$lookupTableLinks[$lookupTable][$mainTableShortName.".".$mainField];
	$effectivePage = $desiredPage;
	if(!isset($arr[$effectivePage]))
	{
		$effectivePage = PAGE_EDIT;
		if(!isset($arr[$effectivePage]))
		{
			if($desiredPage == "" && 0 < count($arr))
			{
				$effectivePage = $arr[0];
			}
			else
				return null;
		}
	}
	return new ProjectSettings($arr[$effectivePage]["table"], $effectivePage);
}

/** 
* $lookupTableLinks array stores all lookup links between tables in the project
*/
function InitLookupLinks()
{
	global $lookupTableLinks;

	$lookupTableLinks = array();

	$lookupTableLinks["public.stato_interventi"]["public_interventi.Stato"]["edit"] = array("table" => "public.interventi", "field" => "Stato", "page" => "edit");
	$lookupTableLinks["public.incident"]["public_interventi.Incident"]["edit"] = array("table" => "public.interventi", "field" => "Incident", "page" => "edit");
	$lookupTableLinks["public.gruppi"]["public_interventi.Gruppo risolutore"]["edit"] = array("table" => "public.interventi", "field" => "Gruppo risolutore", "page" => "edit");
	$lookupTableLinks["public.elenco_clienti"]["public_incident.Cliente impattato"]["edit"] = array("table" => "public.incident", "field" => "Cliente impattato", "page" => "edit");
	$lookupTableLinks["public.gruppi"]["public_incident.Gruppo risolutore"]["edit"] = array("table" => "public.incident", "field" => "Gruppo risolutore", "page" => "edit");
	$lookupTableLinks["public.stato_gravita"]["public_incident.Gravità"]["edit"] = array("table" => "public.incident", "field" => "Gravità", "page" => "edit");
	$lookupTableLinks["public.tipo_causa"]["public_incident.Tipologia causa"]["edit"] = array("table" => "public.incident", "field" => "Tipologia causa", "page" => "edit");
}

?>