<?php
require_once(getabspath("classes/cipherer.php"));




$tdatapublic_incident = array();
	$tdatapublic_incident[".truncateText"] = true;
	$tdatapublic_incident[".NumberOfChars"] = 80;
	$tdatapublic_incident[".ShortName"] = "public_incident";
	$tdatapublic_incident[".OwnerID"] = "";
	$tdatapublic_incident[".OriginalTable"] = "public.incident";

//	field labels
$fieldLabelspublic_incident = array();
$fieldToolTipspublic_incident = array();
$pageTitlespublic_incident = array();

if(mlang_getcurrentlang()=="Italian")
{
	$fieldLabelspublic_incident["Italian"] = array();
	$fieldToolTipspublic_incident["Italian"] = array();
	$pageTitlespublic_incident["Italian"] = array();
	$fieldLabelspublic_incident["Italian"]["id_inc"] = "Id Inc";
	$fieldToolTipspublic_incident["Italian"]["id_inc"] = "";
	$fieldLabelspublic_incident["Italian"]["Cliente_impattato"] = "Cliente impattato";
	$fieldToolTipspublic_incident["Italian"]["Cliente_impattato"] = "";
	$fieldLabelspublic_incident["Italian"]["Risoluzione"] = "Risoluzione";
	$fieldToolTipspublic_incident["Italian"]["Risoluzione"] = "";
	$fieldLabelspublic_incident["Italian"]["Data_e_ora_iniziale"] = "Data e ora iniziale";
	$fieldToolTipspublic_incident["Italian"]["Data_e_ora_iniziale"] = "";
	$fieldLabelspublic_incident["Italian"]["Data_e_ora_chiusura"] = "Data e ora chiusura";
	$fieldToolTipspublic_incident["Italian"]["Data_e_ora_chiusura"] = "";
	$fieldLabelspublic_incident["Italian"]["Descrizione_incident"] = "Descrizione incident";
	$fieldToolTipspublic_incident["Italian"]["Descrizione_incident"] = "";
	$fieldLabelspublic_incident["Italian"]["In_perimetro_ISO27001_"] = "In perimetro ISO27001?";
	$fieldToolTipspublic_incident["Italian"]["In_perimetro_ISO27001_"] = "";
	$fieldLabelspublic_incident["Italian"]["Impatto_appl__oggetto"] = "Impatto appl. oggetto";
	$fieldToolTipspublic_incident["Italian"]["Impatto_appl__oggetto"] = "";
	$fieldLabelspublic_incident["Italian"]["Impatto_appl__normalizzato"] = "Impatto appl. normalizzato";
	$fieldToolTipspublic_incident["Italian"]["Impatto_appl__normalizzato"] = "";
	$fieldLabelspublic_incident["Italian"]["Gruppo_risolutore"] = "Gruppo risolutore";
	$fieldToolTipspublic_incident["Italian"]["Gruppo_risolutore"] = "";
	$fieldLabelspublic_incident["Italian"]["Gravit_"] = "Gravità";
	$fieldToolTipspublic_incident["Italian"]["Gravit_"] = "";
	$fieldLabelspublic_incident["Italian"]["Tipologia_causa"] = "Tipologia causa";
	$fieldToolTipspublic_incident["Italian"]["Tipologia_causa"] = "";
	$fieldLabelspublic_incident["Italian"]["Identificativo_Remedy"] = "Identificativo remedy";
	$fieldToolTipspublic_incident["Italian"]["Identificativo_Remedy"] = "";
	$fieldLabelspublic_incident["Italian"]["Segnalato_da"] = "Segnalato da";
	$fieldToolTipspublic_incident["Italian"]["Segnalato_da"] = "";
	$fieldLabelspublic_incident["Italian"]["Note"] = "Note";
	$fieldToolTipspublic_incident["Italian"]["Note"] = "";
	if (count($fieldToolTipspublic_incident["Italian"]))
		$tdatapublic_incident[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelspublic_incident[""] = array();
	$fieldToolTipspublic_incident[""] = array();
	$pageTitlespublic_incident[""] = array();
	$fieldLabelspublic_incident[""]["id_inc"] = "Id Inc";
	$fieldToolTipspublic_incident[""]["id_inc"] = "";
	$fieldLabelspublic_incident[""]["Cliente_impattato"] = "Cliente Impattato";
	$fieldToolTipspublic_incident[""]["Cliente_impattato"] = "";
	$fieldLabelspublic_incident[""]["Risoluzione"] = "Risoluzione";
	$fieldToolTipspublic_incident[""]["Risoluzione"] = "";
	$fieldLabelspublic_incident[""]["Data_e_ora_iniziale"] = "Data e ora iniziale";
	$fieldToolTipspublic_incident[""]["Data_e_ora_iniziale"] = "";
	$fieldLabelspublic_incident[""]["Data_e_ora_chiusura"] = "Data e ora chiusura";
	$fieldToolTipspublic_incident[""]["Data_e_ora_chiusura"] = "";
	$fieldLabelspublic_incident[""]["Descrizione_incident"] = "Descrizione Incident";
	$fieldToolTipspublic_incident[""]["Descrizione_incident"] = "";
	$fieldLabelspublic_incident[""]["In_perimetro_ISO27001_"] = "In perimetro ISO27001?";
	$fieldToolTipspublic_incident[""]["In_perimetro_ISO27001_"] = "";
	$fieldLabelspublic_incident[""]["Impatto_appl__oggetto"] = "Impatto appl. oggetto";
	$fieldToolTipspublic_incident[""]["Impatto_appl__oggetto"] = "";
	$fieldLabelspublic_incident[""]["Impatto_appl__normalizzato"] = "Impatto appl. normalizzato";
	$fieldToolTipspublic_incident[""]["Impatto_appl__normalizzato"] = "";
	$fieldLabelspublic_incident[""]["Gruppo_risolutore"] = "Gruppo Risolutore";
	$fieldToolTipspublic_incident[""]["Gruppo_risolutore"] = "";
	$fieldLabelspublic_incident[""]["Gravit_"] = "Gravità";
	$fieldToolTipspublic_incident[""]["Gravit_"] = "";
	$fieldLabelspublic_incident[""]["Tipologia_causa"] = "Tipologia Causa";
	$fieldToolTipspublic_incident[""]["Tipologia_causa"] = "";
	$fieldLabelspublic_incident[""]["Identificativo_Remedy"] = "Identificativo Remedy";
	$fieldToolTipspublic_incident[""]["Identificativo_Remedy"] = "";
	$fieldLabelspublic_incident[""]["Segnalato_da"] = "Segnalato Da";
	$fieldToolTipspublic_incident[""]["Segnalato_da"] = "";
	$fieldLabelspublic_incident[""]["Note"] = "Note";
	$fieldToolTipspublic_incident[""]["Note"] = "";
	if (count($fieldToolTipspublic_incident[""]))
		$tdatapublic_incident[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="English")
{
	$fieldLabelspublic_incident["English"] = array();
	$fieldToolTipspublic_incident["English"] = array();
	$pageTitlespublic_incident["English"] = array();
	$fieldLabelspublic_incident["English"]["Cliente_impattato"] = "Cliente Impattato";
	$fieldToolTipspublic_incident["English"]["Cliente_impattato"] = "";
	$fieldLabelspublic_incident["English"]["Risoluzione"] = "Risoluzione";
	$fieldToolTipspublic_incident["English"]["Risoluzione"] = "";
	$fieldLabelspublic_incident["English"]["Data_e_ora_iniziale"] = "Data e ora iniziale";
	$fieldToolTipspublic_incident["English"]["Data_e_ora_iniziale"] = "";
	$fieldLabelspublic_incident["English"]["Data_e_ora_chiusura"] = "Data e ora chiusura";
	$fieldToolTipspublic_incident["English"]["Data_e_ora_chiusura"] = "";
	$fieldLabelspublic_incident["English"]["Descrizione_incident"] = "Descrizione Incident";
	$fieldToolTipspublic_incident["English"]["Descrizione_incident"] = "";
	$fieldLabelspublic_incident["English"]["In_perimetro_ISO27001_"] = "In perimetro ISO27001?";
	$fieldToolTipspublic_incident["English"]["In_perimetro_ISO27001_"] = "";
	$fieldLabelspublic_incident["English"]["Impatto_appl__oggetto"] = "Impatto appl. oggetto";
	$fieldToolTipspublic_incident["English"]["Impatto_appl__oggetto"] = "";
	$fieldLabelspublic_incident["English"]["Impatto_appl__normalizzato"] = "Impatto appl. normalizzato";
	$fieldToolTipspublic_incident["English"]["Impatto_appl__normalizzato"] = "";
	$fieldLabelspublic_incident["English"]["Gruppo_risolutore"] = "Gruppo Risolutore";
	$fieldToolTipspublic_incident["English"]["Gruppo_risolutore"] = "";
	$fieldLabelspublic_incident["English"]["Gravit_"] = "Gravità";
	$fieldToolTipspublic_incident["English"]["Gravit_"] = "";
	$fieldLabelspublic_incident["English"]["Tipologia_causa"] = "Tipologia Causa";
	$fieldToolTipspublic_incident["English"]["Tipologia_causa"] = "";
	$fieldLabelspublic_incident["English"]["Identificativo_Remedy"] = "Identificativo Remedy";
	$fieldToolTipspublic_incident["English"]["Identificativo_Remedy"] = "";
	$fieldLabelspublic_incident["English"]["Segnalato_da"] = "Segnalato Da";
	$fieldToolTipspublic_incident["English"]["Segnalato_da"] = "";
	$fieldLabelspublic_incident["English"]["Note"] = "Note";
	$fieldToolTipspublic_incident["English"]["Note"] = "";
	if (count($fieldToolTipspublic_incident["English"]))
		$tdatapublic_incident[".isUseToolTips"] = true;
}


	$tdatapublic_incident[".NCSearch"] = true;



$tdatapublic_incident[".shortTableName"] = "public_incident";
$tdatapublic_incident[".nSecOptions"] = 0;
$tdatapublic_incident[".recsPerRowPrint"] = 1;
$tdatapublic_incident[".mainTableOwnerID"] = "";
$tdatapublic_incident[".moveNext"] = 1;
$tdatapublic_incident[".entityType"] = 0;

$tdatapublic_incident[".strOriginalTableName"] = "public.incident";

	



$tdatapublic_incident[".showAddInPopup"] = false;

$tdatapublic_incident[".showEditInPopup"] = false;

$tdatapublic_incident[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatapublic_incident[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatapublic_incident[".fieldsForRegister"] = array();

$tdatapublic_incident[".listAjax"] = false;

	$tdatapublic_incident[".audit"] = false;

	$tdatapublic_incident[".locking"] = false;

$tdatapublic_incident[".edit"] = true;
$tdatapublic_incident[".afterEditAction"] = 1;
$tdatapublic_incident[".closePopupAfterEdit"] = 1;
$tdatapublic_incident[".afterEditActionDetTable"] = "";

$tdatapublic_incident[".add"] = true;
$tdatapublic_incident[".afterAddAction"] = 1;
$tdatapublic_incident[".closePopupAfterAdd"] = 1;
$tdatapublic_incident[".afterAddActionDetTable"] = "";

$tdatapublic_incident[".list"] = true;



$tdatapublic_incident[".exportTo"] = true;

$tdatapublic_incident[".printFriendly"] = true;


$tdatapublic_incident[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdatapublic_incident[".searchSaving"] = true;
//

$tdatapublic_incident[".showSearchPanel"] = true;
		$tdatapublic_incident[".flexibleSearch"] = true;

$tdatapublic_incident[".isUseAjaxSuggest"] = true;

$tdatapublic_incident[".rowHighlite"] = true;



$tdatapublic_incident[".addPageEvents"] = false;

// use timepicker for search panel
$tdatapublic_incident[".isUseTimeForSearch"] = false;



$tdatapublic_incident[".badgeColor"] = "4682B4";


$tdatapublic_incident[".allSearchFields"] = array();
$tdatapublic_incident[".filterFields"] = array();
$tdatapublic_incident[".requiredSearchFields"] = array();

$tdatapublic_incident[".allSearchFields"][] = "Data e ora iniziale";
	$tdatapublic_incident[".allSearchFields"][] = "Data e ora chiusura";
	$tdatapublic_incident[".allSearchFields"][] = "Cliente impattato";
	$tdatapublic_incident[".allSearchFields"][] = "Descrizione incident";
	$tdatapublic_incident[".allSearchFields"][] = "In perimetro ISO27001?";
	$tdatapublic_incident[".allSearchFields"][] = "Impatto appl. oggetto";
	$tdatapublic_incident[".allSearchFields"][] = "Risoluzione";
	$tdatapublic_incident[".allSearchFields"][] = "Impatto appl. normalizzato";
	$tdatapublic_incident[".allSearchFields"][] = "Gruppo risolutore";
	$tdatapublic_incident[".allSearchFields"][] = "Gravità";
	$tdatapublic_incident[".allSearchFields"][] = "Identificativo Remedy";
	$tdatapublic_incident[".allSearchFields"][] = "Tipologia causa";
	$tdatapublic_incident[".allSearchFields"][] = "Segnalato da";
	

$tdatapublic_incident[".googleLikeFields"] = array();
$tdatapublic_incident[".googleLikeFields"][] = "Data e ora iniziale";
$tdatapublic_incident[".googleLikeFields"][] = "Data e ora chiusura";
$tdatapublic_incident[".googleLikeFields"][] = "Cliente impattato";
$tdatapublic_incident[".googleLikeFields"][] = "Descrizione incident";
$tdatapublic_incident[".googleLikeFields"][] = "In perimetro ISO27001?";
$tdatapublic_incident[".googleLikeFields"][] = "Impatto appl. oggetto";
$tdatapublic_incident[".googleLikeFields"][] = "Impatto appl. normalizzato";
$tdatapublic_incident[".googleLikeFields"][] = "Gruppo risolutore";
$tdatapublic_incident[".googleLikeFields"][] = "Risoluzione";
$tdatapublic_incident[".googleLikeFields"][] = "Gravità";
$tdatapublic_incident[".googleLikeFields"][] = "Tipologia causa";
$tdatapublic_incident[".googleLikeFields"][] = "Identificativo Remedy";
$tdatapublic_incident[".googleLikeFields"][] = "Segnalato da";


$tdatapublic_incident[".advSearchFields"] = array();
$tdatapublic_incident[".advSearchFields"][] = "Data e ora iniziale";
$tdatapublic_incident[".advSearchFields"][] = "Data e ora chiusura";
$tdatapublic_incident[".advSearchFields"][] = "Cliente impattato";
$tdatapublic_incident[".advSearchFields"][] = "Descrizione incident";
$tdatapublic_incident[".advSearchFields"][] = "In perimetro ISO27001?";
$tdatapublic_incident[".advSearchFields"][] = "Impatto appl. oggetto";
$tdatapublic_incident[".advSearchFields"][] = "Risoluzione";
$tdatapublic_incident[".advSearchFields"][] = "Impatto appl. normalizzato";
$tdatapublic_incident[".advSearchFields"][] = "Gruppo risolutore";
$tdatapublic_incident[".advSearchFields"][] = "Gravità";
$tdatapublic_incident[".advSearchFields"][] = "Identificativo Remedy";
$tdatapublic_incident[".advSearchFields"][] = "Tipologia causa";
$tdatapublic_incident[".advSearchFields"][] = "Segnalato da";

$tdatapublic_incident[".tableType"] = "list";

$tdatapublic_incident[".printerPageOrientation"] = 1;
$tdatapublic_incident[".isPrinterPageFitToPage"] = 0;
$tdatapublic_incident[".nPrinterPageScale"] = 100;

$tdatapublic_incident[".nPrinterSplitRecords"] = 40;

$tdatapublic_incident[".nPrinterPDFSplitRecords"] = 40;



$tdatapublic_incident[".geocodingEnabled"] = false;





$tdatapublic_incident[".listGridLayout"] = 3;





// view page pdf
$tdatapublic_incident[".isViewPagePDF"] = true;
$tdatapublic_incident[".isLandscapeViewPDFOrientation"] = 1;
$tdatapublic_incident[".isViewPagePDFFitToPage"] = 0;
$tdatapublic_incident[".nViewPagePDFScale"] = 100;

// print page pdf
$tdatapublic_incident[".isPrinterPagePDF"] = true;
$tdatapublic_incident[".isLandscapePrinterPagePDFOrientation"] = 1;
$tdatapublic_incident[".isPrinterPagePDFFitToPage"] = 0;
$tdatapublic_incident[".nPrinterPagePDFScale"] = 100;


$tdatapublic_incident[".pageSize"] = 20;

$tdatapublic_incident[".warnLeavingPages"] = true;



$tstrOrderBy = "ORDER BY id_inc DESC";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatapublic_incident[".strOrderBy"] = $tstrOrderBy;

$tdatapublic_incident[".orderindexes"] = array();
$tdatapublic_incident[".orderindexes"][] = array(1, (0 ? "ASC" : "DESC"), "id_inc");

$tdatapublic_incident[".sqlHead"] = "SELECT id_inc,  inc_data_ora_iniziale AS \"Data e ora iniziale\",  inc_data_ora_chiusura AS \"Data e ora chiusura\",  \"cliente impattato\" AS \"Cliente impattato\",  descrizione_incident AS \"Descrizione incident\",  perimetro_27001 AS \"In perimetro ISO27001?\",  oggetto_incident AS \"Impatto appl. oggetto\",  impatto_normalizzato AS \"Impatto appl. normalizzato\",  gruppo_risolutore AS \"Gruppo risolutore\",  risoluzione AS \"Risoluzione\",  inc_gravita AS \"Gravità\",  tipologia_causa AS \"Tipologia causa\",  identificativo_remedy AS \"Identificativo Remedy\",  note_incident AS \"Note\",  segnalato_da AS \"Segnalato da\"";
$tdatapublic_incident[".sqlFrom"] = "FROM \"public\".incident";
$tdatapublic_incident[".sqlWhereExpr"] = "";
$tdatapublic_incident[".sqlTail"] = "";











//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatapublic_incident[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatapublic_incident[".arrGroupsPerPage"] = $arrGPP;

$tdatapublic_incident[".highlightSearchResults"] = true;

$tableKeyspublic_incident = array();
$tableKeyspublic_incident[] = "id_inc";
$tdatapublic_incident[".Keys"] = $tableKeyspublic_incident;

$tdatapublic_incident[".listFields"] = array();
$tdatapublic_incident[".listFields"][] = "Data e ora iniziale";
$tdatapublic_incident[".listFields"][] = "Data e ora chiusura";
$tdatapublic_incident[".listFields"][] = "Cliente impattato";
$tdatapublic_incident[".listFields"][] = "Descrizione incident";
$tdatapublic_incident[".listFields"][] = "Impatto appl. oggetto";
$tdatapublic_incident[".listFields"][] = "Risoluzione";
$tdatapublic_incident[".listFields"][] = "Gravità";
$tdatapublic_incident[".listFields"][] = "Identificativo Remedy";
$tdatapublic_incident[".listFields"][] = "Note";

$tdatapublic_incident[".hideMobileList"] = array();


$tdatapublic_incident[".viewFields"] = array();

$tdatapublic_incident[".addFields"] = array();
$tdatapublic_incident[".addFields"][] = "Data e ora iniziale";
$tdatapublic_incident[".addFields"][] = "Data e ora chiusura";
$tdatapublic_incident[".addFields"][] = "Cliente impattato";
$tdatapublic_incident[".addFields"][] = "Descrizione incident";
$tdatapublic_incident[".addFields"][] = "In perimetro ISO27001?";
$tdatapublic_incident[".addFields"][] = "Impatto appl. oggetto";
$tdatapublic_incident[".addFields"][] = "Risoluzione";
$tdatapublic_incident[".addFields"][] = "Impatto appl. normalizzato";
$tdatapublic_incident[".addFields"][] = "Gruppo risolutore";
$tdatapublic_incident[".addFields"][] = "Gravità";
$tdatapublic_incident[".addFields"][] = "Identificativo Remedy";
$tdatapublic_incident[".addFields"][] = "Note";
$tdatapublic_incident[".addFields"][] = "Tipologia causa";
$tdatapublic_incident[".addFields"][] = "Segnalato da";

$tdatapublic_incident[".masterListFields"] = array();
$tdatapublic_incident[".masterListFields"][] = "Data e ora iniziale";
$tdatapublic_incident[".masterListFields"][] = "id_inc";
$tdatapublic_incident[".masterListFields"][] = "Data e ora chiusura";
$tdatapublic_incident[".masterListFields"][] = "Cliente impattato";
$tdatapublic_incident[".masterListFields"][] = "Descrizione incident";
$tdatapublic_incident[".masterListFields"][] = "In perimetro ISO27001?";
$tdatapublic_incident[".masterListFields"][] = "Impatto appl. oggetto";
$tdatapublic_incident[".masterListFields"][] = "Risoluzione";
$tdatapublic_incident[".masterListFields"][] = "Gravità";
$tdatapublic_incident[".masterListFields"][] = "Impatto appl. normalizzato";
$tdatapublic_incident[".masterListFields"][] = "Identificativo Remedy";
$tdatapublic_incident[".masterListFields"][] = "Gruppo risolutore";
$tdatapublic_incident[".masterListFields"][] = "Note";
$tdatapublic_incident[".masterListFields"][] = "Tipologia causa";
$tdatapublic_incident[".masterListFields"][] = "Segnalato da";

$tdatapublic_incident[".inlineAddFields"] = array();

$tdatapublic_incident[".editFields"] = array();
$tdatapublic_incident[".editFields"][] = "Data e ora iniziale";
$tdatapublic_incident[".editFields"][] = "Data e ora chiusura";
$tdatapublic_incident[".editFields"][] = "Cliente impattato";
$tdatapublic_incident[".editFields"][] = "Descrizione incident";
$tdatapublic_incident[".editFields"][] = "In perimetro ISO27001?";
$tdatapublic_incident[".editFields"][] = "Impatto appl. oggetto";
$tdatapublic_incident[".editFields"][] = "Risoluzione";
$tdatapublic_incident[".editFields"][] = "Impatto appl. normalizzato";
$tdatapublic_incident[".editFields"][] = "Gruppo risolutore";
$tdatapublic_incident[".editFields"][] = "Gravità";
$tdatapublic_incident[".editFields"][] = "Identificativo Remedy";
$tdatapublic_incident[".editFields"][] = "Note";
$tdatapublic_incident[".editFields"][] = "Tipologia causa";
$tdatapublic_incident[".editFields"][] = "Segnalato da";

$tdatapublic_incident[".inlineEditFields"] = array();

$tdatapublic_incident[".exportFields"] = array();
$tdatapublic_incident[".exportFields"][] = "Data e ora iniziale";
$tdatapublic_incident[".exportFields"][] = "Data e ora chiusura";
$tdatapublic_incident[".exportFields"][] = "Cliente impattato";
$tdatapublic_incident[".exportFields"][] = "Descrizione incident";
$tdatapublic_incident[".exportFields"][] = "In perimetro ISO27001?";
$tdatapublic_incident[".exportFields"][] = "Impatto appl. oggetto";
$tdatapublic_incident[".exportFields"][] = "Risoluzione";
$tdatapublic_incident[".exportFields"][] = "Impatto appl. normalizzato";
$tdatapublic_incident[".exportFields"][] = "Gruppo risolutore";
$tdatapublic_incident[".exportFields"][] = "Gravità";
$tdatapublic_incident[".exportFields"][] = "Identificativo Remedy";
$tdatapublic_incident[".exportFields"][] = "Note";
$tdatapublic_incident[".exportFields"][] = "Tipologia causa";
$tdatapublic_incident[".exportFields"][] = "Segnalato da";

$tdatapublic_incident[".importFields"] = array();

$tdatapublic_incident[".printFields"] = array();
$tdatapublic_incident[".printFields"][] = "Data e ora iniziale";
$tdatapublic_incident[".printFields"][] = "Data e ora chiusura";
$tdatapublic_incident[".printFields"][] = "Cliente impattato";
$tdatapublic_incident[".printFields"][] = "Descrizione incident";
$tdatapublic_incident[".printFields"][] = "In perimetro ISO27001?";
$tdatapublic_incident[".printFields"][] = "Impatto appl. oggetto";
$tdatapublic_incident[".printFields"][] = "Risoluzione";
$tdatapublic_incident[".printFields"][] = "Impatto appl. normalizzato";
$tdatapublic_incident[".printFields"][] = "Gruppo risolutore";
$tdatapublic_incident[".printFields"][] = "Gravità";
$tdatapublic_incident[".printFields"][] = "Identificativo Remedy";
$tdatapublic_incident[".printFields"][] = "Note";
$tdatapublic_incident[".printFields"][] = "Tipologia causa";
$tdatapublic_incident[".printFields"][] = "Segnalato da";

//	id_inc
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "id_inc";
	$fdata["GoodName"] = "id_inc";
	$fdata["ownerTable"] = "public.incident";
	$fdata["Label"] = GetFieldLabel("public_incident","id_inc");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "id_inc";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "id_inc";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "number";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_incident["id_inc"] = $fdata;
//	Data e ora iniziale
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "Data e ora iniziale";
	$fdata["GoodName"] = "Data_e_ora_iniziale";
	$fdata["ownerTable"] = "public.incident";
	$fdata["Label"] = GetFieldLabel("public_incident","Data_e_ora_iniziale");
	$fdata["FieldType"] = 135;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "inc_data_ora_iniziale";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "inc_data_ora_iniziale";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Datetime");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

		$edata["ShowTime"] = true;

	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 11;
	$edata["InitialYearFactor"] = 10;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings




	$tdatapublic_incident["Data e ora iniziale"] = $fdata;
//	Data e ora chiusura
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "Data e ora chiusura";
	$fdata["GoodName"] = "Data_e_ora_chiusura";
	$fdata["ownerTable"] = "public.incident";
	$fdata["Label"] = GetFieldLabel("public_incident","Data_e_ora_chiusura");
	$fdata["FieldType"] = 135;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "inc_data_ora_chiusura";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "inc_data_ora_chiusura";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Datetime");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

		$edata["ShowTime"] = true;

	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 11;
	$edata["InitialYearFactor"] = 10;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings




	$tdatapublic_incident["Data e ora chiusura"] = $fdata;
//	Cliente impattato
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "Cliente impattato";
	$fdata["GoodName"] = "Cliente_impattato";
	$fdata["ownerTable"] = "public.incident";
	$fdata["Label"] = GetFieldLabel("public_incident","Cliente_impattato");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "cliente impattato";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"cliente impattato\"";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "public.elenco_clienti";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "Cliente";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "Cliente";

	
	$edata["LookupOrderBy"] = "id_elenco_clienti";

	
	
	
	

	
		$edata["Multiselect"] = true;

		$edata["SelectSize"] = 1;

// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_incident["Cliente impattato"] = $fdata;
//	Descrizione incident
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "Descrizione incident";
	$fdata["GoodName"] = "Descrizione_incident";
	$fdata["ownerTable"] = "public.incident";
	$fdata["Label"] = GetFieldLabel("public_incident","Descrizione_incident");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "descrizione_incident";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "descrizione_incident";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_incident["Descrizione incident"] = $fdata;
//	In perimetro ISO27001?
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "In perimetro ISO27001?";
	$fdata["GoodName"] = "In_perimetro_ISO27001_";
	$fdata["ownerTable"] = "public.incident";
	$fdata["Label"] = GetFieldLabel("public_incident","In_perimetro_ISO27001_");
	$fdata["FieldType"] = 11;

	
	
	
			
	
		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "perimetro_27001";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "perimetro_27001";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Checkbox");

	
	
	
	
	
	
	
	
	
	
	
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Checkbox");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_incident["In perimetro ISO27001?"] = $fdata;
//	Impatto appl. oggetto
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "Impatto appl. oggetto";
	$fdata["GoodName"] = "Impatto_appl__oggetto";
	$fdata["ownerTable"] = "public.incident";
	$fdata["Label"] = GetFieldLabel("public_incident","Impatto_appl__oggetto");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "oggetto_incident";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "oggetto_incident";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_incident["Impatto appl. oggetto"] = $fdata;
//	Impatto appl. normalizzato
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "Impatto appl. normalizzato";
	$fdata["GoodName"] = "Impatto_appl__normalizzato";
	$fdata["ownerTable"] = "public.incident";
	$fdata["Label"] = GetFieldLabel("public_incident","Impatto_appl__normalizzato");
	$fdata["FieldType"] = 200;

	
	
	
			
	
		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "impatto_normalizzato";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "impatto_normalizzato";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_incident["Impatto appl. normalizzato"] = $fdata;
//	Gruppo risolutore
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 9;
	$fdata["strName"] = "Gruppo risolutore";
	$fdata["GoodName"] = "Gruppo_risolutore";
	$fdata["ownerTable"] = "public.incident";
	$fdata["Label"] = GetFieldLabel("public_incident","Gruppo_risolutore");
	$fdata["FieldType"] = 200;

	
	
	
			
	
		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "gruppo_risolutore";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "gruppo_risolutore";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "public.gruppi";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "Nome gruppo";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "Nome gruppo";

	
	$edata["LookupOrderBy"] = "id_gruppi";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_incident["Gruppo risolutore"] = $fdata;
//	Risoluzione
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 10;
	$fdata["strName"] = "Risoluzione";
	$fdata["GoodName"] = "Risoluzione";
	$fdata["ownerTable"] = "public.incident";
	$fdata["Label"] = GetFieldLabel("public_incident","Risoluzione");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "risoluzione";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "risoluzione";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=510";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_incident["Risoluzione"] = $fdata;
//	Gravità
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 11;
	$fdata["strName"] = "Gravità";
	$fdata["GoodName"] = "Gravit_";
	$fdata["ownerTable"] = "public.incident";
	$fdata["Label"] = GetFieldLabel("public_incident","Gravit_");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "inc_gravita";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "inc_gravita";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "public.stato_gravita";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "Gravità";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "Gravità";

	
	$edata["LookupOrderBy"] = "Gravità";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_incident["Gravità"] = $fdata;
//	Tipologia causa
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 12;
	$fdata["strName"] = "Tipologia causa";
	$fdata["GoodName"] = "Tipologia_causa";
	$fdata["ownerTable"] = "public.incident";
	$fdata["Label"] = GetFieldLabel("public_incident","Tipologia_causa");
	$fdata["FieldType"] = 200;

	
	
	
			
	
		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "tipologia_causa";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "tipologia_causa";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "public.tipo_causa";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "Tipologia causa";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "Tipologia causa";

	
	$edata["LookupOrderBy"] = "id_tipo_causa";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_incident["Tipologia causa"] = $fdata;
//	Identificativo Remedy
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 13;
	$fdata["strName"] = "Identificativo Remedy";
	$fdata["GoodName"] = "Identificativo_Remedy";
	$fdata["ownerTable"] = "public.incident";
	$fdata["Label"] = GetFieldLabel("public_incident","Identificativo_Remedy");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "identificativo_remedy";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "identificativo_remedy";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_incident["Identificativo Remedy"] = $fdata;
//	Note
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 14;
	$fdata["strName"] = "Note";
	$fdata["GoodName"] = "Note";
	$fdata["ownerTable"] = "public.incident";
	$fdata["Label"] = GetFieldLabel("public_incident","Note");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "note_incident";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "note_incident";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_incident["Note"] = $fdata;
//	Segnalato da
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 15;
	$fdata["strName"] = "Segnalato da";
	$fdata["GoodName"] = "Segnalato_da";
	$fdata["ownerTable"] = "public.incident";
	$fdata["Label"] = GetFieldLabel("public_incident","Segnalato_da");
	$fdata["FieldType"] = 200;

	
	
	
			
	
		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "segnalato_da";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "segnalato_da";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_incident["Segnalato da"] = $fdata;


$tables_data["public.incident"]=&$tdatapublic_incident;
$field_labels["public_incident"] = &$fieldLabelspublic_incident;
$fieldToolTips["public_incident"] = &$fieldToolTipspublic_incident;
$page_titles["public_incident"] = &$pageTitlespublic_incident;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["public.incident"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["public.incident"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_public_incident()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "id_inc,  inc_data_ora_iniziale AS \"Data e ora iniziale\",  inc_data_ora_chiusura AS \"Data e ora chiusura\",  \"cliente impattato\" AS \"Cliente impattato\",  descrizione_incident AS \"Descrizione incident\",  perimetro_27001 AS \"In perimetro ISO27001?\",  oggetto_incident AS \"Impatto appl. oggetto\",  impatto_normalizzato AS \"Impatto appl. normalizzato\",  gruppo_risolutore AS \"Gruppo risolutore\",  risoluzione AS \"Risoluzione\",  inc_gravita AS \"Gravità\",  tipologia_causa AS \"Tipologia causa\",  identificativo_remedy AS \"Identificativo Remedy\",  note_incident AS \"Note\",  segnalato_da AS \"Segnalato da\"";
$proto0["m_strFrom"] = "FROM \"public\".incident";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "ORDER BY id_inc DESC";
$proto0["m_strTail"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "id_inc",
	"m_strTable" => "public.incident",
	"m_srcTableName" => "public.incident"
));

$proto6["m_sql"] = "id_inc";
$proto6["m_srcTableName"] = "public.incident";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "inc_data_ora_iniziale",
	"m_strTable" => "public.incident",
	"m_srcTableName" => "public.incident"
));

$proto8["m_sql"] = "inc_data_ora_iniziale";
$proto8["m_srcTableName"] = "public.incident";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "Data e ora iniziale";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "inc_data_ora_chiusura",
	"m_strTable" => "public.incident",
	"m_srcTableName" => "public.incident"
));

$proto10["m_sql"] = "inc_data_ora_chiusura";
$proto10["m_srcTableName"] = "public.incident";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "Data e ora chiusura";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "cliente impattato",
	"m_strTable" => "public.incident",
	"m_srcTableName" => "public.incident"
));

$proto12["m_sql"] = "\"cliente impattato\"";
$proto12["m_srcTableName"] = "public.incident";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "Cliente impattato";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "descrizione_incident",
	"m_strTable" => "public.incident",
	"m_srcTableName" => "public.incident"
));

$proto14["m_sql"] = "descrizione_incident";
$proto14["m_srcTableName"] = "public.incident";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "Descrizione incident";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "perimetro_27001",
	"m_strTable" => "public.incident",
	"m_srcTableName" => "public.incident"
));

$proto16["m_sql"] = "perimetro_27001";
$proto16["m_srcTableName"] = "public.incident";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "In perimetro ISO27001?";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "oggetto_incident",
	"m_strTable" => "public.incident",
	"m_srcTableName" => "public.incident"
));

$proto18["m_sql"] = "oggetto_incident";
$proto18["m_srcTableName"] = "public.incident";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "Impatto appl. oggetto";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "impatto_normalizzato",
	"m_strTable" => "public.incident",
	"m_srcTableName" => "public.incident"
));

$proto20["m_sql"] = "impatto_normalizzato";
$proto20["m_srcTableName"] = "public.incident";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "Impatto appl. normalizzato";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
						$proto22=array();
			$obj = new SQLField(array(
	"m_strName" => "gruppo_risolutore",
	"m_strTable" => "public.incident",
	"m_srcTableName" => "public.incident"
));

$proto22["m_sql"] = "gruppo_risolutore";
$proto22["m_srcTableName"] = "public.incident";
$proto22["m_expr"]=$obj;
$proto22["m_alias"] = "Gruppo risolutore";
$obj = new SQLFieldListItem($proto22);

$proto0["m_fieldlist"][]=$obj;
						$proto24=array();
			$obj = new SQLField(array(
	"m_strName" => "risoluzione",
	"m_strTable" => "public.incident",
	"m_srcTableName" => "public.incident"
));

$proto24["m_sql"] = "risoluzione";
$proto24["m_srcTableName"] = "public.incident";
$proto24["m_expr"]=$obj;
$proto24["m_alias"] = "Risoluzione";
$obj = new SQLFieldListItem($proto24);

$proto0["m_fieldlist"][]=$obj;
						$proto26=array();
			$obj = new SQLField(array(
	"m_strName" => "inc_gravita",
	"m_strTable" => "public.incident",
	"m_srcTableName" => "public.incident"
));

$proto26["m_sql"] = "inc_gravita";
$proto26["m_srcTableName"] = "public.incident";
$proto26["m_expr"]=$obj;
$proto26["m_alias"] = "Gravità";
$obj = new SQLFieldListItem($proto26);

$proto0["m_fieldlist"][]=$obj;
						$proto28=array();
			$obj = new SQLField(array(
	"m_strName" => "tipologia_causa",
	"m_strTable" => "public.incident",
	"m_srcTableName" => "public.incident"
));

$proto28["m_sql"] = "tipologia_causa";
$proto28["m_srcTableName"] = "public.incident";
$proto28["m_expr"]=$obj;
$proto28["m_alias"] = "Tipologia causa";
$obj = new SQLFieldListItem($proto28);

$proto0["m_fieldlist"][]=$obj;
						$proto30=array();
			$obj = new SQLField(array(
	"m_strName" => "identificativo_remedy",
	"m_strTable" => "public.incident",
	"m_srcTableName" => "public.incident"
));

$proto30["m_sql"] = "identificativo_remedy";
$proto30["m_srcTableName"] = "public.incident";
$proto30["m_expr"]=$obj;
$proto30["m_alias"] = "Identificativo Remedy";
$obj = new SQLFieldListItem($proto30);

$proto0["m_fieldlist"][]=$obj;
						$proto32=array();
			$obj = new SQLField(array(
	"m_strName" => "note_incident",
	"m_strTable" => "public.incident",
	"m_srcTableName" => "public.incident"
));

$proto32["m_sql"] = "note_incident";
$proto32["m_srcTableName"] = "public.incident";
$proto32["m_expr"]=$obj;
$proto32["m_alias"] = "Note";
$obj = new SQLFieldListItem($proto32);

$proto0["m_fieldlist"][]=$obj;
						$proto34=array();
			$obj = new SQLField(array(
	"m_strName" => "segnalato_da",
	"m_strTable" => "public.incident",
	"m_srcTableName" => "public.incident"
));

$proto34["m_sql"] = "segnalato_da";
$proto34["m_srcTableName"] = "public.incident";
$proto34["m_expr"]=$obj;
$proto34["m_alias"] = "Segnalato da";
$obj = new SQLFieldListItem($proto34);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto36=array();
$proto36["m_link"] = "SQLL_MAIN";
			$proto37=array();
$proto37["m_strName"] = "public.incident";
$proto37["m_srcTableName"] = "public.incident";
$proto37["m_columns"] = array();
$proto37["m_columns"][] = "id_inc";
$proto37["m_columns"][] = "inc_data_ora_iniziale";
$proto37["m_columns"][] = "inc_data_ora_chiusura";
$proto37["m_columns"][] = "cliente impattato";
$proto37["m_columns"][] = "descrizione_incident";
$proto37["m_columns"][] = "perimetro_27001";
$proto37["m_columns"][] = "oggetto_incident";
$proto37["m_columns"][] = "impatto_normalizzato";
$proto37["m_columns"][] = "gruppo_risolutore";
$proto37["m_columns"][] = "risoluzione";
$proto37["m_columns"][] = "inc_gravita";
$proto37["m_columns"][] = "tipologia_causa";
$proto37["m_columns"][] = "identificativo_remedy";
$proto37["m_columns"][] = "note_incident";
$proto37["m_columns"][] = "segnalato_da";
$obj = new SQLTable($proto37);

$proto36["m_table"] = $obj;
$proto36["m_sql"] = "\"public\".incident";
$proto36["m_alias"] = "";
$proto36["m_srcTableName"] = "public.incident";
$proto38=array();
$proto38["m_sql"] = "";
$proto38["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto38["m_column"]=$obj;
$proto38["m_contained"] = array();
$proto38["m_strCase"] = "";
$proto38["m_havingmode"] = false;
$proto38["m_inBrackets"] = false;
$proto38["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto38);

$proto36["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto36);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
												$proto40=array();
						$obj = new SQLField(array(
	"m_strName" => "id_inc",
	"m_strTable" => "public.incident",
	"m_srcTableName" => "public.incident"
));

$proto40["m_column"]=$obj;
$proto40["m_bAsc"] = 0;
$proto40["m_nColumn"] = 0;
$obj = new SQLOrderByItem($proto40);

$proto0["m_orderby"][]=$obj;					
$proto0["m_srcTableName"]="public.incident";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_public_incident = createSqlQuery_public_incident();


	
		;

															

$tdatapublic_incident[".sqlquery"] = $queryData_public_incident;

include_once(getabspath("include/public_incident_events.php"));
$tableEvents["public.incident"] = new eventclass_public_incident;
$tdatapublic_incident[".hasEvents"] = true;

?>