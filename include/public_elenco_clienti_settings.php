<?php
require_once(getabspath("classes/cipherer.php"));




$tdatapublic_elenco_clienti = array();
	$tdatapublic_elenco_clienti[".truncateText"] = true;
	$tdatapublic_elenco_clienti[".NumberOfChars"] = 80;
	$tdatapublic_elenco_clienti[".ShortName"] = "public_elenco_clienti";
	$tdatapublic_elenco_clienti[".OwnerID"] = "";
	$tdatapublic_elenco_clienti[".OriginalTable"] = "public.elenco_clienti";

//	field labels
$fieldLabelspublic_elenco_clienti = array();
$fieldToolTipspublic_elenco_clienti = array();
$pageTitlespublic_elenco_clienti = array();

if(mlang_getcurrentlang()=="Italian")
{
	$fieldLabelspublic_elenco_clienti["Italian"] = array();
	$fieldToolTipspublic_elenco_clienti["Italian"] = array();
	$pageTitlespublic_elenco_clienti["Italian"] = array();
	$fieldLabelspublic_elenco_clienti["Italian"]["id_elenco_clienti"] = "Id Elenco Clienti";
	$fieldToolTipspublic_elenco_clienti["Italian"]["id_elenco_clienti"] = "";
	$fieldLabelspublic_elenco_clienti["Italian"]["Cliente"] = "Cliente";
	$fieldToolTipspublic_elenco_clienti["Italian"]["Cliente"] = "";
	if (count($fieldToolTipspublic_elenco_clienti["Italian"]))
		$tdatapublic_elenco_clienti[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelspublic_elenco_clienti[""] = array();
	$fieldToolTipspublic_elenco_clienti[""] = array();
	$pageTitlespublic_elenco_clienti[""] = array();
	$fieldLabelspublic_elenco_clienti[""]["id_elenco_clienti"] = "Id Elenco Clienti";
	$fieldToolTipspublic_elenco_clienti[""]["id_elenco_clienti"] = "";
	$fieldLabelspublic_elenco_clienti[""]["Cliente"] = "Cliente";
	$fieldToolTipspublic_elenco_clienti[""]["Cliente"] = "";
	if (count($fieldToolTipspublic_elenco_clienti[""]))
		$tdatapublic_elenco_clienti[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="English")
{
	$fieldLabelspublic_elenco_clienti["English"] = array();
	$fieldToolTipspublic_elenco_clienti["English"] = array();
	$pageTitlespublic_elenco_clienti["English"] = array();
	$fieldLabelspublic_elenco_clienti["English"]["id_elenco_clienti"] = "Id Elenco Clienti";
	$fieldToolTipspublic_elenco_clienti["English"]["id_elenco_clienti"] = "";
	$fieldLabelspublic_elenco_clienti["English"]["Cliente"] = "Cliente";
	$fieldToolTipspublic_elenco_clienti["English"]["Cliente"] = "";
	if (count($fieldToolTipspublic_elenco_clienti["English"]))
		$tdatapublic_elenco_clienti[".isUseToolTips"] = true;
}


	$tdatapublic_elenco_clienti[".NCSearch"] = true;



$tdatapublic_elenco_clienti[".shortTableName"] = "public_elenco_clienti";
$tdatapublic_elenco_clienti[".nSecOptions"] = 0;
$tdatapublic_elenco_clienti[".recsPerRowPrint"] = 1;
$tdatapublic_elenco_clienti[".mainTableOwnerID"] = "";
$tdatapublic_elenco_clienti[".moveNext"] = 1;
$tdatapublic_elenco_clienti[".entityType"] = 0;

$tdatapublic_elenco_clienti[".strOriginalTableName"] = "public.elenco_clienti";

	



$tdatapublic_elenco_clienti[".showAddInPopup"] = false;

$tdatapublic_elenco_clienti[".showEditInPopup"] = false;

$tdatapublic_elenco_clienti[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatapublic_elenco_clienti[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatapublic_elenco_clienti[".fieldsForRegister"] = array();

$tdatapublic_elenco_clienti[".listAjax"] = false;

	$tdatapublic_elenco_clienti[".audit"] = false;

	$tdatapublic_elenco_clienti[".locking"] = false;

$tdatapublic_elenco_clienti[".edit"] = true;
$tdatapublic_elenco_clienti[".afterEditAction"] = 1;
$tdatapublic_elenco_clienti[".closePopupAfterEdit"] = 1;
$tdatapublic_elenco_clienti[".afterEditActionDetTable"] = "";

$tdatapublic_elenco_clienti[".add"] = true;
$tdatapublic_elenco_clienti[".afterAddAction"] = 1;
$tdatapublic_elenco_clienti[".closePopupAfterAdd"] = 1;
$tdatapublic_elenco_clienti[".afterAddActionDetTable"] = "";

$tdatapublic_elenco_clienti[".list"] = true;





$tdatapublic_elenco_clienti[".delete"] = true;

$tdatapublic_elenco_clienti[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdatapublic_elenco_clienti[".searchSaving"] = false;
//

$tdatapublic_elenco_clienti[".showSearchPanel"] = true;
		$tdatapublic_elenco_clienti[".flexibleSearch"] = true;

$tdatapublic_elenco_clienti[".isUseAjaxSuggest"] = true;

$tdatapublic_elenco_clienti[".rowHighlite"] = true;



$tdatapublic_elenco_clienti[".addPageEvents"] = false;

// use timepicker for search panel
$tdatapublic_elenco_clienti[".isUseTimeForSearch"] = false;





$tdatapublic_elenco_clienti[".allSearchFields"] = array();
$tdatapublic_elenco_clienti[".filterFields"] = array();
$tdatapublic_elenco_clienti[".requiredSearchFields"] = array();

$tdatapublic_elenco_clienti[".allSearchFields"][] = "Cliente";
	

$tdatapublic_elenco_clienti[".googleLikeFields"] = array();
$tdatapublic_elenco_clienti[".googleLikeFields"][] = "id_elenco_clienti";
$tdatapublic_elenco_clienti[".googleLikeFields"][] = "Cliente";


$tdatapublic_elenco_clienti[".advSearchFields"] = array();
$tdatapublic_elenco_clienti[".advSearchFields"][] = "id_elenco_clienti";
$tdatapublic_elenco_clienti[".advSearchFields"][] = "Cliente";

$tdatapublic_elenco_clienti[".tableType"] = "list";

$tdatapublic_elenco_clienti[".printerPageOrientation"] = 0;
$tdatapublic_elenco_clienti[".nPrinterPageScale"] = 100;

$tdatapublic_elenco_clienti[".nPrinterSplitRecords"] = 40;

$tdatapublic_elenco_clienti[".nPrinterPDFSplitRecords"] = 40;



$tdatapublic_elenco_clienti[".geocodingEnabled"] = false;





$tdatapublic_elenco_clienti[".listGridLayout"] = 3;





// view page pdf
$tdatapublic_elenco_clienti[".isViewPagePDF"] = true;
$tdatapublic_elenco_clienti[".isLandscapeViewPDFOrientation"] = 1;
$tdatapublic_elenco_clienti[".isViewPagePDFFitToPage"] = 0;
$tdatapublic_elenco_clienti[".nViewPagePDFScale"] = 100;

// print page pdf
$tdatapublic_elenco_clienti[".isPrinterPagePDF"] = true;
$tdatapublic_elenco_clienti[".isLandscapePrinterPagePDFOrientation"] = 1;
$tdatapublic_elenco_clienti[".isPrinterPagePDFFitToPage"] = 0;
$tdatapublic_elenco_clienti[".nPrinterPagePDFScale"] = 100;


$tdatapublic_elenco_clienti[".pageSize"] = 20;

$tdatapublic_elenco_clienti[".warnLeavingPages"] = true;



$tstrOrderBy = "ORDER BY id_elenco_clienti DESC";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatapublic_elenco_clienti[".strOrderBy"] = $tstrOrderBy;

$tdatapublic_elenco_clienti[".orderindexes"] = array();
$tdatapublic_elenco_clienti[".orderindexes"][] = array(1, (0 ? "ASC" : "DESC"), "id_elenco_clienti");

$tdatapublic_elenco_clienti[".sqlHead"] = "SELECT id_elenco_clienti,  elenco_clienti AS \"Cliente\"";
$tdatapublic_elenco_clienti[".sqlFrom"] = "FROM \"public\".elenco_clienti";
$tdatapublic_elenco_clienti[".sqlWhereExpr"] = "";
$tdatapublic_elenco_clienti[".sqlTail"] = "";











//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatapublic_elenco_clienti[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatapublic_elenco_clienti[".arrGroupsPerPage"] = $arrGPP;

$tdatapublic_elenco_clienti[".highlightSearchResults"] = true;

$tableKeyspublic_elenco_clienti = array();
$tableKeyspublic_elenco_clienti[] = "id_elenco_clienti";
$tdatapublic_elenco_clienti[".Keys"] = $tableKeyspublic_elenco_clienti;

$tdatapublic_elenco_clienti[".listFields"] = array();
$tdatapublic_elenco_clienti[".listFields"][] = "Cliente";

$tdatapublic_elenco_clienti[".hideMobileList"] = array();


$tdatapublic_elenco_clienti[".viewFields"] = array();

$tdatapublic_elenco_clienti[".addFields"] = array();
$tdatapublic_elenco_clienti[".addFields"][] = "Cliente";

$tdatapublic_elenco_clienti[".masterListFields"] = array();
$tdatapublic_elenco_clienti[".masterListFields"][] = "id_elenco_clienti";
$tdatapublic_elenco_clienti[".masterListFields"][] = "Cliente";

$tdatapublic_elenco_clienti[".inlineAddFields"] = array();

$tdatapublic_elenco_clienti[".editFields"] = array();
$tdatapublic_elenco_clienti[".editFields"][] = "Cliente";

$tdatapublic_elenco_clienti[".inlineEditFields"] = array();

$tdatapublic_elenco_clienti[".exportFields"] = array();

$tdatapublic_elenco_clienti[".importFields"] = array();

$tdatapublic_elenco_clienti[".printFields"] = array();

//	id_elenco_clienti
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "id_elenco_clienti";
	$fdata["GoodName"] = "id_elenco_clienti";
	$fdata["ownerTable"] = "public.elenco_clienti";
	$fdata["Label"] = GetFieldLabel("public_elenco_clienti","id_elenco_clienti");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "id_elenco_clienti";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "id_elenco_clienti";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "number";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_elenco_clienti["id_elenco_clienti"] = $fdata;
//	Cliente
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "Cliente";
	$fdata["GoodName"] = "Cliente";
	$fdata["ownerTable"] = "public.elenco_clienti";
	$fdata["Label"] = GetFieldLabel("public_elenco_clienti","Cliente");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
	
		$fdata["bAdvancedSearch"] = true;

	
	
		$fdata["strField"] = "elenco_clienti";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "elenco_clienti";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
			$edata["validateAs"]["basicValidate"][] = "DenyDuplicated";
	$edata["validateAs"]["customMessages"]["DenyDuplicated"] = array("message" => "Questo cliente %value% esiste già!", "messageType" => "Text");

	
	//	End validation

	
			
	
		$edata["denyDuplicates"] = true;

	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_elenco_clienti["Cliente"] = $fdata;


$tables_data["public.elenco_clienti"]=&$tdatapublic_elenco_clienti;
$field_labels["public_elenco_clienti"] = &$fieldLabelspublic_elenco_clienti;
$fieldToolTips["public_elenco_clienti"] = &$fieldToolTipspublic_elenco_clienti;
$page_titles["public_elenco_clienti"] = &$pageTitlespublic_elenco_clienti;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["public.elenco_clienti"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["public.elenco_clienti"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_public_elenco_clienti()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "id_elenco_clienti,  elenco_clienti AS \"Cliente\"";
$proto0["m_strFrom"] = "FROM \"public\".elenco_clienti";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "ORDER BY id_elenco_clienti DESC";
$proto0["m_strTail"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "id_elenco_clienti",
	"m_strTable" => "public.elenco_clienti",
	"m_srcTableName" => "public.elenco_clienti"
));

$proto6["m_sql"] = "id_elenco_clienti";
$proto6["m_srcTableName"] = "public.elenco_clienti";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "elenco_clienti",
	"m_strTable" => "public.elenco_clienti",
	"m_srcTableName" => "public.elenco_clienti"
));

$proto8["m_sql"] = "elenco_clienti";
$proto8["m_srcTableName"] = "public.elenco_clienti";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "Cliente";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto10=array();
$proto10["m_link"] = "SQLL_MAIN";
			$proto11=array();
$proto11["m_strName"] = "public.elenco_clienti";
$proto11["m_srcTableName"] = "public.elenco_clienti";
$proto11["m_columns"] = array();
$proto11["m_columns"][] = "id_elenco_clienti";
$proto11["m_columns"][] = "elenco_clienti";
$obj = new SQLTable($proto11);

$proto10["m_table"] = $obj;
$proto10["m_sql"] = "\"public\".elenco_clienti";
$proto10["m_alias"] = "";
$proto10["m_srcTableName"] = "public.elenco_clienti";
$proto12=array();
$proto12["m_sql"] = "";
$proto12["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto12["m_column"]=$obj;
$proto12["m_contained"] = array();
$proto12["m_strCase"] = "";
$proto12["m_havingmode"] = false;
$proto12["m_inBrackets"] = false;
$proto12["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto12);

$proto10["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto10);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
												$proto14=array();
						$obj = new SQLField(array(
	"m_strName" => "id_elenco_clienti",
	"m_strTable" => "public.elenco_clienti",
	"m_srcTableName" => "public.elenco_clienti"
));

$proto14["m_column"]=$obj;
$proto14["m_bAsc"] = 0;
$proto14["m_nColumn"] = 0;
$obj = new SQLOrderByItem($proto14);

$proto0["m_orderby"][]=$obj;					
$proto0["m_srcTableName"]="public.elenco_clienti";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_public_elenco_clienti = createSqlQuery_public_elenco_clienti();


	
		;

		

$tdatapublic_elenco_clienti[".sqlquery"] = $queryData_public_elenco_clienti;

$tableEvents["public.elenco_clienti"] = new eventsBase;
$tdatapublic_elenco_clienti[".hasEvents"] = false;

?>