<?php
require_once(getabspath("classes/cipherer.php"));




$tdatapublic_interventi = array();
	$tdatapublic_interventi[".ShortName"] = "public_interventi";
	$tdatapublic_interventi[".OwnerID"] = "";
	$tdatapublic_interventi[".OriginalTable"] = "public.interventi";

//	field labels
$fieldLabelspublic_interventi = array();
$fieldToolTipspublic_interventi = array();
$pageTitlespublic_interventi = array();

if(mlang_getcurrentlang()=="Italian")
{
	$fieldLabelspublic_interventi["Italian"] = array();
	$fieldToolTipspublic_interventi["Italian"] = array();
	$pageTitlespublic_interventi["Italian"] = array();
	$fieldLabelspublic_interventi["Italian"]["id_int"] = "Id Intervento";
	$fieldToolTipspublic_interventi["Italian"]["id_int"] = "";
	$fieldLabelspublic_interventi["Italian"]["Stato"] = "Stato";
	$fieldToolTipspublic_interventi["Italian"]["Stato"] = "";
	$fieldLabelspublic_interventi["Italian"]["Note"] = "Note";
	$fieldToolTipspublic_interventi["Italian"]["Note"] = "";
	$fieldLabelspublic_interventi["Italian"]["Incident"] = "Incident";
	$fieldToolTipspublic_interventi["Italian"]["Incident"] = "";
	$fieldLabelspublic_interventi["Italian"]["Esito"] = "Esito";
	$fieldToolTipspublic_interventi["Italian"]["Esito"] = "";
	$fieldLabelspublic_interventi["Italian"]["Data_e_orario_iniziale"] = "Data e orario iniziale";
	$fieldToolTipspublic_interventi["Italian"]["Data_e_orario_iniziale"] = "";
	$fieldLabelspublic_interventi["Italian"]["Data_e_orario_chiusura"] = "Data e orario chiusura";
	$fieldToolTipspublic_interventi["Italian"]["Data_e_orario_chiusura"] = "";
	$fieldLabelspublic_interventi["Italian"]["Gruppo_risolutore"] = "Gruppo risolutore";
	$fieldToolTipspublic_interventi["Italian"]["Gruppo_risolutore"] = "";
	$fieldLabelspublic_interventi["Italian"]["Attivit_"] = "Attività";
	$fieldToolTipspublic_interventi["Italian"]["Attivit_"] = "";
	$fieldLabelspublic_interventi["Italian"]["Allegato_operativo"] = "Allegato operativo";
	$fieldToolTipspublic_interventi["Italian"]["Allegato_operativo"] = "";
	$fieldLabelspublic_interventi["Italian"]["Link_alla_KB"] = "Link alla KB";
	$fieldToolTipspublic_interventi["Italian"]["Link_alla_KB"] = "";
	if (count($fieldToolTipspublic_interventi["Italian"]))
		$tdatapublic_interventi[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelspublic_interventi[""] = array();
	$fieldToolTipspublic_interventi[""] = array();
	$pageTitlespublic_interventi[""] = array();
	$fieldLabelspublic_interventi[""]["Incident"] = "Incident";
	$fieldToolTipspublic_interventi[""]["Incident"] = "";
	$fieldLabelspublic_interventi[""]["Esito"] = "Esito";
	$fieldToolTipspublic_interventi[""]["Esito"] = "";
	$fieldLabelspublic_interventi[""]["Data_e_orario_iniziale"] = "Data e orario iniziale";
	$fieldToolTipspublic_interventi[""]["Data_e_orario_iniziale"] = "";
	$fieldLabelspublic_interventi[""]["Data_e_orario_chiusura"] = "Data e orario chiusura";
	$fieldToolTipspublic_interventi[""]["Data_e_orario_chiusura"] = "";
	$fieldLabelspublic_interventi[""]["Gruppo_risolutore"] = "Gruppo Risolutore";
	$fieldToolTipspublic_interventi[""]["Gruppo_risolutore"] = "";
	$fieldLabelspublic_interventi[""]["Attivit_"] = "Attività";
	$fieldToolTipspublic_interventi[""]["Attivit_"] = "";
	$fieldLabelspublic_interventi[""]["Allegato_operativo"] = "Allegato Operativo";
	$fieldToolTipspublic_interventi[""]["Allegato_operativo"] = "";
	$fieldLabelspublic_interventi[""]["Link_alla_KB"] = "Link alla KB";
	$fieldToolTipspublic_interventi[""]["Link_alla_KB"] = "";
	if (count($fieldToolTipspublic_interventi[""]))
		$tdatapublic_interventi[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="English")
{
	$fieldLabelspublic_interventi["English"] = array();
	$fieldToolTipspublic_interventi["English"] = array();
	$pageTitlespublic_interventi["English"] = array();
	$fieldLabelspublic_interventi["English"]["Incident"] = "Incident";
	$fieldToolTipspublic_interventi["English"]["Incident"] = "";
	$fieldLabelspublic_interventi["English"]["Esito"] = "Esito";
	$fieldToolTipspublic_interventi["English"]["Esito"] = "";
	$fieldLabelspublic_interventi["English"]["Data_e_orario_iniziale"] = "Data e orario iniziale";
	$fieldToolTipspublic_interventi["English"]["Data_e_orario_iniziale"] = "";
	$fieldLabelspublic_interventi["English"]["Data_e_orario_chiusura"] = "Data e orario chiusura";
	$fieldToolTipspublic_interventi["English"]["Data_e_orario_chiusura"] = "";
	$fieldLabelspublic_interventi["English"]["Gruppo_risolutore"] = "Gruppo Risolutore";
	$fieldToolTipspublic_interventi["English"]["Gruppo_risolutore"] = "";
	$fieldLabelspublic_interventi["English"]["Attivit_"] = "Attività";
	$fieldToolTipspublic_interventi["English"]["Attivit_"] = "";
	$fieldLabelspublic_interventi["English"]["Allegato_operativo"] = "Allegato Operativo";
	$fieldToolTipspublic_interventi["English"]["Allegato_operativo"] = "";
	$fieldLabelspublic_interventi["English"]["Link_alla_KB"] = "Link alla KB";
	$fieldToolTipspublic_interventi["English"]["Link_alla_KB"] = "";
	if (count($fieldToolTipspublic_interventi["English"]))
		$tdatapublic_interventi[".isUseToolTips"] = true;
}


	$tdatapublic_interventi[".NCSearch"] = true;



$tdatapublic_interventi[".shortTableName"] = "public_interventi";
$tdatapublic_interventi[".nSecOptions"] = 0;
$tdatapublic_interventi[".recsPerRowPrint"] = 1;
$tdatapublic_interventi[".mainTableOwnerID"] = "";
$tdatapublic_interventi[".moveNext"] = 1;
$tdatapublic_interventi[".entityType"] = 0;

$tdatapublic_interventi[".strOriginalTableName"] = "public.interventi";

	



$tdatapublic_interventi[".showAddInPopup"] = false;

$tdatapublic_interventi[".showEditInPopup"] = false;

$tdatapublic_interventi[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatapublic_interventi[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatapublic_interventi[".fieldsForRegister"] = array();

$tdatapublic_interventi[".listAjax"] = false;

	$tdatapublic_interventi[".audit"] = false;

	$tdatapublic_interventi[".locking"] = false;

$tdatapublic_interventi[".edit"] = true;
$tdatapublic_interventi[".afterEditAction"] = 1;
$tdatapublic_interventi[".closePopupAfterEdit"] = 1;
$tdatapublic_interventi[".afterEditActionDetTable"] = "";

$tdatapublic_interventi[".add"] = true;
$tdatapublic_interventi[".afterAddAction"] = 1;
$tdatapublic_interventi[".closePopupAfterAdd"] = 1;
$tdatapublic_interventi[".afterAddActionDetTable"] = "";

$tdatapublic_interventi[".list"] = true;



$tdatapublic_interventi[".exportTo"] = true;

$tdatapublic_interventi[".printFriendly"] = true;


$tdatapublic_interventi[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdatapublic_interventi[".searchSaving"] = true;
//

$tdatapublic_interventi[".showSearchPanel"] = true;
		$tdatapublic_interventi[".flexibleSearch"] = true;

$tdatapublic_interventi[".isUseAjaxSuggest"] = true;

$tdatapublic_interventi[".rowHighlite"] = true;



$tdatapublic_interventi[".addPageEvents"] = false;

// use timepicker for search panel
$tdatapublic_interventi[".isUseTimeForSearch"] = false;



$tdatapublic_interventi[".badgeColor"] = "4682B4";


$tdatapublic_interventi[".allSearchFields"] = array();
$tdatapublic_interventi[".filterFields"] = array();
$tdatapublic_interventi[".requiredSearchFields"] = array();

$tdatapublic_interventi[".allSearchFields"][] = "Data e orario iniziale";
	$tdatapublic_interventi[".allSearchFields"][] = "Data e orario chiusura";
	$tdatapublic_interventi[".allSearchFields"][] = "Incident";
	$tdatapublic_interventi[".allSearchFields"][] = "Gruppo risolutore";
	$tdatapublic_interventi[".allSearchFields"][] = "Attività";
	$tdatapublic_interventi[".allSearchFields"][] = "Esito";
	$tdatapublic_interventi[".allSearchFields"][] = "Stato";
	

$tdatapublic_interventi[".googleLikeFields"] = array();
$tdatapublic_interventi[".googleLikeFields"][] = "Data e orario iniziale";
$tdatapublic_interventi[".googleLikeFields"][] = "Data e orario chiusura";
$tdatapublic_interventi[".googleLikeFields"][] = "Stato";
$tdatapublic_interventi[".googleLikeFields"][] = "Incident";
$tdatapublic_interventi[".googleLikeFields"][] = "Gruppo risolutore";
$tdatapublic_interventi[".googleLikeFields"][] = "Attività";
$tdatapublic_interventi[".googleLikeFields"][] = "Esito";


$tdatapublic_interventi[".advSearchFields"] = array();
$tdatapublic_interventi[".advSearchFields"][] = "Data e orario iniziale";
$tdatapublic_interventi[".advSearchFields"][] = "Data e orario chiusura";
$tdatapublic_interventi[".advSearchFields"][] = "Incident";
$tdatapublic_interventi[".advSearchFields"][] = "Gruppo risolutore";
$tdatapublic_interventi[".advSearchFields"][] = "Attività";
$tdatapublic_interventi[".advSearchFields"][] = "Esito";
$tdatapublic_interventi[".advSearchFields"][] = "Stato";

$tdatapublic_interventi[".tableType"] = "list";

$tdatapublic_interventi[".printerPageOrientation"] = 1;
$tdatapublic_interventi[".isPrinterPageFitToPage"] = 0;
$tdatapublic_interventi[".nPrinterPageScale"] = 100;

$tdatapublic_interventi[".nPrinterSplitRecords"] = 40;

$tdatapublic_interventi[".nPrinterPDFSplitRecords"] = 40;



$tdatapublic_interventi[".geocodingEnabled"] = false;





$tdatapublic_interventi[".listGridLayout"] = 3;





// view page pdf
$tdatapublic_interventi[".isViewPagePDF"] = true;
$tdatapublic_interventi[".isLandscapeViewPDFOrientation"] = 1;
$tdatapublic_interventi[".isViewPagePDFFitToPage"] = 0;
$tdatapublic_interventi[".nViewPagePDFScale"] = 100;

// print page pdf
$tdatapublic_interventi[".isPrinterPagePDF"] = true;
$tdatapublic_interventi[".isLandscapePrinterPagePDFOrientation"] = 1;
$tdatapublic_interventi[".isPrinterPagePDFFitToPage"] = 0;
$tdatapublic_interventi[".nPrinterPagePDFScale"] = 100;


$tdatapublic_interventi[".pageSize"] = 20;

$tdatapublic_interventi[".warnLeavingPages"] = true;



$tstrOrderBy = "ORDER BY id_int DESC";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatapublic_interventi[".strOrderBy"] = $tstrOrderBy;

$tdatapublic_interventi[".orderindexes"] = array();
$tdatapublic_interventi[".orderindexes"][] = array(1, (0 ? "ASC" : "DESC"), "id_int");

$tdatapublic_interventi[".sqlHead"] = "SELECT id_int,  int_creato_il AS \"Data e orario iniziale\",  int_chiuso_il AS \"Data e orario chiusura\",  stato AS \"Stato\",  incident AS \"Incident\",  note AS \"Note\",  nome_gruppo AS \"Gruppo risolutore\",  attivita AS \"Attività\",  esito AS \"Esito\",  file_allegato AS \"Allegato operativo\",  int_link_kb AS \"Link alla KB\"";
$tdatapublic_interventi[".sqlFrom"] = "FROM \"public\".interventi";
$tdatapublic_interventi[".sqlWhereExpr"] = "";
$tdatapublic_interventi[".sqlTail"] = "";











//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatapublic_interventi[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatapublic_interventi[".arrGroupsPerPage"] = $arrGPP;

$tdatapublic_interventi[".highlightSearchResults"] = true;

$tableKeyspublic_interventi = array();
$tableKeyspublic_interventi[] = "id_int";
$tdatapublic_interventi[".Keys"] = $tableKeyspublic_interventi;

$tdatapublic_interventi[".listFields"] = array();
$tdatapublic_interventi[".listFields"][] = "Data e orario iniziale";
$tdatapublic_interventi[".listFields"][] = "Data e orario chiusura";
$tdatapublic_interventi[".listFields"][] = "Incident";
$tdatapublic_interventi[".listFields"][] = "Gruppo risolutore";
$tdatapublic_interventi[".listFields"][] = "Attività";
$tdatapublic_interventi[".listFields"][] = "Esito";
$tdatapublic_interventi[".listFields"][] = "Stato";
$tdatapublic_interventi[".listFields"][] = "Note";
$tdatapublic_interventi[".listFields"][] = "Allegato operativo";
$tdatapublic_interventi[".listFields"][] = "Link alla KB";

$tdatapublic_interventi[".hideMobileList"] = array();


$tdatapublic_interventi[".viewFields"] = array();

$tdatapublic_interventi[".addFields"] = array();
$tdatapublic_interventi[".addFields"][] = "Data e orario iniziale";
$tdatapublic_interventi[".addFields"][] = "Data e orario chiusura";
$tdatapublic_interventi[".addFields"][] = "Incident";
$tdatapublic_interventi[".addFields"][] = "Gruppo risolutore";
$tdatapublic_interventi[".addFields"][] = "Attività";
$tdatapublic_interventi[".addFields"][] = "Esito";
$tdatapublic_interventi[".addFields"][] = "Stato";
$tdatapublic_interventi[".addFields"][] = "Note";
$tdatapublic_interventi[".addFields"][] = "Allegato operativo";
$tdatapublic_interventi[".addFields"][] = "Link alla KB";

$tdatapublic_interventi[".masterListFields"] = array();
$tdatapublic_interventi[".masterListFields"][] = "Data e orario iniziale";
$tdatapublic_interventi[".masterListFields"][] = "id_int";
$tdatapublic_interventi[".masterListFields"][] = "Data e orario chiusura";
$tdatapublic_interventi[".masterListFields"][] = "Incident";
$tdatapublic_interventi[".masterListFields"][] = "Gruppo risolutore";
$tdatapublic_interventi[".masterListFields"][] = "Attività";
$tdatapublic_interventi[".masterListFields"][] = "Esito";
$tdatapublic_interventi[".masterListFields"][] = "Stato";
$tdatapublic_interventi[".masterListFields"][] = "Note";
$tdatapublic_interventi[".masterListFields"][] = "Allegato operativo";
$tdatapublic_interventi[".masterListFields"][] = "Link alla KB";

$tdatapublic_interventi[".inlineAddFields"] = array();

$tdatapublic_interventi[".editFields"] = array();
$tdatapublic_interventi[".editFields"][] = "Data e orario iniziale";
$tdatapublic_interventi[".editFields"][] = "Data e orario chiusura";
$tdatapublic_interventi[".editFields"][] = "Incident";
$tdatapublic_interventi[".editFields"][] = "Gruppo risolutore";
$tdatapublic_interventi[".editFields"][] = "Attività";
$tdatapublic_interventi[".editFields"][] = "Esito";
$tdatapublic_interventi[".editFields"][] = "Stato";
$tdatapublic_interventi[".editFields"][] = "Note";
$tdatapublic_interventi[".editFields"][] = "Allegato operativo";
$tdatapublic_interventi[".editFields"][] = "Link alla KB";

$tdatapublic_interventi[".inlineEditFields"] = array();

$tdatapublic_interventi[".exportFields"] = array();
$tdatapublic_interventi[".exportFields"][] = "Data e orario iniziale";
$tdatapublic_interventi[".exportFields"][] = "Data e orario chiusura";
$tdatapublic_interventi[".exportFields"][] = "Incident";
$tdatapublic_interventi[".exportFields"][] = "Gruppo risolutore";
$tdatapublic_interventi[".exportFields"][] = "Attività";
$tdatapublic_interventi[".exportFields"][] = "Esito";
$tdatapublic_interventi[".exportFields"][] = "Stato";
$tdatapublic_interventi[".exportFields"][] = "Note";

$tdatapublic_interventi[".importFields"] = array();

$tdatapublic_interventi[".printFields"] = array();
$tdatapublic_interventi[".printFields"][] = "Data e orario iniziale";
$tdatapublic_interventi[".printFields"][] = "Data e orario chiusura";
$tdatapublic_interventi[".printFields"][] = "Incident";
$tdatapublic_interventi[".printFields"][] = "Gruppo risolutore";
$tdatapublic_interventi[".printFields"][] = "Attività";
$tdatapublic_interventi[".printFields"][] = "Esito";
$tdatapublic_interventi[".printFields"][] = "Stato";
$tdatapublic_interventi[".printFields"][] = "Note";

//	id_int
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "id_int";
	$fdata["GoodName"] = "id_int";
	$fdata["ownerTable"] = "public.interventi";
	$fdata["Label"] = GetFieldLabel("public_interventi","id_int");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "id_int";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "id_int";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "number";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_interventi["id_int"] = $fdata;
//	Data e orario iniziale
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "Data e orario iniziale";
	$fdata["GoodName"] = "Data_e_orario_iniziale";
	$fdata["ownerTable"] = "public.interventi";
	$fdata["Label"] = GetFieldLabel("public_interventi","Data_e_orario_iniziale");
	$fdata["FieldType"] = 135;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "int_creato_il";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "int_creato_il";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Datetime");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

		$edata["ShowTime"] = true;

	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 11;
	$edata["InitialYearFactor"] = 10;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings




	$tdatapublic_interventi["Data e orario iniziale"] = $fdata;
//	Data e orario chiusura
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "Data e orario chiusura";
	$fdata["GoodName"] = "Data_e_orario_chiusura";
	$fdata["ownerTable"] = "public.interventi";
	$fdata["Label"] = GetFieldLabel("public_interventi","Data_e_orario_chiusura");
	$fdata["FieldType"] = 135;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "int_chiuso_il";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "int_chiuso_il";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Datetime");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Date");

		$edata["ShowTime"] = true;

	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
		$edata["DateEditType"] = 11;
	$edata["InitialYearFactor"] = 10;
	$edata["LastYearFactor"] = 10;

	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Equals", "More than", "Less than", "Between", EMPTY_SEARCH, NOT_EMPTY );
// the end of search options settings




	$tdatapublic_interventi["Data e orario chiusura"] = $fdata;
//	Stato
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "Stato";
	$fdata["GoodName"] = "Stato";
	$fdata["ownerTable"] = "public.interventi";
	$fdata["Label"] = GetFieldLabel("public_interventi","Stato");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "stato";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "stato";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "public.stato_interventi";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "Stato intervento";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "stato_intervento";

		$edata["CustomDisplay"] = "true";

	$edata["LookupOrderBy"] = "id_stato_int";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_interventi["Stato"] = $fdata;
//	Incident
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "Incident";
	$fdata["GoodName"] = "Incident";
	$fdata["ownerTable"] = "public.interventi";
	$fdata["Label"] = GetFieldLabel("public_interventi","Incident");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "incident";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "incident";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "public.incident";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "id_inc";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "id_inc || ' ' || oggetto_incident || ' ' || identificativo_remedy";

		$edata["CustomDisplay"] = "true";

	$edata["LookupOrderBy"] = "id_inc";

		$edata["LookupDesc"] = true;

	
	
	

	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_interventi["Incident"] = $fdata;
//	Note
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "Note";
	$fdata["GoodName"] = "Note";
	$fdata["ownerTable"] = "public.interventi";
	$fdata["Label"] = GetFieldLabel("public_interventi","Note");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
	
	
		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "note";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "note";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=510";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_interventi["Note"] = $fdata;
//	Gruppo risolutore
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "Gruppo risolutore";
	$fdata["GoodName"] = "Gruppo_risolutore";
	$fdata["ownerTable"] = "public.interventi";
	$fdata["Label"] = GetFieldLabel("public_interventi","Gruppo_risolutore");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "nome_gruppo";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "nome_gruppo";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Lookup wizard");

	
	

// Begin Lookup settings
				$edata["LookupType"] = 2;
	$edata["LookupTable"] = "public.gruppi";
		$edata["autoCompleteFieldsOnEdit"] = 0;
	$edata["autoCompleteFields"] = array();
		$edata["LCType"] = 0;

	
		
	$edata["LinkField"] = "Nome gruppo";
	$edata["LinkFieldType"] = 0;
	$edata["DisplayField"] = "Nome gruppo";

	
	$edata["LookupOrderBy"] = "id_gruppi";

	
	
	
	

	
	
		$edata["SelectSize"] = 1;

// End Lookup Settings


		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Equals";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_interventi["Gruppo risolutore"] = $fdata;
//	Attività
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 8;
	$fdata["strName"] = "Attività";
	$fdata["GoodName"] = "Attivit_";
	$fdata["ownerTable"] = "public.interventi";
	$fdata["Label"] = GetFieldLabel("public_interventi","Attivit_");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "attivita";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "attivita";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_interventi["Attività"] = $fdata;
//	Esito
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 9;
	$fdata["strName"] = "Esito";
	$fdata["GoodName"] = "Esito";
	$fdata["ownerTable"] = "public.interventi";
	$fdata["Label"] = GetFieldLabel("public_interventi","Esito");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
	
		$fdata["bAdvancedSearch"] = true;

		$fdata["bPrinterPage"] = true;

		$fdata["bExportPage"] = true;

		$fdata["strField"] = "esito";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "esito";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
			$edata["EditParams"].= " maxlength=510";

		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;




// the field's search options settings
		$fdata["defaultSearchOption"] = "Contains";

			// the default search options list
				$fdata["searchOptionsList"] = array("Contains", "Equals", "Starts with", "More than", "Less than", "Between", "Empty", NOT_EMPTY);
// the end of search options settings




	$tdatapublic_interventi["Esito"] = $fdata;
//	Allegato operativo
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 10;
	$fdata["strName"] = "Allegato operativo";
	$fdata["GoodName"] = "Allegato_operativo";
	$fdata["ownerTable"] = "public.interventi";
	$fdata["Label"] = GetFieldLabel("public_interventi","Allegato_operativo");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
	
	
	
	
		$fdata["strField"] = "file_allegato";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "file_allegato";

		$fdata["DeleteAssociatedFile"] = true;

	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Document Download");

	
	
	
						$vdata["ShowFileSize"] = true;
			$vdata["ShowIcon"] = true;
		
	
	
	
	
	
	
	
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Document upload");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
	
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_interventi["Allegato operativo"] = $fdata;
//	Link alla KB
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 11;
	$fdata["strName"] = "Link alla KB";
	$fdata["GoodName"] = "Link_alla_KB";
	$fdata["ownerTable"] = "public.interventi";
	$fdata["Label"] = GetFieldLabel("public_interventi","Link_alla_KB");
	$fdata["FieldType"] = 200;

	
	
	
			
		$fdata["bListPage"] = true;

		$fdata["bAddPage"] = true;

	
		$fdata["bEditPage"] = true;

	
	
	
	
	
		$fdata["strField"] = "int_link_kb";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "int_link_kb";

	
	
				$fdata["FieldPermissions"] = true;

				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "Hyperlink");

	
	
	
	
				$vdata["hlNewWindow"] = true;
	$vdata["hlType"] = 1;
	$vdata["hlLinkWordNameType"] = "Text";
	$vdata["hlLinkWordText"] = "Link alla KB (interna/esterna)";
	$vdata["hlTitleField"] = "Link alla KB";

	
	
	
	
	
	
	
	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "url";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Regular expression");
				$edata["validateAs"]["regExp"] = "(https?:\\/\\/(?:www\\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\\.[^\\s]{2,}|www\\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\\.[^\\s]{2,}|https?:\\/\\/(?:www\\.|(?!www))[a-zA-Z0-9]+\\.[^\\s]{2,}|www\\.[a-zA-Z0-9]+\\.[^\\s]{2,})";
	$edata["validateAs"]["customMessages"]["RegExp"] = array("message" => "URL inserito non valido!", "messageType" => "Text");
				
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_interventi["Link alla KB"] = $fdata;


$tables_data["public.interventi"]=&$tdatapublic_interventi;
$field_labels["public_interventi"] = &$fieldLabelspublic_interventi;
$fieldToolTips["public_interventi"] = &$fieldToolTipspublic_interventi;
$page_titles["public_interventi"] = &$pageTitlespublic_interventi;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["public.interventi"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["public.interventi"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_public_interventi()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "id_int,  int_creato_il AS \"Data e orario iniziale\",  int_chiuso_il AS \"Data e orario chiusura\",  stato AS \"Stato\",  incident AS \"Incident\",  note AS \"Note\",  nome_gruppo AS \"Gruppo risolutore\",  attivita AS \"Attività\",  esito AS \"Esito\",  file_allegato AS \"Allegato operativo\",  int_link_kb AS \"Link alla KB\"";
$proto0["m_strFrom"] = "FROM \"public\".interventi";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "ORDER BY id_int DESC";
$proto0["m_strTail"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "id_int",
	"m_strTable" => "public.interventi",
	"m_srcTableName" => "public.interventi"
));

$proto6["m_sql"] = "id_int";
$proto6["m_srcTableName"] = "public.interventi";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "int_creato_il",
	"m_strTable" => "public.interventi",
	"m_srcTableName" => "public.interventi"
));

$proto8["m_sql"] = "int_creato_il";
$proto8["m_srcTableName"] = "public.interventi";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "Data e orario iniziale";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "int_chiuso_il",
	"m_strTable" => "public.interventi",
	"m_srcTableName" => "public.interventi"
));

$proto10["m_sql"] = "int_chiuso_il";
$proto10["m_srcTableName"] = "public.interventi";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "Data e orario chiusura";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "stato",
	"m_strTable" => "public.interventi",
	"m_srcTableName" => "public.interventi"
));

$proto12["m_sql"] = "stato";
$proto12["m_srcTableName"] = "public.interventi";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "Stato";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "incident",
	"m_strTable" => "public.interventi",
	"m_srcTableName" => "public.interventi"
));

$proto14["m_sql"] = "incident";
$proto14["m_srcTableName"] = "public.interventi";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "Incident";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "note",
	"m_strTable" => "public.interventi",
	"m_srcTableName" => "public.interventi"
));

$proto16["m_sql"] = "note";
$proto16["m_srcTableName"] = "public.interventi";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "Note";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "nome_gruppo",
	"m_strTable" => "public.interventi",
	"m_srcTableName" => "public.interventi"
));

$proto18["m_sql"] = "nome_gruppo";
$proto18["m_srcTableName"] = "public.interventi";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "Gruppo risolutore";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
						$proto20=array();
			$obj = new SQLField(array(
	"m_strName" => "attivita",
	"m_strTable" => "public.interventi",
	"m_srcTableName" => "public.interventi"
));

$proto20["m_sql"] = "attivita";
$proto20["m_srcTableName"] = "public.interventi";
$proto20["m_expr"]=$obj;
$proto20["m_alias"] = "Attività";
$obj = new SQLFieldListItem($proto20);

$proto0["m_fieldlist"][]=$obj;
						$proto22=array();
			$obj = new SQLField(array(
	"m_strName" => "esito",
	"m_strTable" => "public.interventi",
	"m_srcTableName" => "public.interventi"
));

$proto22["m_sql"] = "esito";
$proto22["m_srcTableName"] = "public.interventi";
$proto22["m_expr"]=$obj;
$proto22["m_alias"] = "Esito";
$obj = new SQLFieldListItem($proto22);

$proto0["m_fieldlist"][]=$obj;
						$proto24=array();
			$obj = new SQLField(array(
	"m_strName" => "file_allegato",
	"m_strTable" => "public.interventi",
	"m_srcTableName" => "public.interventi"
));

$proto24["m_sql"] = "file_allegato";
$proto24["m_srcTableName"] = "public.interventi";
$proto24["m_expr"]=$obj;
$proto24["m_alias"] = "Allegato operativo";
$obj = new SQLFieldListItem($proto24);

$proto0["m_fieldlist"][]=$obj;
						$proto26=array();
			$obj = new SQLField(array(
	"m_strName" => "int_link_kb",
	"m_strTable" => "public.interventi",
	"m_srcTableName" => "public.interventi"
));

$proto26["m_sql"] = "int_link_kb";
$proto26["m_srcTableName"] = "public.interventi";
$proto26["m_expr"]=$obj;
$proto26["m_alias"] = "Link alla KB";
$obj = new SQLFieldListItem($proto26);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto28=array();
$proto28["m_link"] = "SQLL_MAIN";
			$proto29=array();
$proto29["m_strName"] = "public.interventi";
$proto29["m_srcTableName"] = "public.interventi";
$proto29["m_columns"] = array();
$proto29["m_columns"][] = "id_int";
$proto29["m_columns"][] = "int_creato_il";
$proto29["m_columns"][] = "int_chiuso_il";
$proto29["m_columns"][] = "stato";
$proto29["m_columns"][] = "incident";
$proto29["m_columns"][] = "note";
$proto29["m_columns"][] = "nome_gruppo";
$proto29["m_columns"][] = "attivita";
$proto29["m_columns"][] = "esito";
$proto29["m_columns"][] = "file_allegato";
$proto29["m_columns"][] = "int_link_kb";
$obj = new SQLTable($proto29);

$proto28["m_table"] = $obj;
$proto28["m_sql"] = "\"public\".interventi";
$proto28["m_alias"] = "";
$proto28["m_srcTableName"] = "public.interventi";
$proto30=array();
$proto30["m_sql"] = "";
$proto30["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto30["m_column"]=$obj;
$proto30["m_contained"] = array();
$proto30["m_strCase"] = "";
$proto30["m_havingmode"] = false;
$proto30["m_inBrackets"] = false;
$proto30["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto30);

$proto28["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto28);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
												$proto32=array();
						$obj = new SQLField(array(
	"m_strName" => "id_int",
	"m_strTable" => "public.interventi",
	"m_srcTableName" => "public.interventi"
));

$proto32["m_column"]=$obj;
$proto32["m_bAsc"] = 0;
$proto32["m_nColumn"] = 0;
$obj = new SQLOrderByItem($proto32);

$proto0["m_orderby"][]=$obj;					
$proto0["m_srcTableName"]="public.interventi";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_public_interventi = createSqlQuery_public_interventi();


	
		;

											

$tdatapublic_interventi[".sqlquery"] = $queryData_public_interventi;

include_once(getabspath("include/public_interventi_events.php"));
$tableEvents["public.interventi"] = new eventclass_public_interventi;
$tdatapublic_interventi[".hasEvents"] = true;

?>