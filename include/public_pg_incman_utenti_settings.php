<?php
require_once(getabspath("classes/cipherer.php"));




$tdatapublic_pg_incman_utenti = array();
	$tdatapublic_pg_incman_utenti[".truncateText"] = true;
	$tdatapublic_pg_incman_utenti[".NumberOfChars"] = 80;
	$tdatapublic_pg_incman_utenti[".ShortName"] = "public_pg_incman_utenti";
	$tdatapublic_pg_incman_utenti[".OwnerID"] = "";
	$tdatapublic_pg_incman_utenti[".OriginalTable"] = "public.pg-incman_utenti";

//	field labels
$fieldLabelspublic_pg_incman_utenti = array();
$fieldToolTipspublic_pg_incman_utenti = array();
$pageTitlespublic_pg_incman_utenti = array();

if(mlang_getcurrentlang()=="Italian")
{
	$fieldLabelspublic_pg_incman_utenti["Italian"] = array();
	$fieldToolTipspublic_pg_incman_utenti["Italian"] = array();
	$pageTitlespublic_pg_incman_utenti["Italian"] = array();
	$fieldLabelspublic_pg_incman_utenti["Italian"]["ID"] = "ID";
	$fieldToolTipspublic_pg_incman_utenti["Italian"]["ID"] = "";
	$fieldLabelspublic_pg_incman_utenti["Italian"]["username"] = "Username";
	$fieldToolTipspublic_pg_incman_utenti["Italian"]["username"] = "";
	$fieldLabelspublic_pg_incman_utenti["Italian"]["password"] = "Password";
	$fieldToolTipspublic_pg_incman_utenti["Italian"]["password"] = "";
	$fieldLabelspublic_pg_incman_utenti["Italian"]["email"] = "Email";
	$fieldToolTipspublic_pg_incman_utenti["Italian"]["email"] = "";
	$fieldLabelspublic_pg_incman_utenti["Italian"]["fullname"] = "Nome esteso";
	$fieldToolTipspublic_pg_incman_utenti["Italian"]["fullname"] = "";
	$fieldLabelspublic_pg_incman_utenti["Italian"]["groupid"] = "Groupid";
	$fieldToolTipspublic_pg_incman_utenti["Italian"]["groupid"] = "";
	$fieldLabelspublic_pg_incman_utenti["Italian"]["active"] = "Attivo";
	$fieldToolTipspublic_pg_incman_utenti["Italian"]["active"] = "";
	if (count($fieldToolTipspublic_pg_incman_utenti["Italian"]))
		$tdatapublic_pg_incman_utenti[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="")
{
	$fieldLabelspublic_pg_incman_utenti[""] = array();
	$fieldToolTipspublic_pg_incman_utenti[""] = array();
	$pageTitlespublic_pg_incman_utenti[""] = array();
	$fieldLabelspublic_pg_incman_utenti[""]["ID"] = "ID";
	$fieldToolTipspublic_pg_incman_utenti[""]["ID"] = "";
	$fieldLabelspublic_pg_incman_utenti[""]["username"] = "Username";
	$fieldToolTipspublic_pg_incman_utenti[""]["username"] = "";
	$fieldLabelspublic_pg_incman_utenti[""]["password"] = "Password";
	$fieldToolTipspublic_pg_incman_utenti[""]["password"] = "";
	$fieldLabelspublic_pg_incman_utenti[""]["email"] = "Email";
	$fieldToolTipspublic_pg_incman_utenti[""]["email"] = "";
	$fieldLabelspublic_pg_incman_utenti[""]["fullname"] = "Fullname";
	$fieldToolTipspublic_pg_incman_utenti[""]["fullname"] = "";
	$fieldLabelspublic_pg_incman_utenti[""]["groupid"] = "Groupid";
	$fieldToolTipspublic_pg_incman_utenti[""]["groupid"] = "";
	$fieldLabelspublic_pg_incman_utenti[""]["active"] = "Active";
	$fieldToolTipspublic_pg_incman_utenti[""]["active"] = "";
	if (count($fieldToolTipspublic_pg_incman_utenti[""]))
		$tdatapublic_pg_incman_utenti[".isUseToolTips"] = true;
}
if(mlang_getcurrentlang()=="English")
{
	$fieldLabelspublic_pg_incman_utenti["English"] = array();
	$fieldToolTipspublic_pg_incman_utenti["English"] = array();
	$pageTitlespublic_pg_incman_utenti["English"] = array();
	$fieldLabelspublic_pg_incman_utenti["English"]["ID"] = "ID";
	$fieldToolTipspublic_pg_incman_utenti["English"]["ID"] = "";
	$fieldLabelspublic_pg_incman_utenti["English"]["username"] = "Username";
	$fieldToolTipspublic_pg_incman_utenti["English"]["username"] = "";
	$fieldLabelspublic_pg_incman_utenti["English"]["password"] = "Password";
	$fieldToolTipspublic_pg_incman_utenti["English"]["password"] = "";
	$fieldLabelspublic_pg_incman_utenti["English"]["email"] = "Email";
	$fieldToolTipspublic_pg_incman_utenti["English"]["email"] = "";
	$fieldLabelspublic_pg_incman_utenti["English"]["fullname"] = "Fullname";
	$fieldToolTipspublic_pg_incman_utenti["English"]["fullname"] = "";
	$fieldLabelspublic_pg_incman_utenti["English"]["groupid"] = "Groupid";
	$fieldToolTipspublic_pg_incman_utenti["English"]["groupid"] = "";
	$fieldLabelspublic_pg_incman_utenti["English"]["active"] = "Active";
	$fieldToolTipspublic_pg_incman_utenti["English"]["active"] = "";
	if (count($fieldToolTipspublic_pg_incman_utenti["English"]))
		$tdatapublic_pg_incman_utenti[".isUseToolTips"] = true;
}


	$tdatapublic_pg_incman_utenti[".NCSearch"] = true;



$tdatapublic_pg_incman_utenti[".shortTableName"] = "public_pg_incman_utenti";
$tdatapublic_pg_incman_utenti[".nSecOptions"] = 0;
$tdatapublic_pg_incman_utenti[".recsPerRowPrint"] = 1;
$tdatapublic_pg_incman_utenti[".mainTableOwnerID"] = "";
$tdatapublic_pg_incman_utenti[".moveNext"] = 1;
$tdatapublic_pg_incman_utenti[".entityType"] = 0;

$tdatapublic_pg_incman_utenti[".strOriginalTableName"] = "public.pg-incman_utenti";

	



$tdatapublic_pg_incman_utenti[".showAddInPopup"] = false;

$tdatapublic_pg_incman_utenti[".showEditInPopup"] = false;

$tdatapublic_pg_incman_utenti[".showViewInPopup"] = false;

//page's base css files names
$popupPagesLayoutNames = array();
$tdatapublic_pg_incman_utenti[".popupPagesLayoutNames"] = $popupPagesLayoutNames;


$tdatapublic_pg_incman_utenti[".fieldsForRegister"] = array();

$tdatapublic_pg_incman_utenti[".listAjax"] = false;

	$tdatapublic_pg_incman_utenti[".audit"] = false;

	$tdatapublic_pg_incman_utenti[".locking"] = false;









$tdatapublic_pg_incman_utenti[".showSimpleSearchOptions"] = false;

// search Saving settings
$tdatapublic_pg_incman_utenti[".searchSaving"] = false;
//

$tdatapublic_pg_incman_utenti[".showSearchPanel"] = true;
		$tdatapublic_pg_incman_utenti[".flexibleSearch"] = true;

$tdatapublic_pg_incman_utenti[".isUseAjaxSuggest"] = true;

$tdatapublic_pg_incman_utenti[".rowHighlite"] = true;



$tdatapublic_pg_incman_utenti[".addPageEvents"] = false;

// use timepicker for search panel
$tdatapublic_pg_incman_utenti[".isUseTimeForSearch"] = false;





$tdatapublic_pg_incman_utenti[".allSearchFields"] = array();
$tdatapublic_pg_incman_utenti[".filterFields"] = array();
$tdatapublic_pg_incman_utenti[".requiredSearchFields"] = array();



$tdatapublic_pg_incman_utenti[".googleLikeFields"] = array();
$tdatapublic_pg_incman_utenti[".googleLikeFields"][] = "ID";
$tdatapublic_pg_incman_utenti[".googleLikeFields"][] = "username";
$tdatapublic_pg_incman_utenti[".googleLikeFields"][] = "password";
$tdatapublic_pg_incman_utenti[".googleLikeFields"][] = "email";
$tdatapublic_pg_incman_utenti[".googleLikeFields"][] = "fullname";
$tdatapublic_pg_incman_utenti[".googleLikeFields"][] = "groupid";
$tdatapublic_pg_incman_utenti[".googleLikeFields"][] = "active";


$tdatapublic_pg_incman_utenti[".advSearchFields"] = array();
$tdatapublic_pg_incman_utenti[".advSearchFields"][] = "ID";
$tdatapublic_pg_incman_utenti[".advSearchFields"][] = "username";
$tdatapublic_pg_incman_utenti[".advSearchFields"][] = "password";
$tdatapublic_pg_incman_utenti[".advSearchFields"][] = "email";
$tdatapublic_pg_incman_utenti[".advSearchFields"][] = "fullname";
$tdatapublic_pg_incman_utenti[".advSearchFields"][] = "groupid";
$tdatapublic_pg_incman_utenti[".advSearchFields"][] = "active";

$tdatapublic_pg_incman_utenti[".tableType"] = "list";

$tdatapublic_pg_incman_utenti[".printerPageOrientation"] = 0;
$tdatapublic_pg_incman_utenti[".nPrinterPageScale"] = 100;

$tdatapublic_pg_incman_utenti[".nPrinterSplitRecords"] = 40;

$tdatapublic_pg_incman_utenti[".nPrinterPDFSplitRecords"] = 40;



$tdatapublic_pg_incman_utenti[".geocodingEnabled"] = false;





$tdatapublic_pg_incman_utenti[".listGridLayout"] = 3;





// view page pdf

// print page pdf


$tdatapublic_pg_incman_utenti[".pageSize"] = 20;

$tdatapublic_pg_incman_utenti[".warnLeavingPages"] = true;



$tstrOrderBy = "ORDER BY \"ID\" DESC";
if(strlen($tstrOrderBy) && strtolower(substr($tstrOrderBy,0,8))!="order by")
	$tstrOrderBy = "order by ".$tstrOrderBy;
$tdatapublic_pg_incman_utenti[".strOrderBy"] = $tstrOrderBy;

$tdatapublic_pg_incman_utenti[".orderindexes"] = array();
$tdatapublic_pg_incman_utenti[".orderindexes"][] = array(1, (0 ? "ASC" : "DESC"), "\"ID\"");

$tdatapublic_pg_incman_utenti[".sqlHead"] = "SELECT \"ID\",  username,  password,  email,  fullname,  groupid,  active";
$tdatapublic_pg_incman_utenti[".sqlFrom"] = "FROM \"public\".\"pg-incman_utenti\"";
$tdatapublic_pg_incman_utenti[".sqlWhereExpr"] = "";
$tdatapublic_pg_incman_utenti[".sqlTail"] = "";











//fill array of records per page for list and report without group fields
$arrRPP = array();
$arrRPP[] = 10;
$arrRPP[] = 20;
$arrRPP[] = 30;
$arrRPP[] = 50;
$arrRPP[] = 100;
$arrRPP[] = 500;
$arrRPP[] = -1;
$tdatapublic_pg_incman_utenti[".arrRecsPerPage"] = $arrRPP;

//fill array of groups per page for report with group fields
$arrGPP = array();
$arrGPP[] = 1;
$arrGPP[] = 3;
$arrGPP[] = 5;
$arrGPP[] = 10;
$arrGPP[] = 50;
$arrGPP[] = 100;
$arrGPP[] = -1;
$tdatapublic_pg_incman_utenti[".arrGroupsPerPage"] = $arrGPP;

$tdatapublic_pg_incman_utenti[".highlightSearchResults"] = true;

$tableKeyspublic_pg_incman_utenti = array();
$tableKeyspublic_pg_incman_utenti[] = "ID";
$tdatapublic_pg_incman_utenti[".Keys"] = $tableKeyspublic_pg_incman_utenti;

$tdatapublic_pg_incman_utenti[".listFields"] = array();

$tdatapublic_pg_incman_utenti[".hideMobileList"] = array();


$tdatapublic_pg_incman_utenti[".viewFields"] = array();

$tdatapublic_pg_incman_utenti[".addFields"] = array();

$tdatapublic_pg_incman_utenti[".masterListFields"] = array();
$tdatapublic_pg_incman_utenti[".masterListFields"][] = "ID";
$tdatapublic_pg_incman_utenti[".masterListFields"][] = "username";
$tdatapublic_pg_incman_utenti[".masterListFields"][] = "password";
$tdatapublic_pg_incman_utenti[".masterListFields"][] = "email";
$tdatapublic_pg_incman_utenti[".masterListFields"][] = "fullname";
$tdatapublic_pg_incman_utenti[".masterListFields"][] = "groupid";
$tdatapublic_pg_incman_utenti[".masterListFields"][] = "active";

$tdatapublic_pg_incman_utenti[".inlineAddFields"] = array();

$tdatapublic_pg_incman_utenti[".editFields"] = array();

$tdatapublic_pg_incman_utenti[".inlineEditFields"] = array();

$tdatapublic_pg_incman_utenti[".exportFields"] = array();

$tdatapublic_pg_incman_utenti[".importFields"] = array();

$tdatapublic_pg_incman_utenti[".printFields"] = array();

//	ID
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 1;
	$fdata["strName"] = "ID";
	$fdata["GoodName"] = "ID";
	$fdata["ownerTable"] = "public.pg-incman_utenti";
	$fdata["Label"] = GetFieldLabel("public_pg_incman_utenti","ID");
	$fdata["FieldType"] = 3;

	
		$fdata["AutoInc"] = true;

	
			
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "ID";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "\"ID\"";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



		$edata["IsRequired"] = true;

	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "number";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
						$edata["validateAs"]["basicValidate"][] = "IsRequired";
		
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_pg_incman_utenti["ID"] = $fdata;
//	username
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 2;
	$fdata["strName"] = "username";
	$fdata["GoodName"] = "username";
	$fdata["ownerTable"] = "public.pg-incman_utenti";
	$fdata["Label"] = GetFieldLabel("public_pg_incman_utenti","username");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "username";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "username";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_pg_incman_utenti["username"] = $fdata;
//	password
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 3;
	$fdata["strName"] = "password";
	$fdata["GoodName"] = "password";
	$fdata["ownerTable"] = "public.pg-incman_utenti";
	$fdata["Label"] = GetFieldLabel("public_pg_incman_utenti","password");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "password";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "password";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Password");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
	
		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_pg_incman_utenti["password"] = $fdata;
//	email
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 4;
	$fdata["strName"] = "email";
	$fdata["GoodName"] = "email";
	$fdata["ownerTable"] = "public.pg-incman_utenti";
	$fdata["Label"] = GetFieldLabel("public_pg_incman_utenti","email");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "email";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "email";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "email";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_pg_incman_utenti["email"] = $fdata;
//	fullname
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 5;
	$fdata["strName"] = "fullname";
	$fdata["GoodName"] = "fullname";
	$fdata["ownerTable"] = "public.pg-incman_utenti";
	$fdata["Label"] = GetFieldLabel("public_pg_incman_utenti","fullname");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "fullname";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "fullname";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_pg_incman_utenti["fullname"] = $fdata;
//	groupid
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 6;
	$fdata["strName"] = "groupid";
	$fdata["GoodName"] = "groupid";
	$fdata["ownerTable"] = "public.pg-incman_utenti";
	$fdata["Label"] = GetFieldLabel("public_pg_incman_utenti","groupid");
	$fdata["FieldType"] = 200;

	
	
	
			
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "groupid";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "groupid";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "text";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
	
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_pg_incman_utenti["groupid"] = $fdata;
//	active
//	Custom field settings
	$fdata = array();
	$fdata["Index"] = 7;
	$fdata["strName"] = "active";
	$fdata["GoodName"] = "active";
	$fdata["ownerTable"] = "public.pg-incman_utenti";
	$fdata["Label"] = GetFieldLabel("public_pg_incman_utenti","active");
	$fdata["FieldType"] = 3;

	
	
	
			
	
	
	
	
	
	
	
	
	
		$fdata["strField"] = "active";

		$fdata["isSQLExpression"] = true;
	$fdata["FullName"] = "active";

	
	
			
				$fdata["UploadFolder"] = "files";

//  Begin View Formats
	$fdata["ViewFormats"] = array();

	$vdata = array("ViewFormat" => "");

	
	
	
	
	
	
	
	
	
	
	
		$vdata["NeedEncode"] = true;

	$fdata["ViewFormats"]["view"] = $vdata;
//  End View Formats

//	Begin Edit Formats
	$fdata["EditFormats"] = array();

	$edata = array("EditFormat" => "Text field");

	
	



	
	
	
	
			$edata["acceptFileTypes"] = ".+$";

		$edata["maxNumberOfFiles"] = 1;

	
	
	
	
			$edata["HTML5InuptType"] = "number";

		$edata["EditParams"] = "";
		
		$edata["controlWidth"] = 200;

//	Begin validation
	$edata["validateAs"] = array();
	$edata["validateAs"]["basicValidate"] = array();
	$edata["validateAs"]["customMessages"] = array();
				$edata["validateAs"]["basicValidate"][] = getJsValidatorName("Number");
							
	
	//	End validation

	
			
	
	
	
	$fdata["EditFormats"]["edit"] = $edata;
//	End Edit Formats


	$fdata["isSeparate"] = false;








	$tdatapublic_pg_incman_utenti["active"] = $fdata;


$tables_data["public.pg-incman_utenti"]=&$tdatapublic_pg_incman_utenti;
$field_labels["public_pg_incman_utenti"] = &$fieldLabelspublic_pg_incman_utenti;
$fieldToolTips["public_pg_incman_utenti"] = &$fieldToolTipspublic_pg_incman_utenti;
$page_titles["public_pg_incman_utenti"] = &$pageTitlespublic_pg_incman_utenti;

// -----------------start  prepare master-details data arrays ------------------------------//
// tables which are detail tables for current table (master)
$detailsTablesData["public.pg-incman_utenti"] = array();

// tables which are master tables for current table (detail)
$masterTablesData["public.pg-incman_utenti"] = array();


// -----------------end  prepare master-details data arrays ------------------------------//

require_once(getabspath("classes/sql.php"));










function createSqlQuery_public_pg_incman_utenti()
{
$proto0=array();
$proto0["m_strHead"] = "SELECT";
$proto0["m_strFieldList"] = "\"ID\",  username,  password,  email,  fullname,  groupid,  active";
$proto0["m_strFrom"] = "FROM \"public\".\"pg-incman_utenti\"";
$proto0["m_strWhere"] = "";
$proto0["m_strOrderBy"] = "ORDER BY \"ID\" DESC";
$proto0["m_strTail"] = "";
	
		;
			$proto0["cipherer"] = null;
$proto2=array();
$proto2["m_sql"] = "";
$proto2["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto2["m_column"]=$obj;
$proto2["m_contained"] = array();
$proto2["m_strCase"] = "";
$proto2["m_havingmode"] = false;
$proto2["m_inBrackets"] = false;
$proto2["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto2);

$proto0["m_where"] = $obj;
$proto4=array();
$proto4["m_sql"] = "";
$proto4["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto4["m_column"]=$obj;
$proto4["m_contained"] = array();
$proto4["m_strCase"] = "";
$proto4["m_havingmode"] = false;
$proto4["m_inBrackets"] = false;
$proto4["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto4);

$proto0["m_having"] = $obj;
$proto0["m_fieldlist"] = array();
						$proto6=array();
			$obj = new SQLField(array(
	"m_strName" => "ID",
	"m_strTable" => "public.pg-incman_utenti",
	"m_srcTableName" => "public.pg-incman_utenti"
));

$proto6["m_sql"] = "\"ID\"";
$proto6["m_srcTableName"] = "public.pg-incman_utenti";
$proto6["m_expr"]=$obj;
$proto6["m_alias"] = "";
$obj = new SQLFieldListItem($proto6);

$proto0["m_fieldlist"][]=$obj;
						$proto8=array();
			$obj = new SQLField(array(
	"m_strName" => "username",
	"m_strTable" => "public.pg-incman_utenti",
	"m_srcTableName" => "public.pg-incman_utenti"
));

$proto8["m_sql"] = "username";
$proto8["m_srcTableName"] = "public.pg-incman_utenti";
$proto8["m_expr"]=$obj;
$proto8["m_alias"] = "";
$obj = new SQLFieldListItem($proto8);

$proto0["m_fieldlist"][]=$obj;
						$proto10=array();
			$obj = new SQLField(array(
	"m_strName" => "password",
	"m_strTable" => "public.pg-incman_utenti",
	"m_srcTableName" => "public.pg-incman_utenti"
));

$proto10["m_sql"] = "password";
$proto10["m_srcTableName"] = "public.pg-incman_utenti";
$proto10["m_expr"]=$obj;
$proto10["m_alias"] = "";
$obj = new SQLFieldListItem($proto10);

$proto0["m_fieldlist"][]=$obj;
						$proto12=array();
			$obj = new SQLField(array(
	"m_strName" => "email",
	"m_strTable" => "public.pg-incman_utenti",
	"m_srcTableName" => "public.pg-incman_utenti"
));

$proto12["m_sql"] = "email";
$proto12["m_srcTableName"] = "public.pg-incman_utenti";
$proto12["m_expr"]=$obj;
$proto12["m_alias"] = "";
$obj = new SQLFieldListItem($proto12);

$proto0["m_fieldlist"][]=$obj;
						$proto14=array();
			$obj = new SQLField(array(
	"m_strName" => "fullname",
	"m_strTable" => "public.pg-incman_utenti",
	"m_srcTableName" => "public.pg-incman_utenti"
));

$proto14["m_sql"] = "fullname";
$proto14["m_srcTableName"] = "public.pg-incman_utenti";
$proto14["m_expr"]=$obj;
$proto14["m_alias"] = "";
$obj = new SQLFieldListItem($proto14);

$proto0["m_fieldlist"][]=$obj;
						$proto16=array();
			$obj = new SQLField(array(
	"m_strName" => "groupid",
	"m_strTable" => "public.pg-incman_utenti",
	"m_srcTableName" => "public.pg-incman_utenti"
));

$proto16["m_sql"] = "groupid";
$proto16["m_srcTableName"] = "public.pg-incman_utenti";
$proto16["m_expr"]=$obj;
$proto16["m_alias"] = "";
$obj = new SQLFieldListItem($proto16);

$proto0["m_fieldlist"][]=$obj;
						$proto18=array();
			$obj = new SQLField(array(
	"m_strName" => "active",
	"m_strTable" => "public.pg-incman_utenti",
	"m_srcTableName" => "public.pg-incman_utenti"
));

$proto18["m_sql"] = "active";
$proto18["m_srcTableName"] = "public.pg-incman_utenti";
$proto18["m_expr"]=$obj;
$proto18["m_alias"] = "";
$obj = new SQLFieldListItem($proto18);

$proto0["m_fieldlist"][]=$obj;
$proto0["m_fromlist"] = array();
												$proto20=array();
$proto20["m_link"] = "SQLL_MAIN";
			$proto21=array();
$proto21["m_strName"] = "public.pg-incman_utenti";
$proto21["m_srcTableName"] = "public.pg-incman_utenti";
$proto21["m_columns"] = array();
$proto21["m_columns"][] = "ID";
$proto21["m_columns"][] = "username";
$proto21["m_columns"][] = "password";
$proto21["m_columns"][] = "email";
$proto21["m_columns"][] = "fullname";
$proto21["m_columns"][] = "groupid";
$proto21["m_columns"][] = "active";
$obj = new SQLTable($proto21);

$proto20["m_table"] = $obj;
$proto20["m_sql"] = "\"public\".\"pg-incman_utenti\"";
$proto20["m_alias"] = "";
$proto20["m_srcTableName"] = "public.pg-incman_utenti";
$proto22=array();
$proto22["m_sql"] = "";
$proto22["m_uniontype"] = "SQLL_UNKNOWN";
	$obj = new SQLNonParsed(array(
	"m_sql" => ""
));

$proto22["m_column"]=$obj;
$proto22["m_contained"] = array();
$proto22["m_strCase"] = "";
$proto22["m_havingmode"] = false;
$proto22["m_inBrackets"] = false;
$proto22["m_useAlias"] = false;
$obj = new SQLLogicalExpr($proto22);

$proto20["m_joinon"] = $obj;
$obj = new SQLFromListItem($proto20);

$proto0["m_fromlist"][]=$obj;
$proto0["m_groupby"] = array();
$proto0["m_orderby"] = array();
												$proto24=array();
						$obj = new SQLField(array(
	"m_strName" => "ID",
	"m_strTable" => "public.pg-incman_utenti",
	"m_srcTableName" => "public.pg-incman_utenti"
));

$proto24["m_column"]=$obj;
$proto24["m_bAsc"] = 0;
$proto24["m_nColumn"] = 0;
$obj = new SQLOrderByItem($proto24);

$proto0["m_orderby"][]=$obj;					
$proto0["m_srcTableName"]="public.pg-incman_utenti";		
$obj = new SQLQuery($proto0);

	return $obj;
}
$queryData_public_pg_incman_utenti = createSqlQuery_public_pg_incman_utenti();


	
		;

							

$tdatapublic_pg_incman_utenti[".sqlquery"] = $queryData_public_pg_incman_utenti;

$tableEvents["public.pg-incman_utenti"] = new eventsBase;
$tdatapublic_pg_incman_utenti[".hasEvents"] = false;

?>